import { useState } from "react";
import { Notes } from "./Components/Notes_Components/Notes";

function App() {
  return (
    <>
      <Notes />
    </>
  );
}

export default App;
