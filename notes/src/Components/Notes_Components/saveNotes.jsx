export const getNotesLocalStorageData = () => {
  const storedNotes = localStorage.getItem("savedNotes");
  return storedNotes ? JSON.parse(storedNotes) : [];
};

export const setNotesToLocalStorage = (notes) => {
  localStorage.setItem("savedNotes", JSON.stringify(notes));
};

export const removeFromLocalStorage = () => {
  localStorage.removeItem("savedNotes");
};
