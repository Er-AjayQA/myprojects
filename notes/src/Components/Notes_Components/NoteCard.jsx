import { useState } from "react";
import notesStyle from "./notes.module.css";
import { IoCloseSharp } from "react-icons/io5";
import { MdOutlineDeleteOutline } from "react-icons/md";

export const NoteCard = ({ notes, onDelete, index }) => {
  const [showMore, setShowMore] = useState(false);

  const handleShowMore = () => {
    setShowMore(true);
  };

  const handleCloseShowMore = () => {
    setShowMore(false);
  };

  const handleDelete = () => {
    onDelete(index);
  };

  return (
    <>
      <li key={notes.title} onClick={handleShowMore}>
        <div className={notesStyle["note-card"]}>
          <MdOutlineDeleteOutline
            className={notesStyle["delete-icon"]}
            onClick={handleDelete}
          />
          <p className={notesStyle["note-title"]}>{notes.title}</p>
          <div className={notesStyle["note-content"]}>
            <p className={notesStyle["note-desc"]}>{notes.description}</p>
          </div>
        </div>
      </li>

      {showMore && (
        <div className={notesStyle["show-more"]}>
          <div className={notesStyle.overlay}></div>
          <div className={notesStyle["more-content"]}>
            <div className={notesStyle["card-content"]}>
              <IoCloseSharp
                className={notesStyle["close-icon"]}
                onClick={handleCloseShowMore}
              />
              <p className={notesStyle["note-title"]}>{notes.title}</p>
              <div>
                <p className={notesStyle["note-desc"]}>{notes.description}</p>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
