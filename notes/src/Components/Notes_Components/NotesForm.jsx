import { useState } from "react";
import notesStyle from "./notes.module.css";
import { IoCloseSharp } from "react-icons/io5";

export const NotesForm = ({ onAddNotes, toggleFormEnable }) => {
  const [notes, setNotes] = useState({ title: "", description: "" });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNotes((prev) => ({ ...prev, [name]: value }));
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    onAddNotes(notes);
  };

  const handleCloseForm = () => {
    toggleFormEnable();
  };

  return (
    <>
      <div className={notesStyle.overlay}></div>
      <form className={notesStyle["notes-form"]} onSubmit={handleFormSubmit}>
        <IoCloseSharp
          className={notesStyle["close-icon"]}
          onClick={handleCloseForm}
        />
        <p className={notesStyle["form-title"]}>Create Note</p>
        <fieldset>
          <label htmlFor="title">Title</label>
          <input
            type="text"
            name="title"
            value={notes.title}
            required
            placeholder="Enter title"
            onChange={handleInputChange}
          />
        </fieldset>
        <fieldset>
          <label htmlFor="description">Description</label>
          <textarea
            name="description"
            placeholder="Enter description"
            required
            value={notes.description}
            onChange={handleInputChange}
          ></textarea>
        </fieldset>
        <fieldset>
          <button className={`${notesStyle.btn} ${notesStyle["save-btn"]}`}>
            Save
          </button>
        </fieldset>
      </form>
    </>
  );
};
