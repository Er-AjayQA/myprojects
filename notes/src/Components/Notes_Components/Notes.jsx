import { useState, useEffect } from "react";
import notesStyle from "./notes.module.css";
import { setNotesToLocalStorage, getNotesLocalStorageData } from "./saveNotes";
import { NotesForm } from "./NotesForm";
import { NoteCard } from "./NoteCard";

export const Notes = () => {
  const [formEnable, setFormEnable] = useState(false);
  const [allNotes, setAllNotes] = useState(getNotesLocalStorageData());
  const [filteredNotes, setFilteredNotes] = useState(allNotes);
  const [search, setSearch] = useState("");

  // Handle Search Input Change
  const handleSearchChange = (e) => {
    const newSearch = e.target.value.toLowerCase();
    setSearch(newSearch);
    const filteredData = allNotes.filter((curNote) =>
      curNote.title.toLowerCase().includes(newSearch)
    );
    setFilteredNotes(filteredData);
  };

  // Toggling Create Note Form
  const toggleFormEnable = () => {
    setFormEnable((prevState) => !prevState);
  };

  const handleFormStatus = (status) => {
    if (!status) {
      toggleFormEnable();
    } else {
      setFormEnable(true);
    }
  };

  // Handle Create Note form submit
  const handleFormSubmit = (newNote) => {
    setAllNotes([...allNotes, newNote]);
    setFilteredNotes([...allNotes, newNote]);
    toggleFormEnable();
  };

  setNotesToLocalStorage(allNotes);

  // Delete Notes
  const deleteNote = (noteIndex) => {
    setAllNotes(allNotes.filter((_, index) => index !== noteIndex));
    setFilteredNotes(allNotes.filter((_, index) => index !== noteIndex));
  };

  useEffect(() => {
    setFilteredNotes(allNotes);
  }, [allNotes]);

  return (
    <>
      <div className={notesStyle.container}>
        <header>
          <h1>My Notes</h1>
          <button
            className={`${notesStyle.btn} ${notesStyle["create-btn"]}`}
            onClick={() => handleFormStatus(true)}
          >
            Create
          </button>
        </header>
        <div className={notesStyle["search-box"]}>
          <input
            type="search"
            placeholder="Search"
            value={search}
            onChange={handleSearchChange}
          />
        </div>

        {formEnable && (
          <NotesForm
            onAddNotes={handleFormSubmit}
            toggleFormEnable={toggleFormEnable}
          />
        )}

        <ul className={notesStyle["notes-container"]}>
          {filteredNotes.map((curNote, index) => {
            return (
              <NoteCard
                key={curNote.title}
                notes={curNote}
                index={index}
                formStatus={handleFormStatus}
                onDelete={deleteNote}
              />
            );
          })}
        </ul>
      </div>
    </>
  );
};
