// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const orderController = require("./order.controller");

// ========== Routes ========== //
router.post("/create_order/:user_id", orderController.createOrder);
router.put(
  "/create_order/:user_id/:order_id/:prod_id",
  orderController.cancelProduct
);
router.get("/get_all_orders", orderController.getAllOrders);
router.get("/get_users_all_orders/:user_id", orderController.getUserAllOrders);

// ========== Export Routes ========== //
module.exports = router;
