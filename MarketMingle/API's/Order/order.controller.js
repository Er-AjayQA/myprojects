// ========== IMPORTS and CONFIGS ========== //
const orderModel = require("./order.model");
const cartModel = require("../AddToCart/addToCart.model");
const userModel = require("../Register/register.model");
const mongoose = require("mongoose");

// ========== Create Order ========== //
module.exports.createOrder = async (req, res) => {
  try {
    const { user_id } = req.params;
    const { paymentMode } = req.body;

    const isUserExist = await userModel.findById({ _id: user_id });

    if (!isUserExist || !isUserExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const getUserCart = await cartModel.findOne({ user_id, isDeleted: false });

    if (!getUserCart) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Cart Not Found!!!",
        },
      });
    }

    if (getUserCart && getUserCart.products.length <= 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Cart is Empty!!!",
        },
      });
    }

    const newOrder = await orderModel({
      user_id,
      products: getUserCart.products,
      paymentMode,
      isPaid: "Online" ? true : false,
    }).save();

    await cartModel.findOneAndUpdate(
      { user_id, isDeleted: false },
      {
        $set: {
          products: [], // Assuming products should be an empty array
        },
      }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Order Created Successfully!!!",
        orderDetail: newOrder,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Cancel Order ========== //
module.exports.cancelProduct = async (req, res) => {
  try {
    const { user_id, order_id, prod_id } = req.params;

    const isUserExist = await userModel.findById({ _id: user_id });

    if (!isUserExist || !isUserExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const getUserOrder = await orderModel.findOne({
      _id: order_id,
      orderStatus: "Order Placed",
      isDelivered: false,
    });

    if (!getUserOrder) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Order Not Found!!!",
        },
      });
    }

    const cancelProduct = await orderModel.findOneAndUpdate(
      { _id: order_id },
      {
        orderStatus:
          getUserOrder.products.length === 1 ? "Cancelled" : "Order Placed",
        $pull: { products: { prod_id: prod_id } },
      },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Cancelled Successfully!!!",
        orderDetail: cancelProduct,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Order ========== //
module.exports.getAllOrders = async (req, res) => {
  try {
    const getAllOrder = await orderModel
      .find()
      .populate("user_id", ["firstName", "lastName"])
      .populate("products.prod_id", ["product_name", "netAmount"]);

    if (!getAllOrder) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Orders Found!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: "All Orders List!!!",
        allOrders: getAllOrder,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get User Order ========== //
module.exports.getUserAllOrders = async (req, res) => {
  try {
    const { user_id } = req.params;

    const isUserExist = await userModel.findById({ _id: user_id });
    if (!isUserExist || !isUserExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const getUserOrders = await orderModel
      .find({ user_id })
      .populate("user_id", ["firstName", "lastName"])
      .populate("products.prod_id", ["product_name", "netAmount"])
      .sort({ createdAt: -1 });

    if (!getUserOrders) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Orders Found!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: "All Orders List!!!",
        allOrders: getUserOrders,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
