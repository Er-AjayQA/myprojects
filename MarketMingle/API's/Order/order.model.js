// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define SubCategory Schema ========== //
const orderSchema = new Schema(
  {
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "registrations",
    },
    products: [
      {
        prod_id: {
          type: Schema.Types.ObjectId,
          ref: "products",
        },
        qty: {
          type: Number,
        },
      },
    ],
    paymentMode: {
      type: String,
      enum: ["COD", "Online"],
    },
    isPaid: {
      type: Boolean,
      default: false,
    },
    orderStatus: {
      type: String,
      enum: ["Order Placed", "Cancelled"],
      default: "Order Placed",
    },
    isDelivered: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== Define SubCategory Model ========== //
module.exports = new mongoose.model("order", orderSchema);
