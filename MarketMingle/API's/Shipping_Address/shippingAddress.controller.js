// ========== IMPORTS and CONFIGS ========== //
const shippingAddressModel = require("./shippingAddress.model");
const userModel = require("../Register/register.model");

// ========== Create Shipping Address ========== //
module.exports.createShippingAddress = async (req, res) => {
  try {
    const { user_id } = req.params;
    const { title, street_address, city, state, zip_code, country } = req.body;

    const userAddressTitleExist = await shippingAddressModel.findOne({
      user_id,
      title,
      isDeleted: false,
    });

    if (userAddressTitleExist) {
      return res.status(409).send({
        statusCode: 409,
        error: {
          message: "Address Title Already Exist!!!",
        },
      });
    }

    const add_Address = await shippingAddressModel({
      user_id,
      title,
      street_address,
      city,
      state,
      zip_code,
      country,
    }).save();
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Address Added Successfully!!!",
        addressDetails: add_Address,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update Shipping Address ========== //
module.exports.updateShippingAddress = async (req, res) => {
  try {
    const { ship_id, user_id } = req.params;
    const { title, street_address, city, state, zip_code, country } = req.body;

    const isAddressExisting = await shippingAddressModel.findById({
      _id: ship_id,
    });

    if (!isAddressExisting || isAddressExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: { message: `Address Not Found!!!` },
      });
    }

    const isAddressTitleExist = await shippingAddressModel.findOne({
      user_id,
      title,
      _id: { $ne: ship_id },
    });

    if (isAddressTitleExist) {
      return res.status(409).send({
        statusCode: 409,
        error: {
          message: "Address Title Already Exist!!!",
        },
      });
    }

    const update_address = await shippingAddressModel.findByIdAndUpdate(
      { _id: ship_id },
      {
        title,
        street_address,
        city,
        state,
        zip_code,
        country,
      },
      { new: true }
    );
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Address Updated Successfully!!!",
        addressDetails: update_address,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Delete Shipping Address ========== //
module.exports.deleteShippingAddress = async (req, res) => {
  try {
    const { ship_id } = req.params;
    const isAddressExisting = await shippingAddressModel.findById({
      _id: ship_id,
    });

    if (!isAddressExisting || isAddressExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Address Not Found!!!",
        },
      });
    }

    const update_address = await shippingAddressModel.findByIdAndUpdate(
      { _id: ship_id },
      { isDeleted: true, isDefault: false },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Address Deleted Successfully!!!",
        deletedAddress: update_address,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Shipping Address ========== //
module.exports.getAllShippingAddress = async (req, res) => {
  try {
    const getAllAddress = await shippingAddressModel
      .find({
        isDeleted: false,
      })
      .populate("user_id", ["firstName", "lastName"]);

    if (!getAllAddress || getAllAddress.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Address Not Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Address Details!!!",
        addressDetails: getAllAddress,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Customer Shipping Address ========== //
module.exports.getUserAddress = async (req, res) => {
  try {
    const { user_id } = req.params;

    const isUserExist = await userModel.findById({ _id: user_id });

    if (!isUserExist || !isUserExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const getUserAddress = await shippingAddressModel
      .find({
        user_id,
        isDeleted: false,
      })
      .populate("user_id", ["firstName", "lastName"]);

    if (!getUserAddress || getUserAddress.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Address Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "List of All Address!!!",
        allAddressDetails: getUserAddress,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Customer Default Shipping Address ========== //
module.exports.getUserDefaultAddress = async (req, res) => {
  try {
    const { user_id } = req.params;

    const isUserExist = await userModel.findById({ _id: user_id });

    if (!isUserExist || !isUserExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const getDefaultAddress = await shippingAddressModel
      .findOne({
        user_id,
        isDefault: true,
        isDeleted: false,
      })
      .populate("user_id", ["firstName", "lastName"]);

    if (!getDefaultAddress || getDefaultAddress.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Address Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Default Address Details!!!",
        defaultAddress: getDefaultAddress,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Customer Address By Title ========== //
module.exports.getUserAddressByTitle = async (req, res) => {
  try {
    const { user_id, title } = req.params;

    const isUserExist = await userModel.findById({ _id: user_id });

    if (!isUserExist || !isUserExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const getAddressByTitle = await shippingAddressModel
      .find({
        user_id,
        title,
        isDeleted: false,
      })
      .populate("user_id", ["firstName", "lastName"]);

    if (!getAddressByTitle || getAddressByTitle.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Address Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Address By Title!!!",
        addressDetails: getAddressByTitle,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
