// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define SubCategory Schema ========== //
const shippingAddressSchema = new Schema(
  {
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "registrations",
    },
    title: {
      type: String,
      required: true,
    },
    street_address: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    state: {
      type: String,
      required: true,
    },
    zip_code: {
      type: Number,
      required: true,
      minLength: 6,
      maxLength: 6,
    },
    country: {
      type: String,
      required: true,
    },
    isDefault: {
      type: Boolean,
      default: false,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== Define SubCategory Model ========== //
module.exports = new mongoose.model("shippingAddress", shippingAddressSchema);
