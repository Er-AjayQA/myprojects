// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const shippingAddressController = require("./shippingAddress.controller");

// ========== Routes ========== //
router.post(
  "/create_shippingAddress/:user_id",
  shippingAddressController.createShippingAddress
);
router.put(
  "/update_shippingAddress/:user_id/:ship_id",
  shippingAddressController.updateShippingAddress
);
router.delete(
  "/delete_shippingAddress/:ship_id",
  shippingAddressController.deleteShippingAddress
);
router.get(
  "/getAllShippingAddress",
  shippingAddressController.getAllShippingAddress
);
router.get(
  "/getUserAddress/:user_id",
  shippingAddressController.getUserAddress
);
router.get(
  "/getUserDefaultAddress/:user_id",
  shippingAddressController.getUserDefaultAddress
);
router.get(
  "/getUserAddressByTitle/:user_id/:title",
  shippingAddressController.getUserAddressByTitle
);

// ========== Export Routes ========== //
module.exports = router;
