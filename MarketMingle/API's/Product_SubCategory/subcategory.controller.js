// ========== IMPORTS and CONFIGS ========== //
const categoryModel = require("../Product_Category/category.model");
const subcategoryModel = require("./subcategory.model");

// ========== Create SubCategory ========== //
module.exports.createSubCategory = async (req, res) => {
  try {
    const { subcategoryName, cat_id } = req.body;

    const isCategoryExist = await categoryModel.findById({
      _id: cat_id,
      isDeleted: false,
    });

    if (!isCategoryExist) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product Category Not Existing!!!` },
      });
    }

    const subcategoryAlreadyExisting = await subcategoryModel.findOne({
      cat_id,
      subcategoryName,
      isDeleted: false,
    });

    if (subcategoryAlreadyExisting) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product SubCategory Already Existing!!!` },
      });
    }

    const create_subcategory = await new subcategoryModel({
      cat_id,
      subcategoryName,
    }).save();

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product SubCategory Created Successfully!!!",
        subcategoryDetails: create_subcategory,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update SubCategory ========== //
module.exports.updateSubCategory = async (req, res) => {
  try {
    const { subCat_id } = req.params;
    const { cat_id, subcategoryName } = req.body;

    const isCategoryExist = await categoryModel.findById({ _id: cat_id });

    if (!isCategoryExist || isCategoryExist.isDeleted) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product Category Not Existing!!!` },
      });
    }

    const isSubcategoryExisting = await subcategoryModel.findOne({
      subCat_id,
      cat_id,
      isDeleted: false,
    });

    if (!isSubcategoryExisting) {
      return res.status(404).send({
        statusCode: 404,
        error: { message: `Product SubCategory Not Found!!!` },
      });
    }

    const isAlreadyExisting = await subcategoryModel.findOne({
      subcategoryName,
      cat_id,
      isDeleted: false,
      _id: { $ne: subCat_id },
    });

    if (isAlreadyExisting) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product SubCategory Already Existing!!!` },
      });
    } else {
      const update_subcategory = await subcategoryModel.findByIdAndUpdate(
        { _id: subCat_id },
        { subcategoryName, cat_id },
        { new: true }
      );
      return res.status(201).send({
        statusCode: 201,
        data: {
          message: "Product SubCategory Updated Successfully!!!",
          subcategoryDetails: update_subcategory,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update SubCategory Status ========== //
module.exports.updateSubCategoryStatus = async (req, res) => {
  try {
    const { subCat_id } = req.params;
    const isSubCategoryExisting = await subcategoryModel.findById({
      _id: subCat_id,
    });

    if (!isSubCategoryExisting || isSubCategoryExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product SubCategory Not Found!!!",
        },
      });
    }

    const update_subcategory = await subcategoryModel.findByIdAndUpdate(
      { _id: subCat_id },
      { isActive: !isSubCategoryExisting.isActive },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product SubCategory Status Updated Successfully!!!",
        categoryDetails: update_subcategory,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Delete SubCategory ========== //
module.exports.deleteSubCategory = async (req, res) => {
  try {
    const { subCat_id } = req.params;
    const isSubCategoryExisting = await subcategoryModel.findById({
      _id: subCat_id,
    });

    if (!isSubCategoryExisting || isSubCategoryExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product SubCategory Not Found!!!",
        },
      });
    }

    const update_subcategory = await subcategoryModel.findByIdAndUpdate(
      { _id: subCat_id },
      { isDeleted: true, isActive: false },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product SubCategory Deleted Successfully!!!",
        deletedData: update_subcategory,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All SubCategory ========== //
module.exports.getAllSubCategory = async (req, res) => {
  try {
    const subCategoryDetails = await subcategoryModel
      .find({ isDeleted: false })
      .populate("cat_id", ["categoryName"]);

    if (!subCategoryDetails) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product SubCategory Found!!!",
        },
      });
    }
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Get All Product SubCategory!!!",
        subcategoryDetails: subCategoryDetails,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Active SubCategory ========== //
module.exports.getAllActiveSubCategory = async (req, res) => {
  try {
    const allActiveSubcategory = await subcategoryModel
      .find({
        isDeleted: false,
        isActive: true,
      })
      .populate("cat_id", ["categoryName"]);

    if (!allActiveSubcategory) {
      return res.status(201).send({
        statusCode: 201,
        error: {
          message: "No Active Product SubCategories Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "All Active Product SubCategory Details!!!",
        allActiveSubcategories: allActiveSubcategory,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All SubCategory By Category ========== //
module.exports.getByCategory = async (req, res) => {
  try {
    const { cat_id } = req.params;

    const isCategoryExist = await categoryModel.findById({ _id: cat_id });

    if (!isCategoryExist || isCategoryExist.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Such Categories Found!!!",
        },
      });
    }

    const allSubcategoryByCategory = await subcategoryModel
      .find({
        cat_id,
        isDeleted: false,
        isActive: true,
      })
      .populate("cat_id", ["categoryName"]);

    if (!allSubcategoryByCategory) {
      return res.status(201).send({
        statusCode: 201,
        error: {
          message: "No SubCategories Found Under This Category!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "All SubCategories!!!",
        allActiveSubcategories: allSubcategoryByCategory,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
