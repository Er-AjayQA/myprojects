// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define SubCategory Schema ========== //
const subcategorySchema = new Schema(
  {
    cat_id: {
      type: Schema.Types.ObjectId,
      ref: "productCategory",
    },
    subcategoryName: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== Define SubCategory Model ========== //
module.exports = new mongoose.model("productSubCategory", subcategorySchema);
