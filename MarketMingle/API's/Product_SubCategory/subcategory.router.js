// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const subcategoryController = require("./subcategory.controller");

// ========== Routes ========== //
router.post("/create_subcategory", subcategoryController.createSubCategory);
router.put(
  "/update_subcategory/:subCat_id",
  subcategoryController.updateSubCategory
);
router.put(
  "/update_subcategory_status/:subCat_id",
  subcategoryController.updateSubCategoryStatus
);
router.delete(
  "/delete_subcategory/:subCat_id",
  subcategoryController.deleteSubCategory
);
router.get("/getAllSubCategory", subcategoryController.getAllSubCategory);
router.get(
  "/getActiveSubCategory",
  subcategoryController.getAllActiveSubCategory
);
router.get("/getByCategory/:cat_id", subcategoryController.getByCategory);
// ========== Export Routes ========== //
module.exports = router;
