// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const brandController = require("./brand.controller");

// ========== Routes ========== //
router.post("/create_brand", brandController.createBrand);
router.put("/update_Brand/:brand_id", brandController.updateBrand);
router.put("/update_Brand_status/:brand_id", brandController.updateBrandStatus);
router.delete("/delete_Brand/:brand_id", brandController.deleteBrand);
router.get("/getAllBrand", brandController.getAllBrand);
router.get("/getActiveBrand", brandController.getAllActiveBrands);

// ========== Export Routes ========== //
module.exports = router;
