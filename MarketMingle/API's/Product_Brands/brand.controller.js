// ========== IMPORTS and CONFIGS ========== //
const brandModel = require("./brand.model");

// ========== Create Brand ========== //
module.exports.createBrand = async (req, res) => {
  try {
    const { brandName } = req.body;
    const brandAlreadyExisting = await brandModel.findOne({
      brandName,
      isDeleted: false,
    });

    if (brandAlreadyExisting) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product Brand Already Existing!!!` },
      });
    }

    const create_brand = await new brandModel({
      brandName,
    }).save();
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Brand Created Successfully!!!",
        brandDetails: create_brand,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update Brand ========== //
module.exports.updateBrand = async (req, res) => {
  try {
    const { brand_id } = req.params;
    const { brandName } = req.body;
    const brandAlreadyExisting = await brandModel.findById({ _id: brand_id });

    if (!brandAlreadyExisting || brandAlreadyExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: { message: `Product Brand Not Found!!!` },
      });
    }

    const isAlreadyExisting = await brandModel.findOne({
      brandName,
      isDeleted: false,
      _id: { $ne: brand_id },
    });

    if (isAlreadyExisting) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product Brand Already Existing!!!` },
      });
    } else {
      const update_brand = await brandModel.findByIdAndUpdate(
        { _id: brand_id },
        { brandName },
        { new: true }
      );
      return res.status(201).send({
        statusCode: 201,
        data: {
          message: "Product  Brand Updated Successfully!!!",
          brandDetails: update_brand,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update Brand Status ========== //
module.exports.updateBrandStatus = async (req, res) => {
  try {
    const { brand_id } = req.params;
    const isBrandExisting = await brandModel.findById({ _id: brand_id });

    if (!isBrandExisting || isBrandExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Brand Not Found!!!",
        },
      });
    }

    const update_brand = await brandModel.findByIdAndUpdate(
      { _id: brand_id },
      { isActive: !isBrandExisting.isActive },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Brand Status Updated Successfully!!!",
        brandDetails: update_brand,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Delete Brand ========== //
module.exports.deleteBrand = async (req, res) => {
  try {
    const { brand_id } = req.params;
    const isBrandExisting = await brandModel.findById({ _id: brand_id });

    if (!isBrandExisting || isBrandExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Brand Not Found!!!",
        },
      });
    }

    const update_brand = await brandModel.findByIdAndUpdate(
      { _id: brand_id },
      { isDeleted: true, isActive: false },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Brand Deleted Successfully!!!",
        deletedBrand: update_brand,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Brand ========== //
module.exports.getAllBrand = async (req, res) => {
  try {
    const getAllBrand = await brandModel.find({ isDeleted: false });

    if (!getAllBrand || getAllBrand.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Brand Not Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Brand Details!!!",
        brandDetails: getAllBrand,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Active Brand ========== //
module.exports.getAllActiveBrands = async (req, res) => {
  try {
    const getAllActiveBrand = await brandModel.find({
      isActive: true,
      isDeleted: false,
    });

    if (!getAllActiveBrand || getAllActiveBrand.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Active Product Brands Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "All Active Product Brands Details!!!",
        brandDetails: getAllActiveBrand,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
