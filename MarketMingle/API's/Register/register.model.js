// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const saltRounds = 10;
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

// ========== Define Registration Schema ========== //
const registerSchema = new Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      enum: ["admin", "customer"],
      required: true,
    },
    email_Id: {
      type: String,
      required: true,
    },
    mobile_No: {
      type: Number,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    isSubscribed: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

// Pre-save hook to conditionally disable orders and wishlist for admin users
registerSchema.pre("save", async function (next) {
  if (this.role === "admin") {
    delete this.addresses;
    delete this.carts;
    delete this.orders;
    delete this.wishlist;
  }
  const hashedPwd = await bcrypt.hash(this.password, saltRounds);
  this.password = hashedPwd;
  next();
});
// ========== Define Registration Model ========== //
module.exports = new mongoose.model("registrations", registerSchema);
