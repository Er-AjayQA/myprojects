// ========== IMPORTS and CONFIGS ========== //
const RegisterModel = require("./register.model");
const { generateAccessToken } = require("../../Utils/utils");
const bcrypt = require("bcrypt");

// ========== Register Users ========== //
module.exports.register = async (req, res) => {
  try {
    const { role, firstName, lastName, email_Id, mobile_No, password } =
      req.body;

    const userExist = await RegisterModel.findOne({
      $and: [
        {
          $or: [{ email_Id }, { mobile_No }],
        },
        { role },
        { isSubscribed: true },
      ],
    });

    if (userExist) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "User Already Exists!!!" },
      });
    } else {
      const registerUser = await new RegisterModel({
        role,
        firstName,
        lastName,
        email_Id,
        mobile_No,
        password,
      }).save();
      return res.status(201).send({
        statusCode: 201,
        data: {
          message: "User Register Successfully!!!",
          userDetails: registerUser,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error },
    });
  }
};

// ========== Users Login ========== //
module.exports.login = async (req, res) => {
  try {
    const { role, email_Id, mobile_No, password } = req.body;

    const userExist = await RegisterModel.findOne({
      $and: [{ role }, { $or: [{ email_Id }, { mobile_No }] }],
    });

    if (!userExist) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "User Not Exists!!!" },
      });
    }

    if (userExist) {
      const verifyPwd = await bcrypt.compare(password, userExist.password);
      if (verifyPwd) {
        const token = generateAccessToken(userExist._id, userExist.role);
        return res.status(201).send({
          statusCode: 201,
          data: { message: "User LoggedIn Successfully!!!", token },
        });
      }
      return res.status(500).send({
        statusCode: 500,
        data: { message: "Invalid Credentials!!!" },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
