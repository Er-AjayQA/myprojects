// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const registerController = require("./register.controller");

// ========== Routes ========== //
router.post("/register", registerController.register);
router.post("/login", registerController.login);

module.exports = router;
