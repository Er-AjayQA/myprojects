// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const productController = require("./product.controller");

// ========== Routes ========== //
router.post("/add_product", productController.addProduct);
router.put("/update_product/:prod_id", productController.updateProduct);
router.put("/publish_product/:prod_id", productController.publishProduct);
router.delete("/delete_product/:prod_id", productController.deleteProduct);
router.get("/getAllProduct", productController.getAllProduct);
router.get("/getProductByFilter", productController.getProductByFilter);
router.get("/searchProductByName", productController.searchProductByName);

// ========== Export Routes ========== //
module.exports = router;
