// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define SubCategory Schema ========== //
const productSchema = new Schema(
  {
    cat_id: {
      type: Schema.Types.ObjectId,
      ref: "productCategory",
    },
    subCat_id: {
      type: Schema.Types.ObjectId,
      ref: "productSubCategory",
    },
    brand_id: {
      type: Schema.Types.ObjectId,
      ref: "productBrand",
    },
    sku: {
      type: String,
      required: true,
      unique: true,
    },
    product_name: {
      type: String,
      required: true,
    },
    unit_price: {
      type: Number,
      required: true,
    },
    isGST: {
      type: Boolean,
      default: false,
    },
    gst: {
      type: Number,
    },
    discount: {
      type: Number,
    },
    netAmount: {
      type: Number,
    },
    stock_count: {
      type: Number,
    },
    isPublished: {
      type: Boolean,
      default: false,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== Define SubCategory Model ========== //
module.exports = new mongoose.model("products", productSchema);
