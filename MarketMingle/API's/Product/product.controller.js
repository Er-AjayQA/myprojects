// ========== IMPORTS and CONFIGS ========== //
const { toFloat } = require("validator");
const productModel = require("./product.model");
const categoryModel = require("../Product_Category/category.model");
const subCategoryModel = require("../Product_SubCategory/subcategory.model");
const brandModel = require("../Product_Brands/brand.model");

// ========== Add Product ========== //
module.exports.addProduct = async (req, res) => {
  try {
    const {
      cat_id,
      subCat_id,
      brand_id,
      sku,
      product_name,
      unit_price,
      isGST,
      gst,
      discount,
      stock_count,
      isPublished,
    } = req.body;

    const isCategoryExist = await categoryModel.findById(cat_id, {
      isDeleted: false,
    });
    if (!isCategoryExist) {
      return res.status(400).send({
        statusCode: 400,
        data: {
          error: { message: "Product Category Not Existing!!!" },
        },
      });
    }

    const isSubCategoryExist = await subCategoryModel.findById(subCat_id, {
      cat_id,
      isDeleted: false,
    });
    if (!isSubCategoryExist) {
      return res.status(400).send({
        statusCode: 400,
        data: {
          error: { message: "Product SubCategory Not Existing!!!" },
        },
      });
    }

    const isBrandExist = await brandModel.findById(brand_id, {
      isDeleted: false,
    });

    if (!isBrandExist) {
      return res.status(400).send({
        statusCode: 400,
        data: {
          error: { message: "Product Brand Not Existing!!!" },
        },
      });
    }

    const getNetAmount = () => {
      let gstAmount = 0;
      let discAmount = 0;
      if (isGST && discount) {
        gstAmount = unit_price + unit_price * toFloat(`.${gst}`);
        discAmount = gstAmount - gstAmount * toFloat(`.${discount}`);
        return discAmount;
      } else if (discount) {
        discAmount = unit_price * toFloat(`.${discount}`);
        return unit_price - discAmount;
      } else if (isGST) {
        gstAmount = unit_price * toFloat(`.${gst}`);
        return unit_price + gstAmount;
      } else {
        return unit_price;
      }
    };

    const add_product = await new productModel({
      product_name,
      sku,
      cat_id,
      subCat_id,
      brand_id,
      unit_price,
      isGST,
      gst,
      discount,
      netAmount: getNetAmount(),
      stock_count,
      isPublished,
    }).save();
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Added Successfully!!!",
        productDetails: add_product,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update Product ========== //
module.exports.updateProduct = async (req, res) => {
  try {
    const { prod_id } = req.params;
    const {
      product_name,
      sku,
      cat_id,
      subCat_id,
      brand_id,
      unit_price,
      isGST,
      gst,
      discount,
      stock_count,
      isPublished,
    } = req.body;

    const productExisting = await productModel.findById({ _id: prod_id });

    if (!productExisting || productExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: { message: `Product Not Found!!!` },
      });
    }

    const isProductSkuExist = await productModel.findOne({
      sku,
      _id: { $ne: prod_id },
    });

    if (isProductSkuExist) {
      return res.status(409).send({
        statusCode: 409,
        error: {
          message: "Product SKU Already Exist!!!",
        },
      });
    }

    const isCategoryExist = await categoryModel.findById({ _id: cat_id });
    if (!isCategoryExist || isCategoryExist.isDeleted) {
      return res.status(400).send({
        statusCode: 400,
        data: {
          error: { message: "Product Category Not Existing!!!" },
        },
      });
    }

    const isSubCategoryExist = await subCategoryModel.findOne({
      subCat_id,
      cat_id,
      isDeleted: false,
    });
    if (!isSubCategoryExist) {
      return res.status(400).send({
        statusCode: 400,
        data: {
          error: { message: "Product SubCategory Not Existing!!!" },
        },
      });
    }

    const isBrandExist = await brandModel.findOne({ _id: brand_id });

    if (!isBrandExist || isBrandExist.isDeleted) {
      return res.status(400).send({
        statusCode: 400,
        data: {
          error: { message: "Product Brand Not Existing!!!" },
        },
      });
    }

    const getNetAmount = () => {
      let gstAmount = 0;
      let discAmount = 0;
      if (isGST && discount) {
        gstAmount = unit_price + unit_price * toFloat(`.${gst}`);
        discAmount = gstAmount - gstAmount * toFloat(`.${discount}`);
        return discAmount;
      } else if (discount) {
        discAmount = unit_price * toFloat(`.${discount}`);
        return unit_price - discAmount;
      } else if (isGST) {
        gstAmount = unit_price * toFloat(`.${gst}`);
        return unit_price + gstAmount;
      } else {
        return unit_price;
      }
    };

    const update_product = await productModel.findByIdAndUpdate(
      { _id: prod_id },
      {
        product_name,
        sku,
        cat_id,
        subCat_id,
        brand_id,
        unit_price,
        isGST,
        gst,
        discount,
        netAmount: getNetAmount(),
        stock_count,
        isPublished,
      },
      { new: true }
    );
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Updated Successfully!!!",
        productDetails: update_product,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Delete Product ========== //
module.exports.deleteProduct = async (req, res) => {
  try {
    const { prod_id } = req.params;
    const isProductsExisting = await productModel.findById({ _id: prod_id });

    if (!isProductsExisting || isProductsExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Not Found!!!",
        },
      });
    }

    const update_product = await productModel.findByIdAndUpdate(
      { _id: prod_id },
      {
        isPublished: false,
        isDeleted: true,
      },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Deleted Successfully!!!",
        deletedProduct: update_product,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Publish/UnPublish Product ========== //
module.exports.publishProduct = async (req, res) => {
  try {
    const { prod_id } = req.params;
    const isProductsExisting = await productModel.findById({ _id: prod_id });

    if (!isProductsExisting || isProductsExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Not Found!!!",
        },
      });
    }

    const update_product = await productModel.findByIdAndUpdate(
      { _id: prod_id },
      {
        isPublished: !isProductsExisting.isPublished,
      },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: `Product ${
          update_product.isPublished ? "Published" : "UnPublished"
        } Successfully!!!`,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Product ========== //
module.exports.getAllProduct = async (req, res) => {
  try {
    const getAllProduct = await productModel.find({
      isDeleted: false,
    });

    if (!getAllProduct || getAllProduct.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Products Not Found!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: "Products List!!!",
        productList: getAllProduct,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Products By Filter ========== //
module.exports.getProductByFilter = async (req, res) => {
  try {
    const query = { isDeleted: false };
    const { filter } = req.query;

    if (filter === "Published") {
      query.isPublished = true;
    } else if (filter === "UnPublished") {
      query.isPublished = false;
    } else if (filter === "Category") {
      query.isPublished = false;
    }

    const getFilteredProduct = await productModel.aggregate([
      { $match: query },
    ]);

    if (!getFilteredProduct || getFilteredProduct.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Products Found!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: `All ${
          query.isPublished ? "Published" : "UnPublished"
        } Products List!!!`,
        productList: getFilteredProduct,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Products By Search Filter ========== //
module.exports.searchProductByName = async (req, res) => {
  try {
    const searchText = req.query.searchText.toLowerCase();
    console.log(searchText);

    const getProduct = await productModel.aggregate([
      {
        $match: {
          product_name: { $regex: searchText, $options: "i" },
        },
      },
    ]);

    if (!getProduct || getProduct.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Products Found!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: `All Products List!!!`,
        productList: getProduct,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
