// ========== IMPORTS and CONFIGS ========== //
const reviewsModel = require("./reviews.model");

// ========== Create Reviews ========== //
module.exports.createReview = async (req, res) => {
  try {
    const { prod_id, customer_id, rev_title, images, ratings, review_desc } =
      req.body;

    const lastReview = await reviewsModel.findOne().sort({ _id: -1 });
    let rev_id = lastReview ? lastReview.id + 1 : 1;

    const create_review = await new reviewsModel({
      rev_id,
      prod_id,
      customer_id,
      rev_title,
      images,
      ratings,
      review_desc,
    }).save();
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Review Added Successfully!!!",
        reviewDetails: create_review,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// // ========== Update Reviews ========== //
// module.exports.updateBrand = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { brandName } = req.body;
//     const brandAlreadyExisting = await brandModel.findOne({
//       id,
//       isDeleted: false,
//     });

//     if (!brandAlreadyExisting || brandAlreadyExisting.isDeleted) {
//       return res.status(404).send({
//         statusCode: 404,
//         error: { message: `Product Brand Not Found!!!` },
//       });
//     }

//     const isAlreadyExisting = await brandModel.findOne({
//       brandName,
//       isDeleted: false,
//       id: { $ne: id },
//     });

//     if (isAlreadyExisting) {
//       return res.status(409).send({
//         statusCode: 409,
//         error: { message: `Product Brand Already Existing!!!` },
//       });
//     } else {
//       const update_brand = await brandModel.findOneAndUpdate(
//         { id },
//         { brandName },
//         { new: true }
//       );
//       return res.status(201).send({
//         statusCode: 201,
//         data: {
//           message: "Product  Brand Updated Successfully!!!",
//           brandDetails: update_brand,
//         },
//       });
//     }
//   } catch (error) {
//     return res.status(500).send({
//       statusCode: 500,
//       error: { message: error.message },
//     });
//   }
// };

// ========== Delete Reviews ========== //
module.exports.deleteReview = async (req, res) => {
  try {
    const { rev_id } = req.params.id;
    const isReviewExisting = await reviewsModel.findOne({
      rev_id,
      isDeleted: false,
    });

    if (!isReviewExisting) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Review Not Found!!!",
        },
      });
    }

    const update_review = await reviewsModel.findOneAndUpdate(
      { rev_id },
      { isDeleted: !isReviewExisting.isDeleted },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Review Deleted Successfully!!!",
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Reviews ========== //
module.exports.getAllReviews = async (req, res) => {
  try {
    const getAllBrand = await brandModel.find({ isDeleted: false });

    if (!getAllBrand || getAllBrand.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Brand Not Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Brand Details!!!",
        brandDetails: getAllBrand,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Reviews By Product ========== //
module.exports.getReviewsByProduct = async (req, res) => {
  try {
    const getAllActiveBrand = await brandModel.find({
      isActive: true,
      isDeleted: false,
    });

    if (!getAllActiveBrand || getAllActiveBrand.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Active Product Brands Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "All Active Product Brands Details!!!",
        brandDetails: getAllActiveBrand,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
