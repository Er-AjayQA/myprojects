// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define Reviews Schema ========== //
const reviewSchema = new Schema(
  {
    rev_id: {
      type: Number,
      required: true,
      unique: true,
    },
    prod_id: {
      type: Schema.Types.Number,
      ref: "products",
      required: true,
    },
    user_id: {
      type: Schema.Types.Number,
      ref: "customers",
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    images: [{ type: String }],
    ratings: {
      type: Number,
      min: 1,
      max: 5,
      required: true,
    },
    review_desc: {
      type: String,
      required: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== Define Reviews Model ========== //
module.exports = new mongoose.model("productReviews", reviewSchema);
