// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const reviewsController = require("./reviews.controller");

// ========== Routes ========== //
router.post("/create_review", reviewsController.createReview);
// router.put("/update_Brand/:id", reviewsController.updateBrand);
router.delete("/delete_Brand/:id", reviewsController.deleteReview);
router.get("/getAllReviews", reviewsController.getAllReviews);
router.get("/getReviewsByProduct", reviewsController.getReviewsByProduct);

// ========== Export Routes ========== //
module.exports = router;
