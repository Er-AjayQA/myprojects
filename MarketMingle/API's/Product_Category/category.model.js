// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define Category Schema ========== //
const categorySchema = new Schema(
  {
    categoryName: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== Define Category Model ========== //
module.exports = new mongoose.model("productCategory", categorySchema);
