// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const categoryController = require("./category.controller");

// ========== Routes ========== //
router.post("/create_category", categoryController.createCategory);
router.put("/update_category/:cat_id", categoryController.updateCategory);
router.put(
  "/update_category_status/:cat_id",
  categoryController.updateCategoryStatus
);
router.delete("/delete_category/:cat_id", categoryController.deleteCategory);
router.get("/getAllCategory", categoryController.getAllCategory);
router.get("/getActiveCategory", categoryController.getAllActiveCategory);

// ========== Export Routes ========== //
module.exports = router;
