// ========== IMPORTS and CONFIGS ========== //
const categoryModel = require("./category.model");

// ========== Create Category ========== //
module.exports.createCategory = async (req, res) => {
  try {
    const { categoryName } = req.body;
    const categoryAlreadyExisting = await categoryModel.findOne({
      categoryName,
      isDeleted: false,
    });

    if (categoryAlreadyExisting) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product Category Already Existing!!!` },
      });
    }

    const create_category = await new categoryModel({
      categoryName,
    }).save();
    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Category Created Successfully!!!",
        categoryDetails: create_category,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update Category ========== //
module.exports.updateCategory = async (req, res) => {
  try {
    const { cat_id } = req.params;
    const { categoryName } = req.body;
    const categoryAlreadyExisting = await categoryModel.findById({
      _id: cat_id,
    });

    if (!categoryAlreadyExisting || categoryAlreadyExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: { message: `Product Category Not Found!!!` },
      });
    }

    const isAlreadyExisting = await categoryModel.findOne({
      categoryName,
      isDeleted: false,
      _id: { $ne: cat_id },
    });

    if (isAlreadyExisting) {
      return res.status(409).send({
        statusCode: 409,
        error: { message: `Product Category Already Existing!!!` },
      });
    } else {
      const update_category = await categoryModel.findByIdAndUpdate(
        { _id: cat_id },
        { categoryName },
        { new: true }
      );
      return res.status(201).send({
        statusCode: 201,
        data: {
          message: "Product  Category Updated Successfully!!!",
          brandDetails: update_category,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Update Category Status ========== //
module.exports.updateCategoryStatus = async (req, res) => {
  try {
    const { cat_id } = req.params;
    const isCategoryExisting = await categoryModel.findById({ _id: cat_id });

    if (!isCategoryExisting || isCategoryExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Category Not Found!!!",
        },
      });
    }

    const update_category = await categoryModel.findByIdAndUpdate(
      { _id: cat_id },
      { isActive: !isCategoryExisting.isActive },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Category Status Updated Successfully!!!",
        categoryDetails: update_category,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Delete Category ========== //
module.exports.deleteCategory = async (req, res) => {
  try {
    const { cat_id } = req.params;
    const isCategoryExisting = await categoryModel.findById({ _id: cat_id });

    if (!isCategoryExisting || isCategoryExisting.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Category Not Found!!!",
        },
      });
    }

    const update_category = await categoryModel.findByIdAndUpdate(
      { _id: cat_id },
      { isDeleted: true, isActive: false },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Category Deleted Successfully!!!",
        deletedCategory: update_category,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Category ========== //
module.exports.getAllCategory = async (req, res) => {
  try {
    const getAllCategory = await categoryModel.find({ isDeleted: false });

    if (!getAllCategory || getAllCategory.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Category Not Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Product Category Details!!!",
        categoryDetails: getAllCategory,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Active Category ========== //
module.exports.getAllActiveCategory = async (req, res) => {
  try {
    const getAllActiveCategory = await categoryModel.find({
      isActive: true,
      isDeleted: false,
    });

    if (!getAllActiveCategory || getAllActiveCategory.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "No Active Product Category Found!!!",
        },
      });
    }

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "All Active Product Category Details!!!",
        categoryDetails: getAllActiveCategory,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
