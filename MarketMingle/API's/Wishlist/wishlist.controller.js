// ========== IMPORTS and CONFIGS ========== //
const wishlistModel = require("./wishlist.model");
const userModel = require("../Register/register.model");
const productModel = require("../Product/product.model");
const mongoose = require("mongoose");

// ========== Add Product To Wishlist ========== //
module.exports.addToWishlist = async (req, res) => {
  try {
    const { user_id } = req.params;
    const { prod_id } = req.body;

    const userExist = await userModel.findOne({
      _id: user_id,
      isSubscribed: true,
    });

    if (!userExist) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const productExist = await productModel.findById(prod_id);

    if (!productExist || productExist.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Not Existing!!!",
        },
      });
    }

    const userWishlist = await wishlistModel.findOne({
      user_id,
      isDeleted: false,
    });

    if (userWishlist) {
      // Check if the product already exists in the wishlist
      const productExistsInWishlist = userWishlist.products.some(
        (product) => product.prod_id.toString() === prod_id.toString()
      );
      if (productExistsInWishlist) {
        return res.status(409).send({
          statusCode: 409,
          error: {
            message: "Product Already Exists in Wishlist!!!",
          },
        });
      }
      const add_to_wishlist = await wishlistModel.findOneAndUpdate(
        { _id: userWishlist._id },
        { $push: { products: { prod_id: prod_id } } },
        { new: true }
      );
      return res.status(201).send({
        statusCode: 201,
        data: {
          message: "Product Added to Wishlist!!!",
          userWishlist: add_to_wishlist,
        },
      });
    }

    const create_new_wishlist = await new wishlistModel({
      user_id,
      products: [{ prod_id }],
    }).save();

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Added to Wishlist Successfully!!!",
        userWishlist: create_new_wishlist,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Delete Product From Wishlist ========== //
module.exports.removeFromWishlist = async (req, res) => {
  try {
    const { user_id, prod_id } = req.params;

    const userExist = await userModel.findOne({
      _id: user_id,
      isSubscribed: true,
    });
    if (!userExist) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Found!!!",
        },
      });
    }

    const isWishlistExisting = await wishlistModel.findOne({
      user_id,
      isDeleted: false,
    });

    if (!isWishlistExisting) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Wishlist Not Found!!!",
        },
      });
    }

    const isProductExistInWishlist = await wishlistModel.findOne({
      user_id,
      isDeleted: false,
      "products.prod_id": prod_id,
    });

    if (!isProductExistInWishlist) {
      return res.status(200).send({
        statusCode: 200,
        error: {
          message: "Product Not Found In Wishlist!!!.",
        },
      });
    }

    const remove_product = await wishlistModel.findOneAndUpdate(
      { user_id: isWishlistExisting.user_id, isDeleted: false },
      { $pull: { products: { prod_id: prod_id } } },
      { new: true }
    );

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: "Product removed from wishlist successfully.",
        removedProduct: remove_product,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Product ========== //
module.exports.getAllWishlist = async (req, res) => {
  try {
    const getAllWishlistData = await wishlistModel
      .find()
      .populate("user_id", ["firstName", "lastName"])
      .populate("products.prod_id", ["product_name", "netAmount"]);

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: "Products List!!!",
        productList: getAllWishlistData,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Wishlist By Customer ========== //
module.exports.getCustomerWishlist = async (req, res) => {
  try {
    const { user_id } = req.params;

    const getCustomerWishlist = await wishlistModel
      .findOne({ user_id })
      .populate("user_id", ["firstName", "lastName"])
      .populate("products.prod_id", ["product_name", "netAmount"]);

    if (getCustomerWishlist.products.length <= 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Wishlist is Empty!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: `Get Customer Wishlist!!!`,
        wishlistList: getCustomerWishlist,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
