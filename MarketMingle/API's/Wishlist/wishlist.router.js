// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const wishlistController = require("./wishlist.controller");

// ========== Routes ========== //
router.post("/addToWishlist/:user_id", wishlistController.addToWishlist);
router.delete(
  "/removeFromWishlist/:user_id/:prod_id",
  wishlistController.removeFromWishlist
);
router.get("/getAllWishlist", wishlistController.getAllWishlist);
router.get(
  "/getCustomerWishlist/:user_id",
  wishlistController.getCustomerWishlist
);

// ========== Export Routes ========== //
module.exports = router;
