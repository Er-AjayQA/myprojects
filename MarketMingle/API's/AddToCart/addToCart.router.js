// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const addToCartController = require("./addToCart.controller");

// ========== Routes ========== //
router.post("/addToCart/:user_id", addToCartController.addToCart);
// router.put("/save_for_later/:cart_id", addToCartController.addForLater);
router.delete(
  "/removeFromCart/:user_id/:prod_id",
  addToCartController.removeFromCart
);
router.get("/getAllActiveCart", addToCartController.getAllActiveCart);
router.get("/getUserCart/:user_id", addToCartController.getUserCart);
// router.get(
//   "/getCustomerSavedProduct/:customer_id",
//   addToCartController.getCustomerSavedProduct
// );

// ========== Export Routes ========== //
module.exports = router;
