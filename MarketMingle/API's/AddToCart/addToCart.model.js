// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define SubCategory Schema ========== //
const cartSchema = new Schema(
  {
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "registrations",
    },
    products: [
      {
        prod_id: {
          type: Schema.Types.ObjectId,
          ref: "products",
        },
        qty: {
          type: Number,
        },
      },
    ],
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== Define SubCategory Model ========== //
module.exports = new mongoose.model("cart", cartSchema);
