// ========== IMPORTS and CONFIGS ========== //
const cartModel = require("./addToCart.model");
const userModel = require("../Register/register.model");
const productModel = require("../Product/product.model");

// ========== Add Product To Cart ========== //
module.exports.addToCart = async (req, res) => {
  try {
    const { user_id } = req.params;
    const { prod_id, qty } = req.body;

    const userExist = await userModel.findById({ _id: user_id });

    if (!userExist || !userExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const productExist = await productModel.findById({ _id: prod_id });

    if (!productExist || productExist.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Not Existing!!!",
        },
      });
    }

    const userCartExist = await cartModel.findOne({
      user_id,
      isDeleted: false,
    });

    if (userCartExist) {
      const productExistInCart = await cartModel.findOne({
        _id: userCartExist._id,
        user_id,
        isDeleted: false,
        "products.prod_id": { $in: [prod_id] },
      });

      if (productExistInCart) {
        return res.status(409).send({
          statusCode: 409,
          data: {
            message: "Product Already Exist In Cart!!!",
          },
        });
      }

      const add_to_cart = await cartModel.findOneAndUpdate(
        { _id: userCartExist._id },
        { $push: { products: { prod_id: prod_id, qty: qty ? qty : 1 } } },
        { new: true }
      );
      return res.status(201).send({
        statusCode: 201,
        data: {
          message: "Added to Cart Successfully!!!",
          userCart: add_to_cart,
        },
      });
    }

    const create_new_cart = await new cartModel({
      user_id,
      products: [{ prod_id, qty: qty || 1 }],
    }).save();

    return res.status(201).send({
      statusCode: 201,
      data: {
        message: "Added to Cart Successfully!!!",
        userCart: create_new_cart,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Remove Product From Cart ========== //
module.exports.removeFromCart = async (req, res) => {
  try {
    const { user_id, prod_id } = req.params;

    const userExist = await userModel.findById({
      _id: user_id,
    });
    if (!userExist || !userExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Found!!!",
        },
      });
    }

    const isCartExisting = await cartModel.findOne({
      user_id,
      isDeleted: false,
    });

    if (!isCartExisting) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Cart Not Found!!!",
        },
      });
    }

    const isProductExistInCart = await cartModel.findOne({
      user_id,
      isDeleted: false,
      "products.prod_id": { $in: [prod_id] },
    });

    if (!isProductExistInCart) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Product Not Found In Cart!!!.",
        },
      });
    }

    const remove_product = await cartModel.findOneAndUpdate(
      {
        _id: isCartExisting._id,
        user_id: isCartExisting.user_id,
        isDeleted: false,
      },
      { $pull: { products: { prod_id: prod_id } } }
    );

    return res.status(404).send({
      statusCode: 404,
      error: {
        message: "Product Removed From Cart!!!.",
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get All Active Cart ========== //
module.exports.getAllActiveCart = async (req, res) => {
  try {
    const getAllCart = await cartModel
      .find({ isDeleted: false })
      .populate("user_id", ["firstName", "lastName"]);

    if (!getAllCart || getAllCart.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Carts Not Found!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: "Products List!!!",
        productList: getAllCart,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};

// ========== Get Cart By User ========== //
module.exports.getUserCart = async (req, res) => {
  try {
    const { user_id } = req.params;

    const userExist = await userModel.findById({ _id: user_id });

    if (!userExist || !userExist.isSubscribed) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Not Registered!!!",
        },
      });
    }

    const getUserCart = await cartModel
      .findOne({ user_id: user_id, isDeleted: false })
      .populate("user_id", ["firstName", "lastName"])
      .populate("products.prod_id", ["product_name", "netAmount"]);

    if (!getUserCart) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "User Cart Not Exist!!!",
        },
      });
    }

    if (getUserCart.products.length <= 0) {
      return res.status(404).send({
        statusCode: 404,
        error: {
          message: "Cart is Empty!!!",
        },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: `Get User Cart!!!`,
        cartList: getUserCart,
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      error: { message: error.message },
    });
  }
};
