// ========== IMPORTS and CONFIGS ========== //
const jwt = require("jsonwebtoken");

const generateAccessToken = (id, role) => {
  return jwt.sign({ id, role }, process.env.SECRETE);
};

// const throwError = (err) => {
//   res.status(500).send({
//     statusCode: 500,
//     data: { error: err },
//   });
// };

// const alreadyExistResponse = (status, message) => {
//   res.status(status).send({
//     statusCode: status,
//     data: { message: `${message} Already Existing!!!` },
//   });
// };

module.exports = { generateAccessToken };
