// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");

// ========== Importing Routers ========== //
const registerRouter = require("./API's/Register/register.router");
const categoryRouter = require("./API's/Product_Category/category.router");
const subcategoryRouter = require("./API's/Product_SubCategory/subcategory.router");
const productRouter = require("./API's/Product/product.router");
const brandRouter = require("./API's/Product_Brands/brand.router");
const shippingAddressRouter = require("./API's/Shipping_Address/shippingAddress.router");
const wishlistRouter = require("./API's/Wishlist/wishlist.router");
const cartRouter = require("./API's/AddToCart/addToCart.router");
const orderRouter = require("./API's/Order/order.router");

// ========== COMMON CONFIG ========== //
const app = express();
const port = process.env.PORT;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ========== HOME ROUTES ========== //
app.get("/api/market_mingle", (req, res) => {
  try {
    return res.status(200).send({
      statusCode: 200,
      data: { message: "Welcome to the Store Sphere backend" },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error },
    });
  }
});

// ========== DB SYNC ========== //
mongoose
  .connect(process.env.DB_URI)
  .then(() => {
    console.log("Connected to database successfully");
  })
  .catch((err) => {
    console.error(err);
  });

// ========== ROUTES ========== //
app.use("/api/market_mingle", registerRouter);
app.use("/api/market_mingle", categoryRouter);
app.use("/api/market_mingle", subcategoryRouter);
app.use("/api/market_mingle", productRouter);
app.use("/api/market_mingle", brandRouter);
app.use("/api/market_mingle", shippingAddressRouter);
app.use("/api/market_mingle", wishlistRouter);
app.use("/api/market_mingle", cartRouter);
app.use("/api/market_mingle", orderRouter);

// ========== LISTENING SERVER ========== //
app.listen(port, (err) => {
  if (!err) {
    console.log(`Listening to the server at port : ${port}`);
  } else {
    console.log(err);
  }
});
