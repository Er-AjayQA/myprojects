import pokemonStyle from "./index.module.css";

export const PokemonCards = ({ pokemonData }) => {
  return (
    <>
      <li className={pokemonStyle["pokemon-card"]}>
        <figure>
          <img
            src={pokemonData.sprites.other.dream_world.front_default}
            alt={pokemonData.name}
            className={pokemonStyle["pokemon-image"]}
          />
        </figure>
        <h1 className={pokemonStyle["pokemon-name"]}>{pokemonData.name}</h1>
        <div className={pokemonStyle["pokemon-highlight"]}>
          <p>
            {pokemonData.types.map((curType) => curType.type.name).join(", ")}
          </p>
        </div>
        <div className={pokemonStyle["grid-three-cols"]}>
          <p className={pokemonStyle["pokemon-info"]}>
            <span>Height: </span>
            {pokemonData.height}
          </p>
          <p className={pokemonStyle["pokemon-info"]}>
            <span>Weight: </span>
            {pokemonData.weight}
          </p>
          <p className={pokemonStyle["pokemon-info"]}>
            <span>Speed: </span>
            {pokemonData.stats[5].base_stat}
          </p>
        </div>
        <div className={pokemonStyle["grid-three-cols"]}>
          <div className={pokemonStyle["pokemon-info"]}>
            <p>{pokemonData.base_experience}</p>
            <span>Experience:</span>
          </div>
          <div className={pokemonStyle["pokemon-info"]}>
            <p>{pokemonData.stats[1].base_stat}</p>
            <span>Attack:</span>
          </div>
          <div className={pokemonStyle["pokemon-info"]}>
            <p>
              {pokemonData.abilities
                .map((abilityInfo) => abilityInfo.ability.name)
                .slice(0, 1)
                .join(", ")}
            </p>
            <span>Abilities:</span>
          </div>
        </div>
      </li>
    </>
  );
};
