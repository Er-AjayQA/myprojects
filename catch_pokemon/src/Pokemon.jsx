import { useEffect, useState } from "react";
import pokemontStyle from "./index.module.css";
import { PokemonCards } from "./PokemonCards";

export const Pokemon = () => {
  const [pokemon, setPokemon] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [search, setSearch] = useState("");

  const api = "https://pokeapi.co/api/v2/pokemon?limit=124";
  const fetchPokemon = async () => {
    try {
      const res = await fetch(api);
      const apiData = await res.json();

      const detailedPokemonData = apiData.results.map(async (curPokemon) => {
        const res = await fetch(curPokemon.url);
        const data = await res.json();
        return data;
      });

      const detailedResponses = await Promise.all(detailedPokemonData);
      setPokemon(detailedResponses);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(error);
    }
  };
  console.log(pokemon);

  useEffect(() => {
    fetchPokemon();
  }, []);

  // Search Functionality
  const searchData = pokemon.filter((curPokemon) =>
    curPokemon.name.toLowerCase().includes(search)
  );

  if (loading) {
    return (
      <div>
        <h1>.........Loading</h1>
      </div>
    );
  }

  if (error) {
    return (
      <div>
        <h1>Error: {error.message}</h1>
      </div>
    );
  }

  return (
    <>
      <section className={pokemontStyle.container}>
        <header>
          <h1>Lets Catch Pokemon</h1>
        </header>
        <div className={pokemontStyle["pokemon-search"]}>
          <input
            type="text"
            placeholder="Search Pokemon"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
        <div>
          <ul className={pokemontStyle.cards}>
            {searchData.map((curPokemon) => {
              return (
                <PokemonCards
                  key={curPokemon.id}
                  pokemonData={curPokemon}
                ></PokemonCards>
              );
            })}
          </ul>
        </div>
      </section>
    </>
  );
};
