// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const port = process.env.PORT;
const usersRouter = require("./APIs/User/Router/user.router");
const booksRouter = require("./APIs/Books/Router/book.router");
const reviewsRouter = require("./APIs/Review/Router/review.router");

// ========== COMMON CONFIG ========== //
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ========== HOME ROUTES ========== //
app.get("/project/booksManagement", (req, res) => {
  try {
    return res.status(200).send({
      statusCode: 200,
      data: { message: "Welcome to Books Management Backend." },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
});

// ========== DB SYNC ========== //
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    console.log("Connected to database successfully");
  })
  .catch((err) => {
    console.error(err);
  });

// ========== ROUTER ========== //
app.use("/project/booksManagement", usersRouter);
app.use("/project/booksManagement", booksRouter);
app.use("/project/booksManagement", reviewsRouter);

// ========== LISTENING SERVER ========== //
app.listen(port, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Listening to the server at port : ${port}`);
  }
});
