// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const validator = require("validator");

// ========== Define Users Schema ========== //
const userSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      enum: {
        values: ["Mr", "Mrs", "Miss"],
        message: "{VALUE} is not supported. Allowed values are Mr, Mrs, Miss",
      },
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: [validator.isEmail, "Please enter a valid email id."],
    },
    password: {
      type: String,
      required: true,
      minLength: 8,
      maxLength: 15,
    },
    address: {
      type: String,
    },
    street: {
      type: String,
    },
    city: {
      type: String,
    },
    pinCode: {
      type: String,
    },
  },
  { timestamps: true }
);

// ========== EXPORTS ========== //
module.exports = new mongoose.model("users", userSchema);
