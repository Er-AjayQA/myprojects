// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const usersController = require("../Controller/user.controller");

// ========== ROUTES ========== //
router.post("/register", usersController.register);
router.post("/login", usersController.login);

// ========== EXPORTS ========== //
module.exports = router;
