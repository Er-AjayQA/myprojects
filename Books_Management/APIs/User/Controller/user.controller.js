// ========== IMPORTS and CONFIGS ========== //
const Users = require("../Model/user.model");
const jwt = require("jsonwebtoken");
require("dotenv").config();

// ========== Create Users ========== //
module.exports.register = async (req, res) => {
  try {
    const {
      title,
      name,
      phone,
      email,
      password,
      address,
      street,
      city,
      pinCode,
    } = req.body;

    const userExist = await Users.find({ $or: [{ phone }, { email }] });

    if (userExist.length > 0) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "User Already Exist" },
      });
    }

    const createUser = new Users({
      title,
      name,
      phone,
      email,
      password,
      address,
      street,
      city,
      pinCode,
    });
    await createUser.save();
    return res.status(201).send({
      statusCode: 201,
      data: { message: "User Created Successfully", user: createUser },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ========== Users Login ========== //
module.exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const userExist = await Users.findOne({ email });

    if (!userExist) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "User Not Exist" },
      });
    }

    if (password !== userExist.password) {
      return res.status(500).send({
        statusCode: 500,
        data: { message: "Invalid Credentials" },
      });
    } else {
      const token = jwt.sign({ userId: userExist._id }, process.env.SECRETE);
      return res.status(200).send({
        statusCode: 200,
        data: {
          message: "User LoggedIn Successfully",
          token: token,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
