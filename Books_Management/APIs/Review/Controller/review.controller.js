// ========== IMPORTS and CONFIGS ========== //
const Books = require("../../Books/Model/book.model");
const Reviews = require("../Model/review.model");

// ========== Add Book Review ========== //
module.exports.addReview = async (req, res) => {
  try {
    const { bookId } = req.params;

    const bookExist = await Books.findById({ _id: bookId });

    if (!bookExist || bookExist.isDeleted === true) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Book Found" },
      });
    }

    const { reviewedBy, rating, review } = req.body;

    const addReview = new Reviews({
      bookId,
      reviewedBy,
      rating,
      review,
      reviewedAt: new Date(),
    });
    const saveReview = await addReview.save();
    return res.status(201).send({
      statusCode: 201,
      data: { message: "Review Added Successfully", addReview },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ========== Update Book Review ========== //
module.exports.updateReview = async (req, res) => {
  try {
    const { bookId, reviewId } = req.params;

    const bookExist = await Books.findById({ _id: bookId });

    if (!bookExist || bookExist.isDeleted === true) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Book Found" },
      });
    }

    const reviewExist = await Reviews.findOne({
      bookId: bookId,
      _id: reviewId,
    });

    if (!reviewExist || reviewExist.isDeleted === true) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Reviews Found" },
      });
    }

    const { reviewedBy, rating, review } = req.body;

    const updateReview = await Reviews.findByIdAndUpdate(
      { _id: reviewId },
      { bookId, reviewedBy, rating, review },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: { message: "Review Updated Successfully", updateReview },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ========== Delete Book Review ========== //
module.exports.deleteReview = async (req, res) => {
  try {
    const { bookId, reviewId } = req.params;

    const bookExist = await Books.findById({ _id: bookId });

    if (!bookExist || bookExist.isDeleted === true) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Book Found" },
      });
    }

    const reviewExist = await Reviews.findOne({
      bookId: bookId,
      _id: reviewId,
    });

    if (!reviewExist || reviewExist.isDeleted === true) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Reviews Found" },
      });
    }

    const deleteReview = await Reviews.findByIdAndUpdate(
      { _id: reviewId },
      { isDeleted: true },
      { new: true }
    );

    return res.status(202).send({
      statusCode: 202,
      data: { message: "Review Deleted Successfully", deleteReview },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
