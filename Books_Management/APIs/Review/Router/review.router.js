// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const reviewsController = require("../Controller/review.controller");

// ========== ROUTES ========== //
router.post("/books/:bookId/review", reviewsController.addReview);
router.put("/books/:bookId/review/:reviewId", reviewsController.updateReview);
router.delete(
  "/books/:bookId/review/:reviewId",
  reviewsController.deleteReview
);

module.exports = router;
