// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const validator = require("validator");
const Schema = mongoose.Schema;

// ========== Define Reviews Schema ========== //
const reviewsSchema = new mongoose.Schema(
  {
    bookId: {
      type: Schema.Types.ObjectId,
      required: true,
      refs: "books",
    },
    reviewedBy: {
      type: String,
      required: true,
      default: "Guest",
    },
    reviewedAt: {
      type: Date,
      required: true,
    },
    rating: {
      type: Number,
      min: 1,
      max: 5,
      required: true,
    },
    review: {
      type: String,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// ========== EXPORTS ========== //
module.exports = new mongoose.model("reviews", reviewsSchema);
