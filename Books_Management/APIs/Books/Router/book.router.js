// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const booksController = require("../Controller/book.controller");
const authenticator = require("../../../middlewares/auth");

// ========== ROUTES ========== //
router.post("/books", authenticator, booksController.createBooks);
router.get("/books", booksController.getAllBooks);
router.get("/books/:bookId", booksController.getBookById);
router.put("/books/:bookId", authenticator, booksController.editBookDetail);
router.delete("/books/:bookId", authenticator, booksController.deleteBook);

module.exports = router;
