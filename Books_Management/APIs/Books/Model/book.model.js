// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const validator = require("validator");
const Schema = mongoose.Schema;

// ========== Define Books Schema ========== //
const booksSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
    excerpt: {
      type: String,
      required: true,
    },
    userId: { type: Schema.Types.ObjectId, required: true, refs: "users" },
    ISBN: { type: String, required: true, unique: true },
    category: { type: String, required: true },
    subcategory: {
      type: [String],
      required: true,
    },
    reviews: {
      type: Number,
      default: 0,
      comment: "Holds number of reviews of this book",
    },
    deletedAt: {
      type: Date,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    releasedAt: {
      type: String,
      required: true,
      validate: {
        validator: function (v) {
          return /^\d{4}-\d{2}-\d{2}$/.test(v);
        },
        message: (props) =>
          `${props.value} is not a valid date format (YYYY-MM-DD)!`,
      },
    },
  },
  {
    timestamps: true,
  }
);

// ========== EXPORTS ========== //
module.exports = new mongoose.model("books", booksSchema);
