// ========== IMPORTS and CONFIGS ========== //
const Users = require("../../User/Model/user.model");
const Books = require("../Model/book.model");
const { ObjectId } = require("mongodb");

// ========== Create Books ========== //
module.exports.createBooks = async (req, res) => {
  try {
    let userId = req.user;
    const { title, excerpt, ISBN, category, subcategory, releasedAt } =
      req.body;

    const userExist = await Users.findById({ _id: userId });

    if (!userExist) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "User Not Exist" },
      });
    }

    const createBook = new Books({
      userId,
      title,
      excerpt,
      ISBN,
      category,
      subcategory,
      releasedAt,
    });
    await createBook.save();
    return res.status(201).send({
      statusCode: 201,
      data: { message: "Book Created Successfully", book: createBook },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ========== Get All Books ========== //
module.exports.getAllBooks = async (req, res) => {
  try {
    const { userId, category, subcategory } = req.query;
    let query = { isDeleted: false };
    if (userId) {
      query.userId = userId;
    }
    if (category) {
      query.category = category;
    }
    if (subcategory) {
      query.subcategory = subcategory;
    }

    const getBookByFilter = await Books.find(query)
      .select(
        "_id title excerpt userId category subcategory releasedAt reviews"
      )
      .sort({ title: 1 })
      .exec();

    if (getBookByFilter.length <= 0) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "No Books Found" },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: { message: "All Books Details", getAllBooks: getBookByFilter },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ========== Get Books By Id ========== //
module.exports.getBookById = async (req, res) => {
  try {
    const { bookId } = req.params;
    const getBook = await Books.findById({ _id: bookId });

    if (!getBook || getBook.isDeleted === true) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "No Book Found" },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      data: { message: "All Books Details", getBook },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ========== Edit Book Details ========== //
module.exports.editBookDetail = async (req, res) => {
  try {
    let userId = req.user;
    const { bookId } = req.params;
    const getBook = await Books.findById({ _id: bookId });

    if (!getBook || getBook.isDeleted === true) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "No Book Found" },
      });
    }

    if (new ObjectId(userId).equals(getBook.userId)) {
      const { title, excerpt, releasedAt, ISBN } = req.body;
      const update = await Books.findByIdAndUpdate(
        { _id: bookId },
        { title, excerpt, releasedAt, ISBN },
        { new: true }
      );
      return res.status(201).send({
        statusCode: 201,
        data: { message: "Books Updated Successfully", update },
      });
    } else {
      return res.status(403).send({
        statusCode: 403,
        data: { message: "Not Authorized User" },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ========== Delete Book ========== //
module.exports.deleteBook = async (req, res) => {
  try {
    let userId = req.user;
    const { bookId } = req.params;
    const getBook = await Books.findById({ _id: bookId });

    if (!getBook || getBook.isDeleted === true) {
      return res.status(409).send({
        statusCode: 409,
        data: { message: "No Book Found" },
      });
    }

    if (new ObjectId(userId).equals(getBook.userId)) {
      const deletedBook = await Books.findByIdAndUpdate(
        { _id: bookId },
        { isDeleted: true },
        { new: true }
      );
      return res.status(201).send({
        statusCode: 201,
        data: { message: "Books Deleted Successfully", deletedBook },
      });
    } else {
      return res.status(403).send({
        statusCode: 403,
        data: { message: "Not Authorized User" },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
