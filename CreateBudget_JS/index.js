const addBtn = document.querySelector(".add");
const reviseBtn = document.querySelector(".revise");
const removeBtn = document.querySelector(".remove");
const submitBtn = document.querySelector(".submit");
const increaseBtn = document.querySelector(".add");
const decreaseBtn = document.querySelector(".add");
const addForm = document.querySelector(".createBudget");
const reviseForm = document.querySelector(".reviseBudget");
const budget = document.querySelector(".budgetAmt");
const addAmountInput = document.querySelector(".addAmount");
const reviseAmountInput = document.querySelector(".reviseAmount");
const increaseBudget = document.querySelector(".upArr");
const decreaseBudget = document.querySelector(".downArr");

reviseForm.classList.add("hide");
addForm.classList.add("hide");

// Functions
function displayForm(form) {
  if (form === "Revise") {
    addForm.classList.add("hide");
    reviseForm.classList.remove("hide");
  }
  if (form === "Add") {
    reviseForm.classList.add("hide");
    addForm.classList.remove("hide");
  }
}

function getInitialValue() {
  let initialValue = parseInt(budget.innerText);
  if (isNaN(initialValue)) {
    initialValue = 0; // Default initial value
    return initialValue;
  } else {
    return initialValue;
  }
}

function setValue(amt) {
  budget.innerText = `${getInitialValue() + amt}`;
}

function getReviseAmount() {
  let reviseAmount = parseInt(reviseAmountInput.value);
  if (isNaN(reviseAmount)) {
    return (reviseAmount = 0);
  } else {
    return reviseAmount;
  }
}

// Event Handlers
reviseBtn.addEventListener("click", (e) => {
  e.preventDefault();
  const form = e.target.innerText;
  displayForm(form);
});

addBtn.addEventListener("click", (e) => {
  e.preventDefault();
  const form = e.target.innerText;
  displayForm(form);
});

submitBtn.addEventListener("click", (e) => {
  e.preventDefault();
  let amount = parseInt(addAmountInput.value);

  if (isNaN(amount)) {
    amount = 0;
    setValue(amount);
  } else if (amount > 0) {
    setValue(amount);
  } else {
    alert("-ve values not allowed!");
  }

  addAmountInput.value = "";
});

removeBtn.addEventListener("click", (e) => {
  e.preventDefault();
  budget.innerText = "";
});

increaseBudget.addEventListener("click", (e) => {
  e.preventDefault();
  if (getReviseAmount() > 0) {
    setValue(getReviseAmount());
  } else {
    alert("-ve values not allowed!");
  }
  reviseAmountInput.value = "";
});

decreaseBudget.addEventListener("click", (e) => {
  e.preventDefault();
  let amount = getReviseAmount();
  if (amount > 0) {
    setValue(-amount);
  } else {
    alert("-ve values not allowed!");
  }
  reviseAmountInput.value = "";
});
