// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");

// ========== College Schema ========== //

const collegeSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true,
    },
    fullName: {
      type: String,
      required: true,
    },
    logoLink: {
      type: String,
      required: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("colleges", collegeSchema);
