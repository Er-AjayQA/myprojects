// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const collegeController = require("../Controller/college.controller");

// ========== Routes ========== //
router.post("/createCollege", collegeController.createCollege);
router.get("/collegeDetails", collegeController.collegeDetails);

module.exports = router;
