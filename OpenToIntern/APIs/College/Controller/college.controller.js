// ========== IMPORTS and CONFIGS ========== //
const Colleges = require("../Model/college.model");
const Interns = require("../../Intern/Model/intern.model");

// ================== Get College Id ================== //
const getCollegeId = async function (collegeName) {
  try {
    const getCollegeDetails = await Colleges.findOne({ name: collegeName });
    const collegeId = getCollegeDetails._id;

    if (!collegeId) {
      return "College Not Found";
    } else {
      return collegeId;
    }
  } catch (error) {
    return console.log(error.message);
  }
};
// ================== Create College Details ================== //
module.exports.createCollege = async (req, res) => {
  try {
    const { name, fullName, logoLink } = req.body;

    const collegeExist = await Colleges.findOne({ name });

    // If College with given name exist.
    if (collegeExist) {
      // If College with given name exist and not deleted.
      if (collegeExist.isDeleted === false) {
        return res.status(409).send({
          statusCode: 409,
          data: { message: "College Already Exist" },
        });
      }
      // If College with given name exist and deleted.
      else {
        const createCollege = await Colleges.findOneAndUpdate(
          collegeExist._id,
          { name, fullName, logoLink, isDeleted: false },
          { new: true }
        );
        return res.status(201).send({
          statusCode: 201,
          data: { message: "College Created Successfully", createCollege },
        });
      }
    }

    // If College with given name not exist.
    const createCollege = new Colleges({ name, fullName, logoLink });
    await createCollege.save();

    return res.status(201).send({
      statusCode: 201,
      data: { message: "College Created Successfully", createCollege },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Get College Details ================== //
module.exports.collegeDetails = async (req, res) => {
  try {
    const { collegeName } = req.query;
    const collegeId = await getCollegeId(collegeName);

    if (!collegeId) {
      return res.status(400).send({
        statusCode: 400,
        data: { message: collegeId },
      });
    }

    const getCollege = await Colleges.findById(collegeId);

    const allInterns = await Interns.find({
      $and: [{ collegeId }, { isDeleted: false }],
    });

    return res.status(200).send({
      statusCode: 200,
      data: {
        message: "Get all details",
        college: {
          college: getCollege,
          totalInterns: allInterns.length ? allInterns.length : "No Interns",
          internsDetails: allInterns,
        },
      },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
