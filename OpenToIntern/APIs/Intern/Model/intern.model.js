// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const validator = require("validator");
const Schema = mongoose.Schema;
const { default: isEmail } = require("validator/lib/isEmail");

// ========== College Schema ========== //

const internSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: [validator.isEmail, "Please provide a valid email address"],
    },
    mobile: {
      type: Number,
      unique: true,
      require: true,
      // validate: [validator.isMobilePhone, "Please enter a valid mobile number"],
    },
    collegeId: {
      type: Schema.Types.ObjectId,
      ref: "colleges",
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("interns", internSchema);
