// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const internController = require("../Controller/intern.controller");

// ========== Routes ========== //
router.post("/createIntern", internController.createIntern);

module.exports = router;
