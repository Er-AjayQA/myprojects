// ========== IMPORTS and CONFIGS ========== //
const Interns = require("../Model/intern.model");
const Colleges = require("../../College/Model/college.model");

// ================== Create Intern Details ================== //
module.exports.createIntern = async (req, res) => {
  try {
    const { name, email, mobile, collegeName } = req.body;
    const getCollegeId = await Colleges.findOne({ name: collegeName });
    const collegeId = getCollegeId._id;

    const internExist = await Interns.findOne({ $or: [{ email }, { mobile }] });

    // If Intern with given "email/mobile" exist.
    if (internExist) {
      // If Intern with given "email/mobile" exist and not deleted.
      if (internExist.isDeleted === false) {
        return res.status(409).send({
          statusCode: 409,
          data: { message: "Intern Already Exist" },
        });
      }
      // If Intern with given email/mobile exist and deleted.
      else {
        const createIntern = await Interns.findOneAndUpdate(
          internExist._id,
          { name, email, mobile, collegeId, isDeleted: false },
          { new: true }
        );
        return res.status(409).send({
          statusCode: 409,
          data: { message: "Intern Created Successfully", createIntern },
        });
      }
    }

    // If Intern with given email/mobile not exist.
    const createIntern = new Interns({ name, email, mobile, collegeId });
    await createIntern.save();
    return res.status(201).send({
      statusCode: 201,
      data: { message: "Intern Created Successfully", createIntern },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
