// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");
const collegeRouter = require("./APIs/College/Router/college.router");
const internRouter = require("./APIs/Intern/Router/intern.router");

// ========== COMMON CONFIG ========== //
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const port = process.env.PORT;

// ========== HOME ROUTES ========== //
app.get("/project/internship", (req, res) => {
  try {
    res.status(200).send({
      statusCode: 200,
      data: { message: "Welcome to Internship Backend." },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
});

// ========== DB SYNC ========== //
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    console.log("Connected to database successfully");
  })
  .catch((err) => {
    console.error(err);
  });

// ========== ROUTER ========== //
app.use("/project/internship", collegeRouter);
app.use("/project/internship", internRouter);

// ========== LISTENING SERVER ========== //
app.listen(port, (err) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(`Listening to the server at port : ${port}`);
});
