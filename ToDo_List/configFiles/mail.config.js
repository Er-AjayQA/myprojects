// ========== IMPORTS and CONFIGS ========== //
const nodemailer = require("nodemailer");
require("dotenv").config();

// ========== TRANSPORTER CONFIGS ========== //
const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  secure: false,
  auth: {
    user: process.env.EMAIL_USER, // your email
    pass: process.env.EMAIL_PASS, //
  },
});

// ========== SEND EMAIL FUNCTION ========== //
module.exports.sendEmail = async (to, sub, text) => {
  const mailOptions = {
    from: process.env.EMAIL_USER,
    to: to,
    subject: sub,
    text: text,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log("Error occurred in sending mail:", error);
    } else {
      console.log("Email sent successfully:", info.response);
    }
  });
};
