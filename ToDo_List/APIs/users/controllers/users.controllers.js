const db = require("../../../indexFiles/modelsIndex");
const mail = require("../../../configFiles/mail.config");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const users = db.tbl_users;
const tasks = db.tbl_tasks;

// ===== Create Users ===== //
module.exports.createUsers = async (req, res) => {
  try {
    const { firstName, lastName, emailId, password, confirmPassword } =
      req.body;
    const userExist = await users.findOne({ where: { emailId } });

    if (userExist) {
      res
        .status(500)
        .send({ status: 500, data: { message: "Email already registered." } });
    } else if (password === confirmPassword) {
      const user = await users.create({
        firstName,
        lastName,
        emailId,
        password,
      });

      await mail.sendEmail(
        emailId,
        process.env.REGISTER_MAIL_SUBJECT,
        process.env.REGISTER_MAIL_BODY
      );
      res.status(201).send({
        status: 201,
        data: { message: "User created successfully.", user },
      });
    } else {
      res.status(500).send({
        status: 500,
        data: { message: "Password and Confirm Password should matched." },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: "Something went wrong." },
    });
  }
};

// ===== Update Users ===== //
module.exports.updateUsersDetails = async (req, res) => {
  try {
    const { u_id } = req.params;
    let userId = req.user;

    if (userId != u_id) {
      return res
        .status(403)
        .send({ code: 403, message: "You are not authorized" });
    }

    const { firstName, lastName, emailId, password } = req.body;
    await users.update(
      { firstName, lastName, emailId, password },
      { where: { u_id } }
    );
    const user = await users.findByPk(u_id);
    res.status(201).send({
      status: 201,
      data: { message: "User details updated successfully.", user },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: "Something went wrong." },
    });
  }
};

// ===== User Login ===== //
module.exports.userLogin = async (req, res) => {
  try {
    const { emailId, password } = req.body;
    const userExist = await users.findOne({ where: { emailId } });

    if (!userExist || userExist.isDeleted === true) {
      res
        .status(500)
        .send({ status: 500, data: { message: "User not exist." } });
    } else if (password === userExist.password) {
      // Generate JWT Token
      const token = jwt.sign({ userId: userExist.u_id }, process.env.SECRETE, {
        expiresIn: "15m",
      });
      res.status(200).send({
        status: 200,
        data: { message: "Login Successfully", userExist, token: token },
      });
    } else {
      res
        .status(500)
        .send({ status: 500, data: { message: "Invalid Credentials" } });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: "Something went wrong." },
    });
  }
};

// ===== Get Users All Tasks ===== //
exports.getUsersTasks = async (req, res) => {
  try {
    const { u_id } = req.params;
    let userId = req.user;

    if (userId != u_id) {
      return res
        .status(403)
        .send({ code: 403, message: "You are not authorized" });
    }

    const userTasks = await users.findByPk(u_id, {
      attributes: ["u_id", "firstName", "lastName"],
      include: [
        {
          model: tasks,
          as: "tasks",
          attributes: ["task", "startDate", "endDate", "isCompleted", "status"],
        },
      ],
    });

    res.status(200).send({
      status: 200,
      userId: userTasks.u_id,
      userName: userTasks.firstName.concat(` ${userTasks.lastName}`),
      totalTasks:
        userTasks.tasks.length > 0
          ? `${userTasks.tasks.length}`
          : "No Records Found",
      data: {
        message: userTasks
          ? "All tasks fetched successfully"
          : "Tasks not assigned yet",
        tasks: userTasks.tasks.length > 0 ? userTasks.tasks : "No Tasks",
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: "Something went wrong." },
    });
  }
};

// ===== Get User Tasks By Filter ===== //
module.exports.getTaskByFilters = async (req, res) => {
  try {
    const { u_id } = req.params;
    let userId = req.user;
    let { filter } = req.body;
    console.log(filter);

    if (userId != u_id) {
      return res
        .status(403)
        .send({ code: 403, message: "You are not authorized" });
    }

    let task = [];
    switch (filter) {
      case "Active":
        task = await tasks.findAll({
          where: {
            [db.Sequelize.Op.and]: [
              { status: "Active" },
              { u_id: u_id },
              { isDeleted: false },
            ],
          },
        });
        res.status(200).send({
          status: 200,
          records: task.length,
          data: {
            message: "All Active tasks.",
            task: task.length > 0 ? task : "No Active tasks",
          },
        });
        break;

      case "InActive":
        task = await tasks.findAll({
          where: {
            [db.Sequelize.Op.and]: [
              { status: "InActive" },
              { u_id: u_id },
              { isDeleted: false },
            ],
          },
        });
        res.status(200).send({
          status: 200,
          records: task.length,
          data: {
            message: "All InActive tasks.",
            task: task.length > 0 ? task : "No InActive tasks",
          },
        });
        break;

      case "InComplete":
        task = await tasks.findAll({
          where: {
            [db.Sequelize.Op.and]: [
              { isCompleted: false },
              { u_id: u_id },
              { isDeleted: false },
            ],
          },
        });
        res.status(200).send({
          status: 200,
          records: task.length,
          data: {
            message: "All InComplete tasks.",
            task: task.length > 0 ? task : "No Pending tasks",
          },
        });
        break;

      case "Complete":
        task = await tasks.findAll({
          where: {
            [db.Sequelize.Op.and]: [
              { isCompleted: true },
              { u_id: u_id },
              { isDeleted: false },
            ],
          },
        });
        res.status(200).send({
          status: 200,
          records: task.length,
          data: {
            message: "All Complete tasks.",
            task: task.length > 0 ? task : "No Completed tasks",
          },
        });
        break;

      default:
        task = await tasks.findAll({
          where: { [db.Sequelize.Op.and]: [{ u_id }, { isDeleted: false }] },
        });
        res.status(200).send({
          status: 200,
          records: task.length,
          data: {
            message: "All tasks.",
            task: task.length > 0 ? task : "No tasks",
          },
        });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: "Something went wrong." },
    });
  }
};

// ===== Delete User ===== //
exports.deleteUser = async (req, res) => {
  try {
    const { u_id } = req.params;
    let userId = req.user;

    if (userId != u_id) {
      return res
        .status(403)
        .send({ code: 403, message: "You are not authorized" });
    }

    await users.update(
      { isDeleted: true, status: "InActive" },
      { where: { u_id } }
    );
    await tasks.update(
      { isDeleted: true, status: "InActive" },
      { where: { u_id } }
    );
    res.status(200).send({
      status: 200,
      data: {
        message: "User deleted successfully.",
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: "Something went wrong." },
    });
  }
};
