const express = require("express");
const router = express.Router();
const authenticator = require("../../../middlewares/auth");
const usersController = require("../controllers/users.controllers");

router.post("/createUsers", usersController.createUsers);
router.post("/userLogin", usersController.userLogin);
router.put(
  "/updateUsersDetails/:u_id",
  authenticator,
  usersController.updateUsersDetails
);
router.get("/getUserTasks/:u_id", authenticator, usersController.getUsersTasks);
router.post(
  "/getTaskByFilters/:u_id",
  authenticator,
  usersController.getTaskByFilters
);
router.delete("/deleteUser/:u_id", authenticator, usersController.deleteUser);

module.exports = router;
