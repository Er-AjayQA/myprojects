module.exports = (sequelize, Sequelize) => {
  const tbl_tasks = sequelize.define("tbl_tasks", {
    t_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true,
    },
    task: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    startDate: {
      type: Sequelize.DATE,
      defaultValue: function () {
        return new Date();
      },
    },
    endDate: {
      type: Sequelize.DATE,
      defaultValue: function () {
        return new Date();
      },
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
    isCompleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
  });
  return tbl_tasks;
};
