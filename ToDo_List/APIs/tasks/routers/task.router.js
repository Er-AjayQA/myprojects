const express = require("express");
const router = express.Router();
const authenticator = require("../../../middlewares/auth");
const taskController = require("../controllers/task.controller");

router.post("/createTask/:u_id", authenticator, taskController.createTask);
router.put("/editTask/:u_id/:t_id", authenticator, taskController.editTask);
router.delete(
  "/deleteTask/:u_id/:t_id",
  authenticator,
  taskController.deleteTask
);

module.exports = router;
