const db = require("../../../indexFiles/modelsIndex");
const users = db.tbl_users;
const tasks = db.tbl_tasks;

// ===== Create Task ===== //
exports.createTask = async (req, res) => {
  try {
    const { task } = req.body;
    const { u_id } = req.params;

    let userId = req.user;

    if (userId != u_id) {
      return res
        .status(403)
        .send({ code: 403, message: "You are not authorized" });
    }

    const taskCreated = await tasks.create({ task, u_id });
    res.status(201).send({
      status: 201,
      data: { message: "Task created successfully.", taskCreated },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: error.message },
    });
  }
};

// ===== Edit Task ===== //
exports.editTask = async (req, res) => {
  try {
    const { t_id, u_id } = req.params;
    let userId = req.user;

    console.log(req.params);
    console.log(t_id, u_id);
    if (userId != u_id) {
      return res
        .status(403)
        .send({ code: 403, message: "You are not authorized" });
    }

    const taskExist = await tasks.findByPk(t_id);
    if (!taskExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Task not exist." },
      });
    }

    const { task, startDate, endDate, isCompleted, status } = req.body;
    await tasks.update(
      { task, startDate, endDate, isCompleted, status },
      { where: { t_id } }
    );
    res.status(201).send({
      status: 201,
      data: { message: "Task updated successfully." },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: error.message },
    });
  }
};

// ===== Delete Task ===== //
exports.deleteTask = async (req, res) => {
  try {
    const { t_id, u_id } = req.params;
    let userId = req.user;

    if (userId != u_id) {
      return res
        .status(403)
        .send({ code: 403, message: "You are not authorized" });
    }

    const taskExist = await tasks.findByPk(t_id);
    if (!taskExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Task not exist." },
      });
    }

    await tasks.update(
      { isDeleted: true, status: "InActive" },
      { where: { t_id } }
    );
    res.status(200).send({
      status: 200,
      data: { message: "Task deleted successfully." },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: { message: error.message },
    });
  }
};
