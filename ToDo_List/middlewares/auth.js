const jwt = require("jsonwebtoken");
require("dotenv").config();
const db = require("../indexFiles/modelsIndex");
const users = db.tbl_users;

const authenticator = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (!token) {
    return res.status(401).send({
      status: 401,
      error: { message: "User Unauthorized." },
    });
  }

  jwt.verify(token, process.env.SECRETE, async (err, user) => {
    if (err) {
      return res.status(403).send({
        status: 403,
        error: { message: "Invalid Token" },
      });
    }

    let userId = user.userId;
    let userExist = await users.findByPk(userId);
    if (!userExist) {
      return res.status(400).send({
        status: 400,
        data: { message: "User not exist." },
      });
    } else {
      req.user = userId;
      next();
    }
  });
};

module.exports = authenticator;
