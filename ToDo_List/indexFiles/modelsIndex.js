// ========== IMPORTS and CONFIGS ========== //
const config = require("../configFiles/db.config");
const Sequelize = require("sequelize");

// ========== DB CONFIG ========== //
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// ========== MODEL ========== //

db.tbl_users = require("../APIs/users/models/users.model")(
  sequelize,
  Sequelize
);
db.tbl_tasks = require("../APIs/tasks/models/task.model")(sequelize, Sequelize);

db.tbl_tasks.belongsTo(db.tbl_users, { as: "tasks", foreignKey: "u_id" });
db.tbl_users.hasMany(db.tbl_tasks, { as: "tasks", foreignKey: "u_id" });

module.exports = db;
