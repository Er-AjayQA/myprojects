// ========== IMPORTS and CONFIGS ========== //

const express = require("express");
const db = require("./indexFiles/modelsIndex");
const userRoutes = require("./APIs/users/routers/users.routers");
const taskRoutes = require("./APIs/tasks/routers/task.router");

// ========== COMMON CONFIG ========== //

const app = express();
const port = 8000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ========== HOME ROUTES ========== //

app.get("/api/v1/todo", (req, res) => {
  try {
    res
      .status(200)
      .send({ data: { message: "Welcome to ToDo List API Project." } });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "Something went wrong.",
    });
  }
});

// ========== DB SYNC ========== //

// db.sequelize
//   .sync({ alter: true })
//   .then(() => {
//     console.log("DB Sync Successfully........");
//   })
//   .catch((err) => {
//     console.log(err, "DB Sync Failed........");
//   });

// ========== ROUTES ========== //

app.use("/api/v1/todo", userRoutes);
app.use("/api/v1/todo", taskRoutes);

// ========== LISTENING SERVER ========== //

app.listen(port, (error) => {
  if (!error) {
    console.log(`Listening to server at port: ${port}`);
  } else {
    console.log("Something Went Wrong from server side.");
  }
});
