// IMPORTING MODULES
const db = require("../util/database");

// PATHS

// EXPORTING MODEL
module.exports = class Product {
  constructor(title, imageUrl, description, price) {
    this.title = title;
    this.price = price;
    this.imageUrl = imageUrl;
    this.description = description;
  }

  save() {
    return db.execute(
      "INSERT INTO products (title, price, imageUrl, description) VALUES (?,?,?,?)",
      [this.title, this.price, this.imageUrl, this.description]
    );
  }

  static deleteById(id) {}

  static fetchAll() {
    return db.execute("SELECT * FROM products");
  }

  static fetchById(id) {
    return db.execute("SELECT * FROM products WHERE products.id=?", [id]);
  }
};
