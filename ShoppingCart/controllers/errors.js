// PAGE NOT FOUND ERROR
exports.pageNotFound = (req, res) => {
  res.status(404).render("404Error", { pageTitle: "Page Not Found" });
};
