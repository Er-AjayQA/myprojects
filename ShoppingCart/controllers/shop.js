// IMPORTING MODULES
const Product = require("../models/product");

// EXPORTING ROUTES
exports.getAddProduct = (req, res, next) => {
  res.render("admin/add-product", {
    path: "/admin/add-product",
    pageTitle: "Add Product",
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const price = req.body.price;
  const imageUrl = req.body.imageUrl;
  const description = req.body.desc;
  const product = new Product(null, title, price, imageUrl, description);
  product
    .save()
    .then(() => {
      res.redirect("/");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getProducts = (req, res, next) => {
  Product.fetchAll()
    .then(([rows, fieldData]) => {
      console.log(rows);
      res.render("shop/product-list", {
        prods: rows,
        path: "/products",
        pageTitle: "Shop",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getCartDetails = (req, res, next) => {
  res
    .status(200)
    .render("shop/cart", { path: "/cart", pageTitle: "Your Cart" });
};

exports.getCheckout = (req, res, next) => {
  res
    .status(200)
    .render("shop/checkout", { path: "checkout", pageTitle: "Checkout" });
};

exports.getIndex = (req, res, next) => {
  res.status(200).render("shop/index", { path: "/" });
};
