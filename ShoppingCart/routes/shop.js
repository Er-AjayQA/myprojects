// IMPORTING MODULES
const express = require("express");
const shopController = require("../controllers/shop");
const router = express.Router();

// ROUTES
router.get("/", shopController.getProducts);
router.get("/products", shopController.getProducts);
router.get("/cart", shopController.getCartDetails);
router.get("/checkout", shopController.getCheckout);

// EXPORTS
module.exports = router;
