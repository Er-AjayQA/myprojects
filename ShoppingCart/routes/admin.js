// IMPORTING MODULES
const express = require("express");
const router = express.Router();
const adminController = require("../controllers/admin");

// ROUTES
// /admin/add-product => GET
router.get("/add-product", adminController.getAddProduct);

// /admin/add-product => POST
router.post("/add-product", adminController.postAddProduct);

// /admin/products => POST
router.get("/products", adminController.getProducts);

// EXPORTS
module.exports = router;
