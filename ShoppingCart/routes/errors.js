const express = require("express");
const errorController = require("../controllers/errors");

const router = express.Router();

router.use("/*", errorController.pageNotFound);

module.exports = router;
