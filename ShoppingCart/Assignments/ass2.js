const express = require("express");
const app = express();
const port = process.env.PORT || 8000;

app.use("/user", (req, res, next) => {
  console.log("List of users");
  res.send("This is User page!!");
});

app.use("/", (req, res, next) => {
  console.log("Home middleware");
  res.send("This is Home page!!");
});

app.listen(port, (err) => {
  if (!err) {
    console.log(`Server is running at port no : ${port}`);
  } else {
    console.error(err);
  }
});
