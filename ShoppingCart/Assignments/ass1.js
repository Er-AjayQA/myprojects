const http = require("http");

const server = http.createServer((req, res) => {
  const url = req.url;
  const method = req.method;

  // Home Page
  if (url === "/") {
    res.writeHeader(200, { "content-type": "text/html" });
    res.write("<html>");
    res.write("<h3>Welcome to Assignment 1</h3>");
    res.write(
      "<form action='/create-user' method='POST'><input type='text' name='username'><button type='submit'>Send</button></form>"
    );
    res.write("</html>");
    res.end();
  }

  // Get All Users
  if (url === "/users") {
    res.writeHeader(200, { "content-type": "text/html" });
    res.write(
      "<ul><li>Ajay Kumar</li><li>Vijay Kumar</li><li>Aman Kumar</li><li>Komal Singh</li></ul>"
    );
    return res.end();
  }

  // Create User
  if (url === "/create-user" && method === "POST") {
    const body = [];

    req.on("data", (chunk) => {
      body.push(chunk);
    });

    req.on("end", () => {
      const user = Buffer.concat(body).toString().split("=")[1];
      console.log(user);
      res.statusCode = 302;
      res.setHeader("Location", "/");
      return res.end();
    });
  }
});

server.listen(3000);
