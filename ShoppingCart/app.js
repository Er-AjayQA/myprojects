// IMPORTING MODULES
const express = require("express");
const path = require("path");
const adminRoute = require("./routes/admin");
const shopRoute = require("./routes/shop");
const errorRoute = require("./routes/errors");

const app = express();
const port = process.env.PORT || 8000;

//PATHS
const staticPath = path.join(__dirname, "public");

// VIEW ENGINE CONFIG
app.set("view engine", "pug");
app.set("views", "views");

// USE MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(staticPath));
app.use("/admin", adminRoute);
app.use(shopRoute);
app.use(errorRoute);

// LISTENING TO SERVER
app.listen(port, (err) => {
  if (!err) {
    console.log(`Server is running at port no :- ${port}`);
  } else {
    console.error(err);
  }
});
