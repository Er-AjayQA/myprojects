import { useEffect, useState } from "react";
import weatherStyle from "./weather.module.css";
import { WeatherCard } from "./WeatherCard";

export const Weather = () => {
  const [searchInput, setSearchInput] = useState("");
  const [weather, setWeather] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  // Handle Search Input
  const handleSearchChange = (value) => {
    setSearchInput(value);
  };

  // Get API data
  const api = `https://api.openweathermap.org/data/2.5/weather?q=${searchInput}&appid=d719daf58da59b42077256a3646147be&units=metric`;
  const fetchWeather = async () => {
    setIsLoading(true);
    setError(null);
    try {
      const response = await fetch(api);
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const weatherData = await response.json();
      setWeather(weatherData);
    } catch (error) {
      setError("Failed to fetch weather data. Please try again later.");
      setIsLoading(false);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchWeather();
  }, []);

  // Handle Form Submit
  const handleFormSubmit = (e) => {
    e.preventDefault();
    fetchWeather();
    setSearchInput("");
  };

  return (
    <>
      <div className={weatherStyle.container}>
        <header>
          <h1>Weather Report</h1>
        </header>
        <div className={weatherStyle["weather-content"]}>
          <div className={weatherStyle["search-box"]}>
            <form onSubmit={handleFormSubmit}>
              <input
                type="search"
                placeholder="Location"
                value={searchInput}
                onChange={(e) => handleSearchChange(e.target.value)}
              />
              <button
                type="submit"
                className={`${weatherStyle.btn}`}
                disabled={isLoading}
              >
                Search
              </button>
            </form>
          </div>
          {weather && !isLoading && !error && (
            <WeatherCard weatherData={weather} />
          )}
        </div>
      </div>
    </>
  );
};
