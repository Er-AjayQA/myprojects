import weatherStyle from "./weather.module.css";
import { IoCloudyOutline } from "react-icons/io5";

export const WeatherCard = ({ weatherData }) => {
  const date = new Date();

  const setMonth = () => {
    const curMonth = date.getMonth();
    const allMonths = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    return allMonths[curMonth];
  };

  const setDay = () => {
    const curDay = date.getDay();
    const allDays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    return allDays[curDay];
  };

  const formattedDate = `${setDay()} - ${date
    .getDate()
    .toString()
    .padStart(2, "0")}-${setMonth()}-${date.getFullYear()}`;

  return (
    <>
      <div className={weatherStyle["weather-body"]}>
        <div className={weatherStyle["weather-card"]}>
          <div className={weatherStyle["weather-info"]}>
            <span className={weatherStyle.date}>{formattedDate}</span>
            <h2 className={weatherStyle.title}>
              {weatherData.name}
              {weatherData.name ? "," : ""}
              {weatherData.sys?.country}
            </h2>
            <p>
              <span>Weather: </span>
              {weatherData.weather?.[0]?.main}
            </p>
            <p>
              <span>Min Temp: </span>
              {weatherData.main?.temp_min}
              {weatherData.main?.temp_min ? "°C" : ""}
            </p>
            <p>
              <span>Max Temp: </span>
              {weatherData.main?.temp_max}
              {weatherData.main?.temp_max ? "°C" : ""}
            </p>
            <p>
              <span>Humidity: </span>
              {weatherData.main?.humidity}
            </p>
          </div>
        </div>
      </div>
      <div className={weatherStyle["right-container"]}></div>
    </>
  );
};
