// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const db = require("./indexFiles/modelsIndex");
require("dotenv").config();
const port = process.env.PORT || 3000;
const assetRouter = require("./APIs/AssetCategory/routers/asset.router");
const uomRouter = require("./APIs/UOM/routers/uom.router");
const deptRouter = require("./APIs/Department/routers/department.router");
const desigRouter = require("./APIs/Designation/routers/designation.router");
const serviceCatRouter = require("./APIs/ServiceCategory/routers/serviceCat.router");
const serviceRouter = require("./APIs/ServiceMaster/routers/service.router");
const countryRouter = require("./APIs/countryStateMaster/routers/countryState.router");
const branchRouter = require("./APIs/BranchSetupMaster/routers/branch.router");
const itemRouter = require("./APIs/ItemMaster/routers/item.router");
const itemSpecRouter = require("./APIs/ItemMaster/routers/itemSpec.router");

// ========== COMMON CONFIG ========== //
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ========== HOME ROUTES ========== //
app.get("/api/v1/masters/home", (req, res) => {
  try {
    res.status(200).send({
      statusCode: 200,
      data: { message: "Welcome to Configurational Masters Api." },
    });
  } catch (error) {
    res.status(500).send({
      statusCode: 500,
      data: { message: "Something went wrong." },
    });
  }
});

// ========== DB SYNC ========== //
// db.sequelize
//   .sync({ alter: true })
//   .then(() => {
//     console.log("DB Sync Successfully............");
//   })
//   .catch((err) => {
//     console.log("DB Sync Failed.............");
//   });

// ========== ROUTES ========== //
app.use("/api/v1/masters/", assetRouter);
app.use("/api/v1/masters/", uomRouter);
app.use("/api/v1/masters/", deptRouter);
app.use("/api/v1/masters/", desigRouter);
app.use("/api/v1/masters/", serviceCatRouter);
app.use("/api/v1/masters/", serviceRouter);
app.use("/api/v1/masters/", countryRouter);
app.use("/api/v1/masters/", branchRouter);
app.use("/api/v1/masters/", itemRouter);
app.use("/api/v1/masters/", itemSpecRouter);

// ========== LISTENING SERVER ========== //
app.listen(port, (err) => {
  if (!err) {
    console.log(`Listening to the server at port : ${port}`);
  } else {
    console.log(err);
  }
});
