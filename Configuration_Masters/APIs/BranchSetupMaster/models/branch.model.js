module.exports = (sequelize, Sequelize) => {
  const tbl_branches = sequelize.define("tbl_branches", {
    branch_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    branchCode: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    branchName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    gstNo: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    contactPerson: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    contactNumber: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    altContactNumber: {
      type: Sequelize.STRING,
    },
    emailId: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    altEmailId: {
      type: Sequelize.STRING,
    },
    country: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    state: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    city: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    pinCode: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    address: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    billingAddress: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_branches;
};
