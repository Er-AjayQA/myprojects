const db = require("../../../indexFiles/modelsIndex");
const services = db.tbl_services;
const serviceCategories = db.tbl_serviceCategories;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Find Service Category By Name ================== //
const serviceCatId = async (srvCatName) => {
  try {
    const getCatId = await serviceCategories.findOne({
      where: { srvCatName: srvCatName },
    });

    if (!getCatId) {
      return null;
    }

    return getCatId.srvCat_id;
  } catch (error) {
    console.error("Error fetching service category ID:", error);
    return null;
  }
};

// ================== Create Service ================== //
module.exports.createService = async (req, res) => {
  try {
    const { serviceCatName, serviceName, serviceDescription, mvp } = req.body;
    const getCategoryId = await serviceCatId(serviceCatName);

    if (!getCategoryId || getCategoryId === null) {
      return res.status(500).send({
        status: 500,
        data: { message: "No Service Category Exist." },
      });
    }

    const serviceExist = await services.findOne({
      where: {
        [db.Sequelize.Op.and]: [{ serviceName }, { srvCat_id: getCategoryId }],
      },
    });

    if (serviceExist) {
      if (!serviceExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "Service Already Exist." },
        });
      } else {
        await services.update(
          {
            serviceName,
            serviceDescription,
            mvp,
            srvCat_id: getCategoryId,
            isDeleted: false,
            status: "Active",
          },
          { where: { service_id: serviceExist.service_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Service Created Successfully." },
        });
      }
    }
    if (!serviceExist) {
      await services.create({
        serviceName,
        serviceDescription,
        mvp,
        srvCat_id: getCategoryId,
      });
      return res.status(201).send({
        status: 201,
        data: { message: "Service Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All Service Category ================== //
module.exports.getListOfServices = async (req, res) => {
  try {
    const allServices = await services.findAll({
      attributes: [
        "service_id",
        "serviceName",
        "mvp",
        "serviceDescription",
        "isDeleted",
        "status",
      ],
      include: [
        {
          model: serviceCategories,
          attributes: [
            "srvCat_id",
            "srvCatName",
            "srvDescription",
            "isDeleted",
            "status",
          ],
        },
      ],
    });

    return res.status(`${allServices.length > 0 ? 200 : 500}`).send({
      status: allServices.length > 0 ? 200 : 500,
      records: allServices.length > 0 ? allServices.length : "None",
      data: {
        message:
          allServices.length > 0 ? "Get All Services List" : "No Data Found.",
        allServices: allServices.length > 0 ? allServices : "No Data.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single Service Details ================== //
module.exports.getServiceDetails = async (req, res) => {
  try {
    const { service_id } = req.params;
    const serviceExist = await services.findByPk(service_id, {
      where: { isDeleted: false },
    });
    const getServiceAllDetail = await services.findByPk(service_id, {
      include: [
        {
          model: serviceCategories,
          attributes: ["srvCat_id", "srvCatName", "srvDescription"],
        },
      ],
    });

    if (!serviceExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Service Not Exist." },
      });
    }

    if (serviceExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "Service Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: {
          message: "Service Details.",
          details: getServiceAllDetail,
        },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Service Category ================== //
module.exports.updateService = async (req, res) => {
  try {
    const { service_id } = req.params;
    const { serviceCatName, serviceName, serviceDescription, mvp } = req.body;
    const serviceExist = await services.findByPk(service_id);
    const getCategoryId = await serviceCatId(serviceCatName);

    // If Service not Exist
    if (!serviceExist || serviceExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Service Not Exist." },
      });
    }

    // Check if Already exist
    const getService = await services.findOne({
      where: {
        [db.Sequelize.Op.and]: [{ serviceName }, { srvCat_id: getCategoryId }],
        service_id: { [db.Sequelize.Op.ne]: service_id }, // Ensure the found Service is not the one being updated
      },
    });

    if (getService) {
      if (getService.isDeleted === false) {
        return res.status(500).send({
          status: 500,
          data: { message: "Service Already Exist." },
        });
      } else {
        await services.update(
          { srvCat_id: getCategoryId, serviceName, serviceDescription, mvp },
          { where: { service_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Service Updated Successfully." },
        });
      }
    } else {
      await services.update(
        { srvCat_id: getCategoryId, serviceName, serviceDescription, mvp },
        { where: { service_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Service Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Service Category Status ================== //
module.exports.updateServiceStatus = async (req, res) => {
  try {
    const { service_id } = req.params;
    const serviceExist = await services.findByPk(service_id, {
      where: { isDeleted: false },
    });

    if (!serviceExist || serviceExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Service Not Exist." },
      });
    }

    if (serviceExist.status === "Active") {
      await services.update({ status: "InActive" }, { where: { service_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Service Status Updated Successfully." },
      });
    } else {
      await services.update({ status: "Active" }, { where: { service_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Service Status Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete Service Category ================== //
module.exports.deleteService = async (req, res) => {
  try {
    const { service_id } = req.params;
    const serviceExist = await services.findByPk(service_id, {
      where: { isDeleted: false },
    });

    if (!serviceExist) {
      return res.status(201).send({
        status: 201,
        data: { message: "Service Not Exist." },
      });
    }

    if (serviceExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "Service Already Deleted." },
      });
    } else {
      await services.update(
        { isDeleted: true, status: "InActive" },
        { where: { service_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Service Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
