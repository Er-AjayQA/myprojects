module.exports = (sequelize, Sequelize) => {
  const tbl_services = sequelize.define("tbl_services", {
    service_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    serviceName: {
      type: Sequelize.STRING,
      allowNull: false,
      // unique: true,
    },
    mvp: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    serviceDescription: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_services;
};
