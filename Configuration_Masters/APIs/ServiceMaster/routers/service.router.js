const express = require("express");
const router = express.Router();
const serviceController = require("../controller/service.controller");

router.post("/createService", serviceController.createService);
router.get("/getListOfServices", serviceController.getListOfServices);
router.get(
  "/getServiceDetails/:service_id",
  serviceController.getServiceDetails
);
router.put("/updateService/:service_id", serviceController.updateService);
router.put(
  "/updateServiceStatus/:service_id",
  serviceController.updateServiceStatus
);
router.delete("/deleteService/:service_id", serviceController.deleteService);

module.exports = router;
