const db = require("../../../indexFiles/modelsIndex");
const designations = db.tbl_designations;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Create Department ================== //
module.exports.createDesignation = async (req, res) => {
  try {
    const { desigName, desigDescription } = req.body;

    const desigExist = await designations.findOne({
      where: { desigName },
    });

    if (desigExist) {
      if (!desigExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "Designation Already Exist." },
        });
      } else {
        await designations.update(
          { desigName, desigDescription, isDeleted: false, status: "Active" },
          { where: { desig_id: desigExist.desig_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Designation Created Successfully." },
        });
      }
    }
    if (!desigExist) {
      await designations.create({ desigName, desigDescription });
      return res.status(201).send({
        status: 201,
        data: { message: "Designation Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All Department ================== //
module.exports.getListOfAllDesignation = async (req, res) => {
  try {
    const allDesignationList = await designations.findAll();

    return res.status(`${allDesignationList.length > 0 ? 200 : 500}`).send({
      status: allDesignationList.length > 0 ? 200 : 500,
      records:
        allDesignationList.length > 0 ? allDesignationList.length : "None",
      data: {
        message:
          allDesignationList.length > 0
            ? "Get All Designation List"
            : "No Data Found.",
        assets: allDesignationList.length > 0 ? allDesignationList : "No Data.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single Designation Details ================== //
module.exports.getDesignationDetails = async (req, res) => {
  try {
    const { desig_id } = req.params;
    const designationExist = await designations.findByPk(desig_id, {
      where: { isDeleted: false },
    });

    if (!designationExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Designation Not Exist." },
      });
    }

    if (designationExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "Designation Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: {
          message: "Designation Details.",
          details: designationExist,
        },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Department ================== //
module.exports.updateDesignation = async (req, res) => {
  try {
    const { desig_id } = req.params;
    const { desigName, desigDescription } = req.body;
    const desigExist = await designations.findByPk(desig_id);

    // If Department not Exist
    if (!desigExist || desigExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Designation Not Exist." },
      });
    }

    // Check if Department Already Exist
    const getAllDesig = await designations.findOne({
      where: {
        desigName,
        desig_id: { [db.Sequelize.Op.ne]: desig_id }, // Ensure the found Designation is not the one being updated
      },
    });

    if (getAllDesig) {
      if (getAllDesig.isDeleted === false) {
        return res.status(409).send({
          status: 409,
          data: { message: "Designation Already Exist." },
        });
      } else {
        await designations.update(
          { desigName, desigDescription },
          { where: { desig_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Designation Updated Successfully." },
        });
      }
    } else {
      await designations.update(
        { desigName, desigDescription },
        { where: { desig_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Designation Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Department Status ================== //
module.exports.updateDesignationStatus = async (req, res) => {
  try {
    const { desig_id } = req.params;
    const desigExist = await designations.findByPk(desig_id, {
      where: { isDeleted: false },
    });

    if (!desigExist || desigExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Designation Not Exist." },
      });
    }

    if (desigExist.status === "Active") {
      await designations.update(
        { status: "InActive" },
        { where: { desig_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Designation Updated Successfully." },
      });
    } else {
      await designations.update({ status: "Active" }, { where: { desig_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Designation Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete Department ================== //
module.exports.deleteDesignation = async (req, res) => {
  try {
    const { desig_id } = req.params;
    const desigExist = await designations.findByPk(desig_id, {
      where: { isDeleted: false },
    });

    if (!desigExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Designation Not Exist." },
      });
    }

    if (desigExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "Designation Already Deleted." },
      });
    } else {
      await designations.update(
        { isDeleted: true, status: "InActive" },
        { where: { desig_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Designation Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
