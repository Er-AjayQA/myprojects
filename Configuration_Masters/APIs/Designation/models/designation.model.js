module.exports = (sequelize, Sequelize) => {
  const tbl_designations = sequelize.define("tbl_designations", {
    desig_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    desigName: {
      type: Sequelize.STRING,
      allowNull: false,
      // unique: true,
    },
    desigDescription: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_designations;
};
