const express = require("express");
const router = express.Router();
const designationController = require("../controller/designation.controller");

router.post("/createDesignation", designationController.createDesignation);
router.get(
  "/getListOfAllDesignation",
  designationController.getListOfAllDesignation
);
router.get(
  "/getDesignationDetails/:desig_id",
  designationController.getDesignationDetails
);
router.put(
  "/updateDesignation/:desig_id",
  designationController.updateDesignation
);
router.put(
  "/updateDesignationStatus/:desig_id",
  designationController.updateDesignationStatus
);
router.delete(
  "/deleteDesignation/:desig_id",
  designationController.deleteDesignation
);

module.exports = router;
