const db = require("../../../indexFiles/modelsIndex");
const assetCategories = db.tbl_assetCategories;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Create Asset Category ================== //
module.exports.createAssetCategory = async (req, res) => {
  try {
    const { assetName, asstDescription } = req.body;

    const assetExist = await assetCategories.findOne({
      where: { assetName },
    });

    if (assetExist) {
      if (!assetExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "Asset Category Already Exist." },
        });
      } else {
        await assetCategories.update(
          { assetName, asstDescription, isDeleted: false, status: "Active" },
          { where: { ast_id: assetExist.ast_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Asset Category Created Successfully." },
        });
      }
    }
    if (!assetExist) {
      await assetCategories.create({ assetName, asstDescription });
      return res.status(201).send({
        status: 201,
        data: { message: "Asset Category Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All Asset Category ================== //
module.exports.getListOfAssetCategories = async (req, res) => {
  try {
    const assets = await assetCategories.findAll();

    return res.status(`${assets.length > 0 ? 200 : 500}`).send({
      status: assets.length > 0 ? 200 : 500,
      records: assets.length > 0 ? assets.length : "None",
      data: {
        message: assets.length > 0 ? "Get Assets List" : "No Data Found.",
        assets: assets.length > 0 ? assets : "No Data.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single Asset Category Details ================== //
module.exports.getAssetCategoryDetails = async (req, res) => {
  try {
    const { ast_id } = req.params;
    const assetCategoryExist = await assetCategories.findByPk(ast_id, {
      where: { isDeleted: false },
    });

    if (!assetCategoryExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Asset Category Not Exist." },
      });
    }

    if (assetCategoryExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "Asset Category Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: {
          message: "Asset Category Details.",
          details: assetCategoryExist,
        },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Asset Category ================== //
module.exports.updateAssetCategories = async (req, res) => {
  try {
    const { ast_id } = req.params;
    const { assetName, asstDescription } = req.body;
    const assetExist = await assetCategories.findByPk(ast_id);

    // If Asset not Exist
    if (!assetExist || assetExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Asset Category Not Exist." },
      });
    }

    // Check if Already exist
    const getAllAssets = await assetCategories.findOne({
      where: {
        assetName,
        ast_id: { [db.Sequelize.Op.ne]: ast_id }, // Ensure the found dept is not the one being updated
      },
    });

    if (getAllAssets) {
      if (getAllAssets.isDeleted === false) {
        return res.status(500).send({
          status: 500,
          data: { message: "Asset Category Already Exist." },
        });
      } else {
        await assetCategories.update(
          { assetName, asstDescription },
          { where: { ast_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Asset Category Updated Successfully." },
        });
      }
    } else {
      await assetCategories.update(
        { assetName, asstDescription },
        { where: { ast_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Asset Category Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Asset Category Status ================== //
module.exports.updateAssetCategoriesStatus = async (req, res) => {
  try {
    const { ast_id } = req.params;
    const assetExist = await assetCategories.findByPk(ast_id, {
      where: { isDeleted: false },
    });

    if (!assetExist || assetExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Asset Category Not Exist." },
      });
    }

    if (assetExist.status === "Active") {
      await assetCategories.update(
        { status: "InActive" },
        { where: { ast_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Asset Category Updated Successfully." },
      });
    } else {
      await assetCategories.update({ status: "Active" }, { where: { ast_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Asset Category Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete Asset Category ================== //
module.exports.deleteAssetCategories = async (req, res) => {
  try {
    const { ast_id } = req.params;
    const assetExist = await assetCategories.findByPk(ast_id, {
      where: { isDeleted: false },
    });

    if (!assetExist) {
      return res.status(201).send({
        status: 201,
        data: { message: "Asset Category Not Exist." },
      });
    }

    if (assetExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "Asset Category Already Deleted." },
      });
    } else {
      await assetCategories.update(
        { isDeleted: true, status: "InActive" },
        { where: { ast_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Asset Category Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
