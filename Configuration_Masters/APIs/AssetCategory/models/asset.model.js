module.exports = (sequelize, Sequelize) => {
  const tbl_assetCategories = sequelize.define("tbl_assetCategories", {
    ast_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    assetName: {
      type: Sequelize.STRING,
      allowNull: false,
      // unique: true,
    },
    asstDescription: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_assetCategories;
};
