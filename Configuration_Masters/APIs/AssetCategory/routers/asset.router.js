const express = require("express");
const router = express.Router();
const assetController = require("../controller/asset.controller");

router.post("/createAssetCategory", assetController.createAssetCategory);
router.get(
  "/getListOfAssetCategories",
  assetController.getListOfAssetCategories
);
router.get(
  "/getAssetCategoryDetails/:ast_id",
  assetController.getAssetCategoryDetails
);
router.put(
  "/updateAssetCategories/:ast_id",
  assetController.updateAssetCategories
);
router.put(
  "/updateAssetCategoriesStatus/:ast_id",
  assetController.updateAssetCategoriesStatus
);
router.delete(
  "/deleteAssetCategories/:ast_id",
  assetController.deleteAssetCategories
);

module.exports = router;
