const db = require("../../../indexFiles/modelsIndex");
const departments = db.tbl_departments;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Create Department ================== //
module.exports.createDepartment = async (req, res) => {
  try {
    const { deptName, deptDescription } = req.body;

    const deptExist = await departments.findOne({
      where: { deptName },
    });

    if (deptExist) {
      if (!deptExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "Department Already Exist." },
        });
      } else {
        await departments.update(
          { deptName, deptDescription, isDeleted: false, status: "Active" },
          { where: { dept_id: deptExist.dept_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Department Created Successfully." },
        });
      }
    }
    if (!deptExist) {
      await departments.create({ deptName, deptDescription });
      return res.status(201).send({
        status: 201,
        data: { message: "Department Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All Department ================== //
module.exports.getListOfAllDepartment = async (req, res) => {
  try {
    const allDepartment = await departments.findAll();

    return res.status(`${allDepartment.length > 0 ? 200 : 500}`).send({
      status: allDepartment.length > 0 ? 200 : 500,
      records: allDepartment.length > 0 ? allDepartment.length : "None",
      data: {
        message:
          allDepartment.length > 0
            ? "Get All Department List"
            : "No Data Found.",
        assets: allDepartment.length > 0 ? allDepartment : "No Data.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single Department Details ================== //
module.exports.getDepartmentDetails = async (req, res) => {
  try {
    const { dept_id } = req.params;
    const departmentExist = await departments.findByPk(dept_id, {
      where: { isDeleted: false },
    });

    if (!departmentExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Department Not Exist." },
      });
    }

    if (departmentExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "Department Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: {
          message: "Department Details.",
          details: departmentExist,
        },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Department ================== //
module.exports.updateDepartment = async (req, res) => {
  try {
    const { dept_id } = req.params;
    const { deptName, deptDescription } = req.body;
    const deptExist = await departments.findByPk(dept_id);

    // If Department not Exist
    if (!deptExist || deptExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Department Not Exist." },
      });
    }

    // Check if Department Already Exist
    const getAllDept = await departments.findOne({
      where: {
        deptName,
        dept_id: { [db.Sequelize.Op.ne]: dept_id }, // Ensure the found dept is not the one being updated
      },
    });

    if (getAllDept) {
      if (getAllDept.isDeleted === false) {
        return res.status(409).send({
          status: 409,
          data: { message: "Department Already Exist." },
        });
      } else {
        await departments.update(
          { deptName, deptDescription },
          { where: { dept_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Department Updated Successfully." },
        });
      }
    } else {
      await departments.update(
        { deptName, deptDescription },
        { where: { dept_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Department Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Department Status ================== //
module.exports.updateDepartmentStatus = async (req, res) => {
  try {
    const { dept_id } = req.params;
    const deptExist = await departments.findByPk(dept_id, {
      where: { isDeleted: false },
    });

    if (!deptExist || deptExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Department Not Exist." },
      });
    }

    if (deptExist.status === "Active") {
      await departments.update({ status: "InActive" }, { where: { dept_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Department Updated Successfully." },
      });
    } else {
      await departments.update({ status: "Active" }, { where: { dept_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Department Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete Department ================== //
module.exports.deleteDepartment = async (req, res) => {
  try {
    const { dept_id } = req.params;
    const deptExist = await departments.findByPk(dept_id, {
      where: { isDeleted: false },
    });

    if (!deptExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Department Not Exist." },
      });
    }

    if (deptExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "Department Already Deleted." },
      });
    } else {
      await departments.update(
        { isDeleted: true, status: "InActive" },
        { where: { dept_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Department Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
