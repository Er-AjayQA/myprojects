const express = require("express");
const router = express.Router();
const deptController = require("../controller/department.controller");

router.post("/createDepartment", deptController.createDepartment);
router.get("/getListOfAllDepartment", deptController.getListOfAllDepartment);
router.get(
  "/getDepartmentDetails/:dept_id",
  deptController.getDepartmentDetails
);
router.put("/updateDepartment/:dept_id", deptController.updateDepartment);
router.put(
  "/updateDepartmentStatus/:dept_id",
  deptController.updateDepartmentStatus
);
router.delete("/deleteDepartment/:dept_id", deptController.deleteDepartment);

module.exports = router;
