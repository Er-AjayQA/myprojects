module.exports = (sequelize, Sequelize) => {
  const tbl_departments = sequelize.define("tbl_departments", {
    dept_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    deptName: {
      type: Sequelize.STRING,
      allowNull: false,
      // unique: true,
    },
    deptDescription: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_departments;
};
