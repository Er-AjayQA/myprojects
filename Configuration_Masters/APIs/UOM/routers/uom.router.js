const express = require("express");
const router = express.Router();
const uomController = require("../controller/uom.controller");

router.post("/createUom", uomController.createUom);
router.get("/getListOfAllUom", uomController.getListOfAllUom);
router.get("/getUomDetails/:uom_id", uomController.getUomDetails);
router.put("/updateUom/:uom_id", uomController.updateUom);
router.put("/updateUomStatus/:uom_id", uomController.updateUomStatus);
router.delete("/deleteUom/:uom_id", uomController.deleteUom);

module.exports = router;
