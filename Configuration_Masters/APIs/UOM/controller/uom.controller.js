const db = require("../../../indexFiles/modelsIndex");
const measurements = db.tbl_uoms;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Create UOM ================== //
module.exports.createUom = async (req, res) => {
  try {
    const { uomName, uomDescription } = req.body;

    const uomExist = await measurements.findOne({
      where: { uomName },
    });

    if (uomExist) {
      if (!uomExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "UOM Already Exist." },
        });
      } else {
        await measurements.update(
          { uomName, uomDescription, isDeleted: false, status: "Active" },
          { where: { uom_id: uomExist.uom_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "UOM Created Successfully." },
        });
      }
    }
    if (!uomExist) {
      await measurements.create({ uomName, uomDescription });
      return res.status(201).send({
        status: 201,
        data: { message: "UOM Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All UOM ================== //
module.exports.getListOfAllUom = async (req, res) => {
  try {
    const allUom = await measurements.findAll();

    return res.status(`${allUom.length > 0 ? 200 : 500}`).send({
      status: allUom.length > 0 ? 200 : 500,
      records: allUom.length > 0 ? allUom.length : "None",
      data: {
        message: allUom.length > 0 ? "Get UOM List" : "No Data Found.",
        assets: allUom.length > 0 ? allUom : "No Data.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single UOM Details ================== //
module.exports.getUomDetails = async (req, res) => {
  try {
    const { uom_id } = req.params;
    const uomExist = await measurements.findByPk(uom_id, {
      where: { isDeleted: false },
    });

    if (!uomExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "UOM Not Exist." },
      });
    }

    if (uomExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "UOM Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: {
          message: "UOM Details.",
          details: uomExist,
        },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Asset Category ================== //
module.exports.updateUom = async (req, res) => {
  try {
    const { uom_id } = req.params;
    const { uomName, uomDescription } = req.body;
    const uomExist = await measurements.findByPk(uom_id);

    // If UOM not Exist
    if (!uomExist || uomExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "UOM Not Exist." },
      });
    }

    // Check if UOM Already Exist
    const getAllUOM = await measurements.findOne({
      where: {
        uomName,
        uom_id: { [db.Sequelize.Op.ne]: uom_id }, // Ensure the found UOM is not the one being updated
      },
    });

    if (getAllUOM) {
      if (getAllUOM.isDeleted === false) {
        return res.status(409).send({
          status: 409,
          data: { message: "UOM Already Exist." },
        });
      } else {
        await measurements.update(
          { uomName, uomDescription },
          { where: { uom_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "UOM Updated Successfully." },
        });
      }
    } else {
      await measurements.update(
        { uomName, uomDescription },
        { where: { uom_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "UOM Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update UOM Status ================== //
module.exports.updateUomStatus = async (req, res) => {
  try {
    const { uom_id } = req.params;
    const uomExist = await measurements.findByPk(uom_id, {
      where: { isDeleted: false },
    });

    if (!uomExist || uomExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "UOM Not Exist." },
      });
    }

    if (uomExist.status === "Active") {
      await measurements.update({ status: "InActive" }, { where: { uom_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "UOM Updated Successfully." },
      });
    } else {
      await measurements.update({ status: "Active" }, { where: { uom_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "UOM Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete UOM ================== //
module.exports.deleteUom = async (req, res) => {
  try {
    const { uom_id } = req.params;
    const uomExist = await measurements.findByPk(uom_id, {
      where: { isDeleted: false },
    });

    if (!uomExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "UOM Not Exist." },
      });
    }

    if (uomExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "UOM Already Deleted." },
      });
    } else {
      await measurements.update(
        { isDeleted: true, status: "InActive" },
        { where: { uom_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "UOM Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
