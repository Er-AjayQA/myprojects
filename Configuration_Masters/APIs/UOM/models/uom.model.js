module.exports = (sequelize, Sequelize) => {
  const tbl_uoms = sequelize.define("tbl_uoms", {
    uom_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    uomName: {
      type: Sequelize.STRING,
      allowNull: false,
      // unique: true,
    },
    uomDescription: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_uoms;
};
