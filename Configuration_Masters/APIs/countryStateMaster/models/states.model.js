const { toDefaultValue } = require("sequelize/lib/utils");

module.exports = (sequelize, Sequelize) => {
  const tbl_states = sequelize.define("tbl_states", {
    state_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    stateCode: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    stateName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    countryCode: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_states;
};
