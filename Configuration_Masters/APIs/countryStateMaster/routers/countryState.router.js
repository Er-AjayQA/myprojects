const express = require("express");
const router = express.Router();
const countryController = require("../controller/countryState.controller");

router.get("/storeCountryData", countryController.storeCountryData);
router.get("/storeStatesData", countryController.storeStatesData);
router.get("/getAllCountries", countryController.getAllCountries);
router.get("/getCountryAllStates", countryController.getCountryAllStates);
router.get("/getStateAllCities", countryController.getStateAllCities);

module.exports = router;
