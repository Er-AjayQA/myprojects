const db = require("../../../indexFiles/modelsIndex");
let Country = require("country-state-city").Country;
let State = require("country-state-city").State;
let City = require("country-state-city").City;
const countries = db.tbl_countries;
const states = db.tbl_states;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Store ALl Country In DB ================== //
module.exports.storeCountryData = async (req, res) => {
  try {
    const allCountries = Country.getAllCountries();

    for (const country of allCountries) {
      await countries.create({
        countryCode: country.isoCode,
        countryName: country.name,
      });
    }

    return res.status(200).send({
      status: 200,
      data: {
        message: "All Data Stored In DataBase.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Store ALl States In DB ================== //
module.exports.storeStatesData = async (req, res) => {
  try {
    const allStates = State.getAllStates();

    for (const state of allStates) {
      await states.create({
        stateCode: state.isoCode,
        stateName: state.name,
        countryCode: state.countryCode,
      });
    }

    return res.status(200).send({
      status: 200,
      data: {
        message: "All Data Stored In DataBase.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get All Countries List ================== //
module.exports.getAllCountries = async (req, res) => {
  try {
    const allCountriesList = await countries.findAll();
    const countriesData = allCountriesList.map((country) => {
      return {
        name: country.countryName,
        code: country.countryCode,
      };
    });
    return res.status(200).send({
      status: 200,
      data: {
        message: "All States Stored In DataBase.",
        countriesData,
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get All States List ================== //
module.exports.getCountryAllStates = async (req, res) => {
  try {
    const { countryName } = req.body;
    const countryDetails = await countries.findOne({ where: { countryName } });
    const countryCode = countryDetails.countryCode;
    console.log(countryCode);

    const getStatesByCode = await states.findAll({
      attributes: ["stateName", "stateCode"],
      where: { countryCode },
    });

    return res.status(200).send({
      status: 200,
      data: {
        message: `All States of ${countryName}`,
        statesDetails: getStatesByCode,
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get All Cities List ================== //
module.exports.getStateAllCities = async (req, res) => {
  try {
    const { countryName, stateName } = req.body;
    const countryDetails = await countries.findOne({ where: { countryName } });
    const countryCode = countryDetails.countryCode;
    const stateDetails = await states.findOne({
      where: { stateName },
    });
    const stateCode = stateDetails.stateCode;
    console.log(countryCode, stateCode);

    const cityDetails = City.getCitiesOfState(countryCode, stateCode);
    const allCities = cityDetails.map((city) => {
      return {
        cityName: city.name,
      };
    });

    return res.status(200).send({
      status: 200,
      data: {
        message: `All Cities of ${stateName}`,
        cityDetails: allCities,
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
