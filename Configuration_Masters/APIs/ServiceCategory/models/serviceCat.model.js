module.exports = (sequelize, Sequelize) => {
  const tbl_serviceCategories = sequelize.define("tbl_serviceCategories", {
    srvCat_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    srvCatName: {
      type: Sequelize.STRING,
      allowNull: false,
      // unique: true,
    },
    srvDescription: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_serviceCategories;
};
