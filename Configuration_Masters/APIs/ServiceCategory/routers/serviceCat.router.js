const express = require("express");
const router = express.Router();
const serviceCatController = require("../controller/serviceCat.controller");

router.post(
  "/createServiceCategory",
  serviceCatController.createServiceCategory
);
router.get(
  "/getListOfServiceCategories",
  serviceCatController.getListOfServiceCategories
);
router.get(
  "/getServiceCategoryDetails/:srvCat_id",
  serviceCatController.getServiceCategoryDetails
);
router.put(
  "/updateServiceCategories/:srvCat_id",
  serviceCatController.updateServiceCategories
);
router.put(
  "/updateServiceCategoriesStatus/:srvCat_id",
  serviceCatController.updateServiceCategoriesStatus
);
router.delete(
  "/deleteServiceCategories/:srvCat_id",
  serviceCatController.deleteServiceCategories
);

module.exports = router;
