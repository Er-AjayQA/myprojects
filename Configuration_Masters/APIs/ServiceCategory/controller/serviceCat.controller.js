const db = require("../../../indexFiles/modelsIndex");
const serviceCategories = db.tbl_serviceCategories;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Create Service Category ================== //
module.exports.createServiceCategory = async (req, res) => {
  try {
    const { srvCatName, srvDescription } = req.body;

    const serviceExist = await serviceCategories.findOne({
      where: { srvCatName },
    });

    if (serviceExist) {
      if (!serviceExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "Service Category Already Exist." },
        });
      } else {
        await serviceCategories.update(
          { srvCatName, srvDescription, isDeleted: false, status: "Active" },
          { where: { srvCat_id: serviceExist.srvCat_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Service Category Created Successfully." },
        });
      }
    }
    if (!serviceExist) {
      await serviceCategories.create({ srvCatName, srvDescription });
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All Service Category ================== //
module.exports.getListOfServiceCategories = async (req, res) => {
  try {
    const services = await serviceCategories.findAll();

    return res.status(`${services.length > 0 ? 200 : 500}`).send({
      status: services.length > 0 ? 200 : 500,
      records: services.length > 0 ? services.length : "None",
      data: {
        message: services.length > 0 ? "Get services List" : "No Data Found.",
        services: services.length > 0 ? services : "No Data.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single Service Category Details ================== //
module.exports.getServiceCategoryDetails = async (req, res) => {
  try {
    const { srvCat_id } = req.params;
    const serviceCategoryExist = await serviceCategories.findByPk(srvCat_id, {
      where: { isDeleted: false },
    });

    if (!serviceCategoryExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Service Category Not Exist." },
      });
    }

    if (serviceCategoryExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "Service Category Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: {
          message: "Service Category Details.",
          details: serviceCategoryExist,
        },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Service Category ================== //
module.exports.updateServiceCategories = async (req, res) => {
  try {
    const { srvCat_id } = req.params;
    const { srvCatName, srvDescription } = req.body;
    const serviceExist = await serviceCategories.findByPk(srvCat_id);

    // If Service not Exist
    if (!serviceExist || serviceExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Service Category Not Exist." },
      });
    }

    // Check if Already exist
    const getAllServiceCategories = await serviceCategories.findOne({
      where: {
        srvCatName,
        srvCat_id: { [db.Sequelize.Op.ne]: srvCat_id }, // Ensure the found Service Category is not the one being updated
      },
    });

    if (getAllServiceCategories) {
      if (getAllServiceCategories.isDeleted === false) {
        return res.status(500).send({
          status: 500,
          data: { message: "Service Category Already Exist." },
        });
      } else {
        await serviceCategories.update(
          { srvCatName, srvDescription },
          { where: { srvCat_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Service Category Updated Successfully." },
        });
      }
    } else {
      await serviceCategories.update(
        { srvCatName, srvDescription },
        { where: { srvCat_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Service Category Status ================== //
module.exports.updateServiceCategoriesStatus = async (req, res) => {
  try {
    const { srvCat_id } = req.params;
    const serviceExist = await serviceCategories.findByPk(srvCat_id, {
      where: { isDeleted: false },
    });

    if (!serviceExist || serviceExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Not Exist." },
      });
    }

    if (serviceExist.status === "Active") {
      await serviceCategories.update(
        { status: "InActive" },
        { where: { srvCat_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Updated Successfully." },
      });
    } else {
      await serviceCategories.update(
        { status: "Active" },
        { where: { srvCat_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete Service Category ================== //
module.exports.deleteServiceCategories = async (req, res) => {
  try {
    const { srvCat_id } = req.params;
    const serviceExist = await serviceCategories.findByPk(srvCat_id, {
      where: { isDeleted: false },
    });

    if (!serviceExist) {
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Not Exist." },
      });
    }

    if (serviceExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Already Deleted." },
      });
    } else {
      await serviceCategories.update(
        { isDeleted: true, status: "InActive" },
        { where: { srvCat_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Service Category Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
