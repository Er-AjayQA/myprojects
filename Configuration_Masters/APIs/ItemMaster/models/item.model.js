module.exports = (sequelize, Sequelize) => {
  const tbl_items = sequelize.define("tbl_items", {
    item_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    itemName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    rental: {
      type: Sequelize.INTEGER,
    },
    buyOut: {
      type: Sequelize.INTEGER,
    },
    mvp: {
      type: Sequelize.INTEGER,
    },
    barCode: {
      type: Sequelize.ENUM,
      allowNull: false,
      values: ["Yes", "No"],
    },
    manageBy: {
      type: Sequelize.ENUM,
      values: ["Batch Only", "S.No. Only"],
      defaultValue: "Batch Only",
    },
    itemNumber: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    itemDesc: {
      type: Sequelize.STRING,
    },
    thresholdStock: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isAsset: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_items;
};
