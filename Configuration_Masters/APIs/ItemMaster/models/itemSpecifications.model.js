module.exports = (sequelize, Sequelize) => {
  const tbl_itemSpecifications = sequelize.define("tbl_itemSpecifications", {
    spec_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    specType: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    specDesc: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.ENUM,
      values: ["Active", "InActive"],
      defaultValue: "Active",
    },
  });
  return tbl_itemSpecifications;
};
