const express = require("express");
const router = express.Router();
const itemSpecsController = require("../controller/itemSpec.controller");

// router.post("/createBranch", branchController.createBranch);
// router.get("/getListOfAllBranches", branchController.getListOfAllBranches);
// router.get("/getBranchDetails/:branch_id", branchController.getBranchDetails);
// router.put("/updateBranch/:branch_id", branchController.updateBranch);
// router.put(
//   "/updateBranchStatus/:branch_id",
//   branchController.updateBranchStatus
// );
// router.delete("/deleteBranch/:branch_id", branchController.deleteBranch);

module.exports = router;
