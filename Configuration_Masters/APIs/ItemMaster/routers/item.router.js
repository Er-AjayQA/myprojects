const express = require("express");
const router = express.Router();
const itemController = require("../controller/item.controller");

router.post("/createItem", itemController.createItem);
router.get("/getListOfAllItems", itemController.getListOfAllItems);
router.get("/getItemDetails/:item_id", itemController.getItemDetails);
// router.put("/updateBranch/:branch_id", branchController.updateBranch);
router.put("/updateItemStatus/:item_id", itemController.updateItemStatus);
router.delete("/deleteItem/:item_id", itemController.deleteItem);

module.exports = router;
