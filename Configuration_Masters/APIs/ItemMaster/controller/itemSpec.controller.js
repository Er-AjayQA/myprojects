const db = require("../../../indexFiles/modelsIndex");
const branches = db.tbl_branches;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Create Branches ================== //
module.exports.createBranch = async (req, res) => {
  try {
    const {
      branchCode,
      branchName,
      gstNo,
      contactPerson,
      contactNumber,
      altContactNumber,
      emailId,
      altEmailId,
      country,
      state,
      city,
      pinCode,
      address,
      billingAddress,
    } = req.body;

    const branchExist = await branches.findOne({
      where: { branchCode, branchName },
    });

    if (branchExist) {
      if (!branchExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "Branch Already Exist." },
        });
      } else {
        await branches.update(
          {
            branchCode,
            branchName,
            gstNo,
            contactPerson,
            contactNumber,
            altContactNumber,
            emailId,
            altEmailId,
            country,
            state,
            city,
            pinCode,
            address,
            billingAddress,
            isDeleted: false,
            status: "Active",
          },
          { where: { branch_id: branchExist.branch_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Branch Created Successfully." },
        });
      }
    }
    if (!branchExist) {
      await branches.create({
        branchCode,
        branchName,
        gstNo,
        contactPerson,
        contactNumber,
        altContactNumber,
        emailId,
        altEmailId,
        country,
        state,
        city,
        pinCode,
        address,
        billingAddress,
      });
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All Branches ================== //
module.exports.getListOfAllBranches = async (req, res) => {
  try {
    const allBranches = await branches.findAll();

    return res.status(`${allBranches.length > 0 ? 200 : 500}`).send({
      status: allBranches.length > 0 ? 200 : 500,
      records: allBranches.length > 0 ? allBranches.length : "None",
      data: {
        message: allBranches.length > 0 ? "Get Branch List" : "No Data Found.",
        allBranches: allBranches.length > 0 ? allBranches : "No Data.",
      },
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single Branch Details ================== //
module.exports.getBranchDetails = async (req, res) => {
  try {
    const { branch_id } = req.params;
    const branchExist = await branches.findByPk(branch_id, {
      where: { isDeleted: false },
    });

    if (!branchExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Branch Not Exist." },
      });
    }

    if (branchExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "Branch Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Details.", details: branchExist },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Branch ================== //
module.exports.updateBranch = async (req, res) => {
  try {
    const { branch_id } = req.params;
    const {
      branchCode,
      branchName,
      gstNo,
      contactPerson,
      contactNumber,
      altContactNumber,
      emailId,
      altEmailId,
      country,
      state,
      city,
      pinCode,
      address,
      billingAddress,
    } = req.body;
    const branchExist = await branches.findByPk(branch_id);

    // If Branch not Exist
    if (!branchExist || branchExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Branch Not Exist." },
      });
    }

    // Check if Already exist
    const getAllBranches = await branches.findOne({
      where: {
        branchCode,
        branchName,
        branch_id: { [db.Sequelize.Op.ne]: branch_id },
      },
    });

    if (getAllBranches) {
      if (getAllBranches.isDeleted === false) {
        return res.status(500).send({
          status: 500,
          data: { message: "Branch Already Exist." },
        });
      } else {
        await branches.update(
          {
            branchCode,
            branchName,
            gstNo,
            contactPerson,
            contactNumber,
            altContactNumber,
            emailId,
            altEmailId,
            country,
            state,
            city,
            pinCode,
            address,
            billingAddress,
          },
          { where: { branch_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Branch Updated Successfully." },
        });
      }
    } else {
      await branches.update(
        {
          branchCode,
          branchName,
          gstNo,
          contactPerson,
          contactNumber,
          altContactNumber,
          emailId,
          altEmailId,
          country,
          state,
          city,
          pinCode,
          address,
          billingAddress,
        },
        { where: { branch_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Update Branch Status ================== //
module.exports.updateBranchStatus = async (req, res) => {
  try {
    const { branch_id } = req.params;
    const branchExist = await branches.findByPk(branch_id, {
      where: { isDeleted: false },
    });

    if (!branchExist || branchExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Branch Not Exist." },
      });
    }

    if (branchExist.status === "Active") {
      await branches.update({ status: "InActive" }, { where: { branch_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Updated Successfully." },
      });
    } else {
      await branches.update({ status: "Active" }, { where: { branch_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete Branch ================== //
module.exports.deleteBranch = async (req, res) => {
  try {
    const { branch_id } = req.params;
    const branchExist = await branches.findByPk(branch_id, {
      where: { isDeleted: false },
    });

    if (!branchExist) {
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Not Exist." },
      });
    }

    if (branchExist.isDeleted === true) {
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Already Deleted." },
      });
    } else {
      await branches.update(
        { isDeleted: true, status: "InActive" },
        { where: { branch_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Branch Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
