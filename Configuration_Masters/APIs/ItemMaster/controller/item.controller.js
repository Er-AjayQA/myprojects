const db = require("../../../indexFiles/modelsIndex");
const items = db.tbl_items;
const assetCategories = db.tbl_assetCategories;
const uoms = db.tbl_uoms;
const sendErrorResponse = require("../../../Utils/utilMethods");

// ================== Create Items ================== //
module.exports.createItem = async (req, res) => {
  try {
    const {
      ast_id,
      itemName,
      rental,
      buyOut,
      mvp,
      barCode,
      uom_id,
      manageBy,
      itemNumber,
      itemDesc,
      thresholdStock,
      isAsset,
    } = req.body;

    const itemExist = await items.findOne({
      where: {
        [db.Sequelize.Op.or]: [
          {
            [db.Sequelize.Op.and]: [{ ast_id }, { itemName }],
          },
          { itemNumber },
        ],
      },
    });

    if (itemExist) {
      if (!itemExist.isDeleted) {
        return res.status(409).send({
          status: 409,
          data: { message: "Item Already Exist." },
        });
      } else {
        await items.update(
          {
            ast_id,
            itemName,
            rental: rental ? rental : 0,
            buyOut: buyOut ? buyOut : 0,
            mvp: mvp ? mvp : 0,
            barCode,
            uom_id,
            manageBy: manageBy ? manageBy : "Batch Only",
            itemNumber,
            itemDesc,
            thresholdStock,
            isAsset,
            isDeleted: false,
            status: "Active",
          },
          { where: { item_id: itemExist.item_id } }
        );
        return res.status(201).send({
          status: 201,
          data: { message: "Item Created Successfully." },
        });
      }
    }
    if (!itemExist) {
      await items.create({
        ast_id,
        itemName,
        rental: rental ? rental : 0,
        buyOut: buyOut ? buyOut : 0,
        mvp: mvp ? mvp : 0,
        barCode,
        uom_id,
        manageBy,
        itemNumber,
        itemDesc,
        thresholdStock,
        isAsset,
      });
      return res.status(201).send({
        status: 201,
        data: { message: "Item Created Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get List Of All Items ================== //
module.exports.getListOfAllItems = async (req, res) => {
  try {
    // const allItems = await items.findAll();

    const allItems = await items.findAll({
      attributes: [
        "item_id",
        "itemName",
        "rental",
        "buyOut",
        "mvp",
        "barCode",
        "manageBy",
        "itemNumber",
        "itemDesc",
        "thresholdStock",
        "isAsset",
        "isDeleted",
        "status",
      ],
      include: [
        {
          model: assetCategories,
          attributes: ["ast_id", "assetName"],
        },
        {
          model: uoms,
          attributes: ["uom_id", "uomName"],
        },
      ],
    });

    return res.status(`${allItems.length > 0 ? 200 : 500}`).send({
      status: allItems.length > 0 ? 200 : 500,
      records: allItems.length > 0 ? allItems.length : "None",
      allItems,
    });
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Get Single Item Details ================== //
module.exports.getItemDetails = async (req, res) => {
  try {
    const { item_id } = req.params;
    const itemExist = await items.findByPk(item_id, {
      attributes: [
        "item_id",
        "itemName",
        "rental",
        "buyOut",
        "mvp",
        "barCode",
        "manageBy",
        "itemNumber",
        "itemDesc",
        "thresholdStock",
        "isAsset",
        "isDeleted",
        "status",
      ],
      include: [
        {
          model: assetCategories,
          attributes: ["ast_id", "assetName"],
        },
      ],
    });

    if (!itemExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Item Not Exist." },
      });
    }

    if (itemExist.isDeleted === true) {
      return res.status(201).send({
        status: 500,
        data: { message: "Item Not Exist." },
      });
    } else {
      return res.status(201).send({
        status: 201,
        data: { message: "Item Details.", details: itemExist },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// // ================== Update Branch ================== //
// module.exports.updateBranch = async (req, res) => {
//   try {
//     const { branch_id } = req.params;
//     const {
//       branchCode,
//       branchName,
//       gstNo,
//       contactPerson,
//       contactNumber,
//       altContactNumber,
//       emailId,
//       altEmailId,
//       country,
//       state,
//       city,
//       pinCode,
//       address,
//       billingAddress,
//     } = req.body;
//     const branchExist = await branches.findByPk(branch_id);

//     // If Branch not Exist
//     if (!branchExist || branchExist.isDeleted === true) {
//       return res.status(500).send({
//         status: 500,
//         data: { message: "Branch Not Exist." },
//       });
//     }

//     // Check if Already exist
//     const getAllBranches = await branches.findOne({
//       where: {
//         branchCode,
//         branchName,
//         branch_id: { [db.Sequelize.Op.ne]: branch_id },
//       },
//     });

//     if (getAllBranches) {
//       if (getAllBranches.isDeleted === false) {
//         return res.status(500).send({
//           status: 500,
//           data: { message: "Branch Already Exist." },
//         });
//       } else {
//         await branches.update(
//           {
//             branchCode,
//             branchName,
//             gstNo,
//             contactPerson,
//             contactNumber,
//             altContactNumber,
//             emailId,
//             altEmailId,
//             country,
//             state,
//             city,
//             pinCode,
//             address,
//             billingAddress,
//           },
//           { where: { branch_id } }
//         );
//         return res.status(201).send({
//           status: 201,
//           data: { message: "Branch Updated Successfully." },
//         });
//       }
//     } else {
//       await branches.update(
//         {
//           branchCode,
//           branchName,
//           gstNo,
//           contactPerson,
//           contactNumber,
//           altContactNumber,
//           emailId,
//           altEmailId,
//           country,
//           state,
//           city,
//           pinCode,
//           address,
//           billingAddress,
//         },
//         { where: { branch_id } }
//       );
//       return res.status(201).send({
//         status: 201,
//         data: { message: "Branch Updated Successfully." },
//       });
//     }
//   } catch (error) {
//     sendErrorResponse(error, res);
//   }
// };

// ================== Update Item Status ================== //
module.exports.updateItemStatus = async (req, res) => {
  try {
    const { item_id } = req.params;
    const itemExist = await items.findByPk(item_id, {
      where: { isDeleted: false },
    });

    if (!itemExist || itemExist.isDeleted === true) {
      return res.status(500).send({
        status: 500,
        data: { message: "Item Not Exist." },
      });
    }

    if (itemExist.status === "Active") {
      await items.update({ status: "InActive" }, { where: { item_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Item Updated Successfully." },
      });
    } else {
      await items.update({ status: "Active" }, { where: { item_id } });
      return res.status(201).send({
        status: 201,
        data: { message: "Item Updated Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};

// ================== Delete Items ================== //
module.exports.deleteItem = async (req, res) => {
  try {
    const { item_id } = req.params;
    const itemExist = await items.findByPk(item_id, {
      where: { isDeleted: false },
    });

    if (!itemExist) {
      return res.status(500).send({
        status: 500,
        data: { message: "Item Not Exist." },
      });
    }

    if (itemExist.isDeleted === true) {
      return res.status(409).send({
        status: 409,
        data: { message: "Item Already Deleted." },
      });
    } else {
      await items.update(
        { isDeleted: true, status: "InActive" },
        { where: { item_id } }
      );
      return res.status(201).send({
        status: 201,
        data: { message: "Item Deleted Successfully." },
      });
    }
  } catch (error) {
    sendErrorResponse(error, res);
  }
};
