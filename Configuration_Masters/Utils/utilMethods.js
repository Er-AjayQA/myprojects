const sendErrorResponse = (error, res) => {
  console.log(error.message);
  return res.status(500).send({
    status: 500,
    error: { message: error.message },
  });
};

module.exports = sendErrorResponse;
