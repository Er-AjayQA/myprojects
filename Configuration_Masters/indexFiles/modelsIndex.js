// ========== IMPORTS and CONFIGS ========== //
const config = require("../configFiles/db.config");
const Sequelize = require("sequelize");

// ========== DB CONFIG ========== //
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// ========== MODEL ========== //
db.tbl_assetCategories = require("../APIs/AssetCategory/models/asset.model")(
  sequelize,
  Sequelize
);
db.tbl_uoms = require("../APIs/UOM/models/uom.model")(sequelize, Sequelize);
db.tbl_departments = require("../APIs/Department/models/department.model")(
  sequelize,
  Sequelize
);
db.tbl_designations = require("../APIs/Designation/models/designation.model")(
  sequelize,
  Sequelize
);
db.tbl_serviceCategories =
  require("../APIs/ServiceCategory/models/serviceCat.model")(
    sequelize,
    Sequelize
  );
db.tbl_services = require("../APIs/ServiceMaster/models/service.model")(
  sequelize,
  Sequelize
);

// Relation Between Service Category and Services Tables
db.tbl_serviceCategories.hasMany(db.tbl_services, { foreignKey: "srvCat_id" });
db.tbl_services.belongsTo(db.tbl_serviceCategories, {
  foreignKey: "srvCat_id",
});

db.tbl_countries = require("../APIs/countryStateMaster/models/countries.model")(
  sequelize,
  Sequelize
);
db.tbl_states = require("../APIs/countryStateMaster/models/states.model")(
  sequelize,
  Sequelize
);
db.tbl_branches = require("../APIs/BranchSetupMaster/models/branch.model")(
  sequelize,
  Sequelize
);
db.tbl_items = require("../APIs/ItemMaster/models/item.model")(
  sequelize,
  Sequelize
);

// Relation Between Asset Category and Items Tables
db.tbl_assetCategories.hasMany(db.tbl_items, { foreignKey: "ast_id" });
db.tbl_items.belongsTo(db.tbl_assetCategories, {
  foreignKey: "ast_id",
});
// Relation Between UOM and Items Tables
db.tbl_uoms.hasMany(db.tbl_items, { foreignKey: "uom_id" });
db.tbl_items.belongsTo(db.tbl_uoms, {
  foreignKey: "uom_id",
});
db.tbl_itemSpecifications =
  require("../APIs/ItemMaster/models/itemSpecifications.model")(
    sequelize,
    Sequelize
  );

// Relation Between Items and Items Specifications Tables
db.tbl_items.hasMany(db.tbl_itemSpecifications, {
  foreignKey: "item_id",
});
db.tbl_itemSpecifications.belongsTo(db.tbl_items, {
  foreignKey: "item_id",
});

module.exports = db;
