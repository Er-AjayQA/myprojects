import { useState } from "react";
import todoStyle from "./ToDo.module.css";
import { IoMdCheckmarkCircleOutline } from "react-icons/io";
import { MdDeleteForever } from "react-icons/md";

export const ToDoList = ({
  data,
  onHandleDelete,
  checked,
  onHandleCheckedToDo,
}) => {
  return (
    <>
      <li className={`${todoStyle["todo-item"]} `}>
        <span
          className={`${
            checked ? `${todoStyle.checkList}` : `${todoStyle.notCheckList}`
          }`}
        >
          {data}
        </span>
        <button
          className={todoStyle["check-btn"]}
          onClick={(event) => {
            onHandleCheckedToDo(event, data);
          }}
        >
          <IoMdCheckmarkCircleOutline />
        </button>
        <button
          className={todoStyle["delete-btn"]}
          onClick={(event) => onHandleDelete(event, data)}
        >
          <MdDeleteForever />
        </button>
      </li>
    </>
  );
};
