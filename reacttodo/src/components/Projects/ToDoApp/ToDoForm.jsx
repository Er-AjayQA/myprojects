import { useState } from "react";
import todoStyle from "./ToDo.module.css";

export const ToDoForm = ({ onAddToDo }) => {
  const [inputValue, setInputValue] = useState({});
  //   Handle Form
  const handleInputChange = (value) => {
    setInputValue({ id: value, content: value, checked: false });
  };

  // Calling Handle Form Submit Event
  const handleFormSubmit = (event) => {
    event.preventDefault();
    onAddToDo(inputValue);
    setInputValue({ id: "", content: "", checked: false });
  };
  return (
    <>
      <section className={todoStyle["todo-form"]}>
        <form onSubmit={handleFormSubmit}>
          <div>
            <input
              type="text"
              className={todoStyle["todo-input"]}
              autoComplete="off"
              value={inputValue.content}
              onChange={(event) => handleInputChange(event.target.value)}
            />
          </div>
          <div>
            <button type="submit" className={todoStyle["todo-btn"]}>
              Add Task
            </button>
          </div>
        </form>
      </section>
    </>
  );
};
