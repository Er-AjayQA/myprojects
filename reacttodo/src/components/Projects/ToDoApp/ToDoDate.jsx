import { useEffect, useState } from "react";
import todoStyle from "./ToDo.module.css";

export const ToDoDate = () => {
  const [dateTime, setDateTime] = useState("");
  //   Date - Time
  useEffect(() => {
    const interval = setInterval(() => {
      const now = new Date();
      const formattedDate = now.toLocaleDateString();
      const formattedTime = now.toLocaleTimeString();
      setDateTime(`${formattedDate} - ${formattedTime}`);
    }, 1000);

    // CleanUp Function
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <h2 className={todoStyle["date-time"]}>{dateTime}</h2>
    </>
  );
};
