import { useState } from "react";
import todoStyle from "./ToDo.module.css";
import { ToDoForm } from "./ToDoForm";
import { ToDoList } from "./ToDoList";
import { ToDoDate } from "./ToDoDate";
import {
  getToDoLocalStorageData,
  setToDoLocalStorageData,
} from "./ToDoLocalStorage";

export const ToDo = () => {
  const [task, setTask] = useState(() => getToDoLocalStorageData());

  // todo handleFormSubmit functionality
  const handleFormSubmit = (inputValue) => {
    const { id, content, checked } = inputValue;
    if (!content) return;

    const isContentExist = task.find((curTask) => curTask.content === content);
    if (isContentExist) return;

    setTask((preTask) => {
      return [...preTask, { id, content, checked }];
    });
  };

  // todo Add to localstorage
  setToDoLocalStorageData(task);

  // todo handleDelete functionality
  const handleDelete = (event, value) => {
    event.preventDefault();
    const updatedTaskList = task.filter((curTask) => curTask.content !== value);
    setTask(updatedTaskList);
  };

  // todo handleClearAll functionality
  const handleClearAll = () => {
    setTask([]);
  };

  // todo handleCheckedToDo functionality
  const handleCheckedToDo = (event, value) => {
    event.preventDefault();
    const updatedTask = task.map((curTask) => {
      if (curTask.content === value) {
        return { ...curTask, checked: !curTask.checked };
      } else {
        return curTask;
      }
    });

    setTask(updatedTask);
    // console.log(updatedTask);
  };

  return (
    <>
      <section className={todoStyle["todo-container"]}>
        <header>
          <h1>ToDo List</h1>
          <ToDoDate />
        </header>
        <ToDoForm onAddToDo={handleFormSubmit} />
        <section className={todoStyle.myUnOrdList}>
          <ul className={todoStyle["todo-list"]}>
            {task.map((curTask) => {
              return (
                <ToDoList
                  key={curTask.id}
                  data={curTask.content}
                  checked={curTask.checked}
                  onHandleDelete={handleDelete}
                  onHandleCheckedToDo={handleCheckedToDo}
                />
              );
            })}
          </ul>
        </section>
        <section>
          <button className={todoStyle["clear-btn"]} onClick={handleClearAll}>
            Clear All
          </button>
        </section>
      </section>
    </>
  );
};
