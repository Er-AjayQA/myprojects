const toDoKey = "reactToDoData";

export const getToDoLocalStorageData = () => {
  const rawToDo = localStorage.getItem(toDoKey);
  if (!rawToDo) return [];
  return JSON.parse(rawToDo);
};

export const setToDoLocalStorageData = (task) => {
  return localStorage.setItem(toDoKey, JSON.stringify(task));
};
