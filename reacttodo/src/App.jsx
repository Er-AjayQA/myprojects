import { ToDo } from "./components/Projects/ToDoApp/ToDo";
import evStyle from "./components/EV.module.css";

export const App = () => {
  return (
    <section>
      <ToDo />
    </section>
  );
};
