// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");
const authorRouter = require("./APIs/Author/Router/author.router");
const blogRouter = require("./APIs/Blogs/Router/blog.router");

// ========== COMMON CONFIG ========== //
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const port = process.env.PORT || 3000;

// ========== HOME ROUTES ========== //
app.get("/blogging", (req, res) => {
  try {
    return res.status(200).send({
      statusCode: 200,
      data: { message: "Welcome to Blogging Site Backend." },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
});

// ========== DB SYNC ========== //
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    console.log("Connected to database successfully");
  })
  .catch((err) => {
    console.error(err);
  });

// ========== ROUTER ========== //
app.use("/blogging", authorRouter);
app.use("/blogging", blogRouter);

// ========== LISTENING SERVER ========== //
app.listen(port, (err) => {
  if (!err) {
    console.log(`Listening to the server at port : ${port}`);
  } else {
    console.log(err);
  }
});
