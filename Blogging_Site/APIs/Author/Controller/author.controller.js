// ================== IMPORTS ================== //
const jwt = require("jsonwebtoken");
require("dotenv").config();
const Authors = require("../Model/author.model");

// ================== Create Author Details ================== //
module.exports.createAuthor = async (req, res) => {
  try {
    const { title, fName, lName, email, password } = req.body;
    const newAuthor = new Authors({ title, fName, lName, email, password });
    await newAuthor.save();

    return res.status(201).send({
      statusCode: 201,
      data: { message: "Author Created Successfully" },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Update Author Details ================== //
module.exports.updateAuthor = async (req, res) => {
  try {
    const { _id } = req.params;
    const { title, fName, lName, email, password } = req.body;
    const authId = req.authId; // Get the authenticated author ID from the request object

    if (authId !== _id) {
      return res.status(500).send({
        statusCode: 500,
        data: { message: "Not Authorized User" },
      });
    }

    const authorExist = await Authors.findById(_id);

    if (!authorExist) {
      return res.status(500).send({
        statusCode: 500,
        data: { message: "Author Not Exist" },
      });
    }

    if (authorExist._id.toString() === _id) {
      const updateAuthor = await Authors.updateOne(
        { _id },
        {
          title,
          fName,
          lName,
          email,
          password,
        }
      );

      return res.status(201).send({
        statusCode: 201,
        data: { message: "Author Updated Successfully" },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Login Author ================== //
module.exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const authorExist = await Authors.findOne({ email });

    if (!authorExist) {
      return res.status(500).send({
        statusCode: 500,
        data: { message: "Author Not Exist" },
      });
    }

    if (authorExist.password === password) {
      const token = jwt.sign({ authId: authorExist._id }, process.env.SECRETE);

      return res.status(201).send({
        statusCode: 201,
        data: { message: "User Login", token: token },
      });
    }
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
