const express = require("express");
const router = express.Router();
const authenticator = require("../../../middlewares/auth");
const authorController = require("../Controller/author.controller");

router.post("/createAuthor", authorController.createAuthor);
router.put("/updateAuthor/:_id", authenticator, authorController.updateAuthor);
router.post("/login", authorController.login);

module.exports = router;
