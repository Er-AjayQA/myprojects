const mongoose = require("mongoose");
// const AutoIncrement = require("mongoose-sequence")(mongoose);
const validator = require("validator");

const authorSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      enum: {
        values: ["Mr", "Mrs", "Miss"],
        message: "{VALUE} is not supported. Allowed values are Mr, Mrs, Miss",
      },
      required: [true, "Title is mandatory"],
    },
    fName: {
      type: String,
      required: [true, "First name is mandatory"],
    },
    lName: {
      type: String,
      required: [true, "Last name is mandatory"],
    },
    email: {
      type: String,
      required: [true, "Email is mandatory"],
      unique: true,
      lowercase: true,
      validate: [validator.isEmail, "Please provide a valid email address"],
    },
    password: {
      type: String,
      required: [true, "Password is mandatory"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("authors", authorSchema);
