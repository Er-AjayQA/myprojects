const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const blogSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Title is mandatory"],
    },
    body: {
      type: String,
      required: [true, "Content is mandatory"],
    },
    authId: {
      type: Schema.Types.ObjectId,
      ref: "authors",
      required: [true, "Author ID is mandatory"],
    },
    tags: {
      type: [String],
    },
    category: {
      type: String,
      required: [true, "Category is mandatory"],
      enum: {
        values: ["technology", "entertainment", "lifestyle", "food", "fashion"],
        message: "{VALUE} is not a valid category",
      },
    },
    subcategory: {
      type: [String],
      required: [true, "Category is mandatory"],
      validate: function (value) {
        const subcategories = {
          technology: ["web development", "mobile development", "AI", "ML"],
          entertainment: ["Cricket", "Snooker", "Chess"],
          lifestyle: ["Yoga", "Bodybuilding"],
          food: ["Indian", "Japanese", "Italian", "Chinese"],
          fashion: ["Sneakers", "Gym Costumes"],
        };
        const allowedCategories = subcategories[this.category] || [];
        return value.every((subcat) => allowedCategories.includes(subcat));
      },
      message: "Invalid subcategory",
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    updatedAt: {
      type: Date,
      default: Date.now,
    },
    deletedAt: {
      type: Date,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    publishedAt: {
      type: Date,
    },
    isPublished: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("blogs", blogSchema);
