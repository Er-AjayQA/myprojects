const express = require("express");
const router = express.Router();
const authenticator = require("../../../middlewares/auth");
const blogController = require("../Controller/blog.controller");

router.post("/createBlog", authenticator, blogController.createBlog);
router.get("/getAllBlogs", authenticator, blogController.getAllBlogs);
router.get("/getBlogsByFilter", authenticator, blogController.getBlogsByFilter);
router.put("/updateBlog/:blogId", authenticator, blogController.updateBlog);
router.delete("/deleteBlog/:blogId", authenticator, blogController.deleteBlog);
router.delete(
  "/deleteBlogByFilter",
  authenticator,
  blogController.deleteBlogByFilter
);

module.exports = router;
