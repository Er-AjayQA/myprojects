// ================== IMPORTS ================== //
const Authors = require("../../Author/Model/author.model");
const Blogs = require("../Model/blog.model");
const { ObjectId } = require("mongoose").Types;

// ================== Create Blog ================== //
module.exports.createBlog = async (req, res) => {
  try {
    const {
      title,
      body,
      authId,
      tags,
      category,
      subcategory,
      isDeleted,
      publishedAt,
      isPublished,
    } = req.body;

    const authorExist = await Authors.findById(authId);

    if (!authorExist) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "Author Not Exist" },
      });
    }

    const newBlog = new Blogs({
      title,
      body,
      authId,
      tags,
      category,
      subcategory,
      isDeleted,
      publishedAt,
      isPublished,
    });

    await newBlog.save();

    return res.status(201).send({
      statusCode: 201,
      data: { message: "Author Created Successfully", blogDetails: newBlog },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Get Blog ================== //
module.exports.getAllBlogs = async (req, res) => {
  try {
    const getAllBlogs = await Blogs.find({
      isDeleted: false,
      isPublished: true,
    });

    if (!getAllBlogs.length > 0) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "Blogs Not Available" },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      records: getAllBlogs.length,
      data: { message: "All Blogs Fetched", getAllBlogs },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Get Blog By Filter ================== //
module.exports.getBlogsByFilter = async (req, res) => {
  try {
    const { authId, category, subcategory, tags } = req.query;
    const query = {};

    if (authId) {
      query.authId = authId;
    }
    if (category) {
      query.category = category;
    }
    if (subcategory) {
      query.subcategory = subcategory;
    }
    if (tags) {
      query.tags = tags;
    }

    const blogExist = await Blogs.find(query);

    if (!blogExist.length > 0) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Blogs Exist" },
      });
    }

    return res.status(200).send({
      statusCode: 200,
      records: blogExist.length,
      data: { message: "All Blogs Fetched", blogExist },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Update Blog ================== //
module.exports.updateBlog = async (req, res) => {
  try {
    const _id = req.params.blogId;
    const {
      title,
      body,
      authId,
      tags,
      category,
      subcategory,
      isDeleted,
      publishedAt,
      isPublished,
    } = req.body;

    const authorId = req.authId;
    const blogExist = await Blogs.findOne({ _id });

    if (authorId !== blogExist.authId.toString()) {
      return res.status(500).send({
        statusCode: 500,
        data: { message: "Not Authorized User" },
      });
    }

    if (!blogExist || blogExist.isDeleted) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Blogs Exist" },
      });
    }

    const updateBlog = await Blogs.findOneAndUpdate(
      { _id },
      {
        title,
        body,
        authId,
        tags,
        category,
        subcategory,
        isDeleted,
        publishedAt,
        isPublished,
      },
      { new: true }
    );

    return res.status(201).send({
      statusCode: 201,
      data: { message: "Blog Updated Successfully", updateBlog },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Delete Blog ================== //
module.exports.deleteBlog = async (req, res) => {
  try {
    const _id = req.params.blogId;
    const authId = req.authId;

    const blogExist = await Blogs.findOne({ _id, isDeleted: false });

    if (!blogExist) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Blogs Exist" },
      });
    }

    if (blogExist.authId.toString() !== authId) {
      return res.status(500).send({
        statusCode: 500,
        data: { message: "Not Authorized User" },
      });
    }

    await Blogs.updateOne({ _id }, { isDeleted: true });

    return res.status(200).send({
      statusCode: 200,
      data: { message: "Blog Deleted Successfully" },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};

// ================== Delete Blog By Filter ================== //
module.exports.deleteBlogByFilter = async (req, res) => {
  try {
    const { authId, category, subcategory, tags, isPublished } = req.query;
    const authorId = req.authId;
    const query = {};
    query.isDeleted = false;

    if (authId) {
      query.authId = authId;
    }
    if (category) {
      query.category = category;
    }
    if (subcategory) {
      query.subcategory = subcategory;
    }
    if (tags) {
      query.tags = tags;
    }
    if (isPublished) {
      query.isPublished = false;
    }

    const blogExist = await Blogs.find(query);

    if (!blogExist.length > 0) {
      return res.status(404).send({
        statusCode: 404,
        data: { message: "No Blogs Exist" },
      });
    }

    const notValidFor = [];

    for (const blog of blogExist) {
      if (blog.authId.toString() !== authorId) {
        notValidFor.push(blog._id);
      } else {
        await Blogs.updateOne({ _id: blog._id }, { isDeleted: true });
      }
    }

    res.status(200).send({
      statusCode: 200,
      notAuthorizedFor: { notValidFor },
      data: { message: "Blog Deleted Successfully" },
    });
  } catch (error) {
    return res.status(500).send({
      statusCode: 500,
      data: { error: error.message },
    });
  }
};
