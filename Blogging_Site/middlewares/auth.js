// ================== IMPORTS ================== //
const jwt = require("jsonwebtoken");
require("dotenv").config();
const Authors = require("../APIs/Author/Model/author.model");

// ================== Authentication Method ================== //
const authenticator = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (!token) {
    return res.status(401).send({
      status: 401,
      error: { message: "User Unauthorized." },
    });
  }

  jwt.verify(token, process.env.SECRETE, async (err, author) => {
    if (err) {
      return res.status(403).send({
        status: 403,
        error: { message: "Invalid Token" },
      });
    }

    let authId = author.authId;
    let authorExist = await Authors.findById(authId);
    if (!authorExist) {
      return res.status(400).send({
        status: 400,
        data: { message: "Author not exist." },
      });
    } else {
      req.authId = authId;
      next();
    }
  });
};

module.exports = authenticator;
