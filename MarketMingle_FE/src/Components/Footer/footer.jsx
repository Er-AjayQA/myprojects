import footerStyle from "./footer.module.css";

export const Footer = () => {
  const currentDate = new Date();

  return (
    <>
      <div className={footerStyle["footer-content"]}>
        <p className={footerStyle["ft-title"]}>
          Copyright {currentDate.getFullYear()}
        </p>
      </div>
    </>
  );
};
