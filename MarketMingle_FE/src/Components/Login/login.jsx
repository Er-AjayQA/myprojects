import style from "./login.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";
import { faUser, faKey } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";

function Login() {
  const [showPassword, setShowPassword] = useState(false);

  function handlePwdVisibility() {
    setShowPassword(!showPassword);
  }
  return (
    <>
      <div className={style["form-Content"]}>
        <div className={style["login-form"]}>
          <div className={style.overlay}></div>
          <div className={style["form-title"]}>
            <h2>Login</h2>
          </div>
          <form className={style["lg-form"]}>
            <fieldset>
              <FontAwesomeIcon
                icon={faUser}
                className={`${style.icon} ${style["user-icon"]}`}
              />
              <input type="email" placeholder="Email/Mobile" />
            </fieldset>
            <fieldset>
              <FontAwesomeIcon
                icon={faKey}
                className={`${style.icon} ${style["key-icon"]}`}
              />
              <input
                type={showPassword ? "text" : "password"}
                placeholder="Password"
              />
              <FontAwesomeIcon
                icon={showPassword ? faEye : faEyeSlash}
                className={style["eye-icon"]}
                onClick={handlePwdVisibility}
              />
            </fieldset>
            <fieldset className={style["btn-group"]}>
              <a href="#" target="_blank">
                <button className={`${style.btn} ${style["login-btn"]}`}>
                  Login
                </button>
              </a>
            </fieldset>
            <fieldset className={style["account-links"]}>
              <a href="#">Forgot Password ?</a>
              <a href="#">Don't Have Account ?</a>
            </fieldset>
          </form>
        </div>
      </div>
    </>
  );
}

export default Login;
