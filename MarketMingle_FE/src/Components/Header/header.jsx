import headerStyle from "./header.module.css";

export const Header = () => {
  return (
    <>
      <div className={headerStyle["header-content"]}>
        <div className={headerStyle["logo-container"]}>
          <span className={headerStyle.logo}>
            M<sup>2</sup>
          </span>
        </div>
        <div className={headerStyle["profile-container"]}></div>
      </div>
    </>
  );
};
