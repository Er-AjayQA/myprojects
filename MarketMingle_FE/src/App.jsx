import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import Login from "./Components/Login/login";
import { Footer } from "./Components/Footer/footer";
import { Header } from "./Components/Header/header";
import "./index.css";

function App() {
  return (
    <>
      <Header />
      <div className="container">
        <Login />
      </div>
      <div className="container">
        <Footer />
      </div>
    </>
  );
}

export default App;
