// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const orderController = require("../Controllers/order.controller");

// ========== Routes ========== //
router.post("/orders", orderController.createOrder);

// ========== EXPORTS ========== //
module.exports = router;
