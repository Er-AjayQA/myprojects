// ========== IMPORTS and CONFIGS ========== //
const Orders = require("../Models/order.model");
const Pizzas = require("../../Pizza/Models/pizza.model");

// ========== Create Order ========== //
module.exports.createOrder = async (req, res) => {
  try {
    const { customerName, pizzas, totalAmount, mobileNumber } = req.body;

    const createOrder = new Orders({
      customerName,
      pizzas,
      totalAmount,
      mobileNumber,
    });
    const saveOrder = await createOrder.save();
    return res.status(201).send({
      statusCode: 201,
      message: "Order Created Successfully",
      orderDetails: createOrder,
    });
  } catch (error) {
    return res.status(500).send({ statusCode: 500, message: error.message });
  }
};
