// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define Order Schema ========== //
const orderSchema = new Schema(
  {
    customerName: {
      type: String,
      required: true,
    },
    pizzas: {
      type: [String],
      required: true,
    },
    totalAmount: {
      type: Number,
      required: true,
    },
    mobileNumber: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// ========== EXPORTS ========== //
module.exports = new mongoose.model("orders", orderSchema);
