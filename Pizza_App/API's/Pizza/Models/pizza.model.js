// ========== IMPORTS and CONFIGS ========== //
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// ========== Define Pizza Schema ========== //
const pizzaSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    size: {
      type: String,
      required: true,
    },
    toppings: {
      type: [String],
      required: true,
      enum: ["tomato sauce", "mozzarella", "basil", "mushrooms", "olives"],
    },
    totalAmount: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

// ========== EXPORTS ========== //
module.exports = new mongoose.model("pizzas", pizzaSchema);
