// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const pizzaController = require("../Controllers/pizza.controller");

// ========== Routes ========== //
router.post("/pizzas", pizzaController.createPizza);
router.get("/pizzas", pizzaController.getAllPizzas);
router.get("/pizzas/:_id", pizzaController.getPizzaById);

// ========== Exports ========== //
module.exports = router;
