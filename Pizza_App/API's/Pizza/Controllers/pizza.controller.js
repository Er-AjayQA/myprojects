// ========== IMPORTS and CONFIGS ========== //
const Pizzas = require("../Models/pizza.model");

// ========== Create Pizza ========== //
module.exports.createPizza = async (req, res) => {
  try {
    const { name, size, toppings, totalAmount } = req.body;

    const pizzaExist = await Pizzas.findOne({ name, size, toppings });

    if (pizzaExist) {
      return res
        .status(403)
        .send({ statusCode: 403, message: "Pizza Already Exist" });
    }

    const createPizza = new Pizzas({ name, size, toppings, totalAmount });
    const savePizza = await createPizza.save();
    return res.status(201).send({
      statusCode: 201,
      message: "Pizza Created Successfully",
      pizza: savePizza,
    });
  } catch (error) {
    return res.status(500).send({ statusCode: 500, message: error.message });
  }
};

// ========== Get All Pizza ========== //
module.exports.getAllPizzas = async (req, res) => {
  try {
    const getAllPizzas = await Pizzas.find();

    if (getAllPizzas.length <= 0) {
      return res
        .status(403)
        .send({ statusCode: 403, message: "No Pizzas Found" });
    }

    return res.status(200).send({
      statusCode: 200,
      message: "List Of All Pizzas",
      totalCount: getAllPizzas.length,
      allPizzas: getAllPizzas,
    });
  } catch (error) {
    return res.status(500).send({ statusCode: 500, message: error.message });
  }
};

// ========== Get Single Pizza ========== //
module.exports.getPizzaById = async (req, res) => {
  try {
    const { _id } = req.params;
    const getPizza = await Pizzas.findById(_id);

    if (!getPizza) {
      return res
        .status(403)
        .send({ statusCode: 403, message: "No Pizza Found" });
    }

    return res.status(200).send({
      statusCode: 200,
      message: "Pizza Detail",
      pizzaDetails: getPizza,
    });
  } catch (error) {
    return res.status(500).send({ statusCode: 500, message: error.message });
  }
};
