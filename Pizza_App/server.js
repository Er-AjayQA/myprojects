// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const port = process.env.PORT || 3000;
const pizzaRouter = require("./API's/Pizza/Routers/pizza.router");
const orderRouter = require("./API's/Orders/Routers/order.router");

// ========== COMMON CONFIG ========== //
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ========== HOME ROUTES ========== //
app.get("/api/pizza/home", (req, res) => {
  try {
    return res
      .status(200)
      .send({ statusCode: 200, data: "Welcome to Pizza App Backend!!" });
  } catch (error) {
    return res.status(500).send({ statusCode: 500, message: error.message });
  }
});
// ========== DB SYNC ========== //
mongoose
  .connect(process.env.DB_URL)
  .then(() => {
    console.log("DB Synced Successfully...............");
  })
  .catch((error) => {
    console.log("DB Synced Failed...............", error);
  });

// ========== ROUTER ========== //
app.use("/api", pizzaRouter);
app.use("/api", orderRouter);

// ========== LISTENING SERVER ========== //
app.listen(process.env.PORT, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Listening to the server at port : ${port}`);
  }
});
