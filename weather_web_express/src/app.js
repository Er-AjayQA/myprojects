const path = require("path");
const express = require("express");
const hbs = require("hbs");
const app = express();
let port = process.env.PORT || 8000;

const webPath = path.join(__dirname, "../public");
const template_Path = path.join(__dirname, "../templates/views");
const partials_Path = path.join(__dirname, "../templates/partials");

app.use(express.static(webPath));

app.set("view engine", "hbs");
app.set("views", template_Path);
hbs.registerPartials(partials_Path);

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/about", (req, res) => {
  res.render("about");
});

app.get("/weather", (req, res) => {
  res.render("weather");
});

app.get("*", (req, res) => {
  res.render("404error", { errorMsg: "Oops! Page Not Found!" });
});

app.listen(port, (err) => {
  if (!err) {
    console.log(`Server running on port : ${port}`);
  } else {
    console.log(err);
  }
});
