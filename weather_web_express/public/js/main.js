const submitBtn = document.getElementById("submitBtn");
const cityName = document.getElementById("cityName");
const city_name = document.getElementById("city_name");
const temp_status = document.getElementById("temp_status");
const temp = document.getElementById("temp");
const dataHide = document.querySelector(".middle_layer");

// Get Weather Info from API
const getInfo = async (event) => {
  event.preventDefault();
  let cityVal = cityName.value;

  if (cityVal === "") {
    city_name.innerText = `Please Enter City Name`;
    dataHide.classList.add("data_hide");
  } else {
    try {
      let url = `https://api.openweathermap.org/data/2.5/weather?q=${cityVal}&appid=d719daf58da59b42077256a3646147be&units=metric`;
      const response = await fetch(url);
      const data = await response.json();
      const arrData = [data];
      // Display City and Country Name
      city_name.innerText = `${arrData[0].name}, ${arrData[0].sys.country}`;
      // Display Temperature
      temp.innerText = arrData[0].main.temp;
      temp.innerHTML = `${arrData[0].main.temp}<sup>o</sup>C`;

      // Temprature Condition
      const tempMod = arrData[0].weather[0].main;
      if (tempMod == "Clouds") {
        temp_status.innerHTML = `<i class="fa-solid fa-cloud" style="color: #63E6BE;"></i>`;
      } else if (tempMod == "Haze") {
        temp_status.innerHTML = `<i class="fa-solid fa-clouds" style="color: #74C0FC;"></i>`;
      } else if (tempMod == "Rain") {
        temp_status.innerHTML = `<i class="fa-solid fa-raindrops" style="color: #b8d0f9;"></i>`;
      } else if (tempMod == "Clear") {
        temp_status.innerHTML = `<i class="fa-regular fa-sun" style="color: #ffffff;"></i>`;
      } else {
        temp_status.innerHTML = `<i class="fa-solid fa-sun" style="color: #ffffff;"></i>`;
      }

      // Hiding the Temperature and Temperature Status if error occurs
      dataHide.classList.remove("data_hide");
    } catch {
      city_name.innerText = `Please Enter the Correct City Name!`;
      dataHide.classList.add("data_hide");
    }
  }
};

submitBtn.addEventListener("click", getInfo);
