// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const bankController = require("../Controllers/bank.controller");

// ========== ROUTES ========== //
router.post("/create_bank", bankController.create_bank);
router.put("/update_bank/:id", bankController.update_bank);
router.put("/update_bankStatus/:id", bankController.update_bankStatus);
router.get("/get_bank_byId/:id", bankController.get_bank_byId);
router.get("/getAll_bank", bankController.getAll_bank);
router.get("/getAllActive_bank", bankController.getAllActive_bank);
router.delete("/delete_bank/:id", bankController.delete_bank);

module.exports = router;
