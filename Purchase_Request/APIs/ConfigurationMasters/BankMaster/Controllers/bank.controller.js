// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const BANK = db.tbl_bankMaster;

// ================== Create Bank ================== //
module.exports.create_bank = async (req, res) => {
  try {
    const { bank_name } = req.body;

    const isAlreadyExist = await BANK.findOne({
      where: { bank_name, isDeleted: { [Op.eq]: false } },
    });

    if (isAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: { message: "Bank Already Existed!" },
      });
    }

    const createBank = await BANK.create({
      bank_name,
    });
    return res.status(201).send({
      code: 201,
      status: "Bank Created Successfully!",
      data: createBank,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update UOM ================== //
module.exports.update_bank = async (req, res) => {
  try {
    const { id } = req.params;
    const { bank_name } = req.body;

    // Check if the Bank exists or not
    const isExist = await BANK.findByPk(id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Bank Not Found!" },
      });
    }

    // Check if the new name already exists
    const isNameAlreadyExist = await BANK.findOne({
      where: { bank_name, id: { [Op.ne]: id } },
    });

    if (isNameAlreadyExist) {
      return res.status(409).send({
        code: 409,
        status: "Bank Already Existed!",
      });
    }

    // Update if updated Bank name not already exists
    const updatedBank = await BANK.update(
      { bank_name },
      { where: { id }, returning: true, plain: true }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedBank ? "Bank Updated Successfully!" : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Bank Status ================== //
module.exports.update_bankStatus = async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the UOM exists or not
    const isExist = await BANK.findByPk(id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Bank Not Found!" },
      });
    }

    // Update the status
    const updatedStatus = await BANK.update(
      { status: !isExist.status },
      { where: { id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedStatus
          ? "Bank Status Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get Bank By Id ================== //
module.exports.get_bank_byId = async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the Bank exists or not
    const isExist = await BANK.findByPk(id, {
      attributes: ["id", "bank_name", "isDeleted", "status"],
    });

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Bank Not Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "Get Bank Successfully",
      data: isExist,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All Bank ================== //
module.exports.getAll_bank = async (req, res) => {
  try {
    // Check if the Bank exists or not
    const getAllData = await BANK.findAll({
      attributes: ["id", "bank_name", "status", "isDeleted"],
      where: { isDeleted: false },
    });

    if (!getAllData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No Bank Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All Bank List",
      data: getAllData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All Active UOM ================== //
module.exports.getAllActive_bank = async (req, res) => {
  try {
    // Check if the Bank exists or not
    const getAllActiveData = await BANK.findAll({
      attributes: ["id", "bank_name", "status", "isDeleted"],
      where: { isDeleted: false, status: true },
    });

    if (!getAllActiveData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No Active Bank Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All Active Bank List",
      data: getAllActiveData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Delete Bank ================== //
module.exports.delete_bank = async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the Bank exists or not
    const isExist = await BANK.findByPk(id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Bank Not Found!" },
      });
    }

    // Delete the Bank if exist
    const deleteBank = await BANK.update(
      { isDeleted: true },
      { where: { id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        deleteBank ? "Bank Deleted Successfully!" : "Error While Deleting"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};
