// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const categoryController = require("../Controllers/asset_category.controller");

// ========== ROUTES ========== //
router.post("/create_assetCategory", categoryController.create_assetCategory);
router.put(
  "/update_assetCategory/:asst_id",
  categoryController.update_assetCategory
);
router.put(
  "/update_categoryStatus/:asst_id",
  categoryController.update_categoryStatus
);
router.get("/get_category_byId/:asst_id", categoryController.get_category_byId);
router.get("/getAll_category", categoryController.getAll_category);
router.get("/getAllActive_category", categoryController.getAllActive_category);
router.delete("/delete_category/:asst_id", categoryController.delete_category);

module.exports = router;
