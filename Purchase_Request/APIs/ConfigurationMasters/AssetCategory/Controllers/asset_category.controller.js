// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const Category = db.tbl_assetCategories;

// ================== Create Asset Category ================== //
/**
 * @swagger
 * /:
 *  post:
 *    summary: Create Asset Category
 *    description: This API is used to create the Asset Category.
 *    responses:
 *        201:
 *            description: Asset Category Created Successfully!.
 */
module.exports.create_assetCategory = async (req, res) => {
  try {
    const { asst_name, asst_desc } = req.body;

    const isAlreadyExist = await Category.findOne({
      where: { asst_name, isDeleted: false },
    });

    if (isAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: { message: "Asset Category Already Existed!" },
      });
    } else {
      const createCategory = await Category.create({
        asst_name,
        asst_desc,
      });
      return res.status(200).send({
        code: 200,
        status: "Asset Category Created Successfully!",
        data: createCategory,
      });
    }
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Asset Category ================== //
/**
 * @swagger
 * /:
 *  put:
 *    summary: Update Asset Category
 *    description: This API is used to create the Asset Category.
 *    responses:
 *        201:
 *            description: Asset Category Created Successfully!.
 */
module.exports.update_assetCategory = async (req, res) => {
  try {
    const { asst_id } = req.params;
    const { asst_name, asst_desc } = req.body;

    // Check if the category exists or not
    const isExist = await Category.findByPk(asst_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset Category Not Found!" },
      });
    }

    // Check if the new name already exists
    const isNameAlreadyExist = await Category.findOne({
      where: { asst_name, asst_id: { [Op.ne]: asst_id } },
    });

    if (isNameAlreadyExist) {
      return res.status(409).send({
        code: 409,
        status: "Asset Category Name Already Existed!",
      });
    }

    // Update if updated category name not already exists
    const updatedCategory = await Category.update(
      { asst_name, asst_desc },
      { where: { asst_id }, returning: true, plain: true }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedCategory
          ? "Asset Category Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Asset Category Status ================== //
module.exports.update_categoryStatus = async (req, res) => {
  try {
    const { asst_id } = req.params;

    // Check if the category exists or not
    const isExist = await Category.findByPk(asst_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset Category Not Found!" },
      });
    }

    // Update if updated category name not already exists
    const updatedCategory = await Category.update(
      { status: !isExist.status },
      { where: { asst_id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedCategory
          ? "Asset Category Status Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get Asset Category By Id ================== //
/**
 * @swagger
 * /:
 *  get:
 *    summary: Get Asset Category By Id
 *    description: This API is used to create the Asset Category.
 *    responses:
 *        201:
 *            description: Asset Category Created Successfully!.
 */
module.exports.get_category_byId = async (req, res) => {
  try {
    const { asst_id } = req.params;

    // Check if the category exists or not
    const isExist = await Category.findByPk(asst_id, {
      attributes: ["asst_id", "asst_name", "asst_desc", "isDeleted"],
    });

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset Category Not Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "Get Asset Category Successfully",
      data: isExist,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All Asset Category ================== //
module.exports.getAll_category = async (req, res) => {
  try {
    // Check if the category exists or not
    const getAllData = await Category.findAll({
      attributes: ["asst_id", "asst_name", "asst_desc", "isDeleted"],
      where: { isDeleted: false },
    });

    if (!getAllData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No Asset Category Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All Asset Category List",
      data: getAllData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All Active Asset Category ================== //
module.exports.getAllActive_category = async (req, res) => {
  try {
    // Check if the category exists or not
    const getAllActiveData = await Category.findAll({
      attributes: ["asst_id", "asst_name", "asst_desc", "isDeleted"],
      where: { isDeleted: false, status: true },
    });

    if (!getAllActiveData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No Asset Category Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All Active Asset Category List",
      data: getAllActiveData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Delete Asset Category ================== //
module.exports.delete_category = async (req, res) => {
  try {
    const { asst_id } = req.params;

    // Check if the category exists or not
    const isExist = await Category.findByPk(asst_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset Category Not Found!" },
      });
    }

    // Update if updated category name not already exists
    const deleteCategory = await Category.update(
      { isDeleted: true },
      { where: { asst_id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        deleteCategory
          ? "Asset Category Deleted Successfully!"
          : "Error While Deleting"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};
