module.exports = (sequelize, Sequelize) => {
  const tbl_assetCategories = sequelize.define(
    "ASSET_CATEGORY",
    {
      asst_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      asst_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      asst_desc: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_assetCategories;
};
