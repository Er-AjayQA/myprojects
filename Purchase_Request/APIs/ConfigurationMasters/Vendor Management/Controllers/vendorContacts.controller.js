// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const VENDOR = db.tbl_Vendor_Management;
const ASSET_CATEGORY = db.tbl_assetCategories;

// ================== Create Vendor ================== //
module.exports.create_vendor = async (req, res) => {
  try {
    const {
      asst_id,
      vendor_name,
      address,
      country,
      state,
      city,
      po_no,
      contact_no,
      altContact_no,
      email,
      web,
      v_code,
      category,
      cc_account_code,
      sns,
      pgs,
      non_registered_supplier,
      supplier_for_cash,
    } = req.body;

    const isAlreadyExist = await VENDOR.findOne({
      where: { vendor_name, asst_id, isDeleted: { [Op.eq]: false } },
    });

    if (isAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Already Exist For The Selected Asset Category!",
        },
      });
    }

    const isAsstCatExist = await ASSET_CATEGORY.findByPk(asst_id);

    if (!isAsstCatExist || isAsstCatExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: {
          message: "Selected Asset Category Not Exist!",
        },
      });
    }

    const createNewVendor = await VENDOR.create({
      asst_id,
      vendor_name,
      address,
      country,
      state,
      city,
      po_no,
      contact_no,
      altContact_no,
      email,
      web,
      v_code,
      category,
      cc_account_code,
      sns,
      pgs,
      non_registered_supplier,
      supplier_for_cash,
    });
    return res.status(200).send({
      code: 200,
      status: "Vendor Basic Details Created Successfully!",
      data: { vendorDetails: createNewVendor },
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// // ================== Update Item ================== //
// module.exports.update_item = async (req, res) => {
//   try {
//     const { item_id } = req.params;
//     const {
//       item_name,
//       item_mvp,
//       bar_code,
//       manage_by,
//       item_desc,
//       thresh_stock,
//       uom_id,
//       asst_id,
//     } = req.body;

//     const isItemExist = await ITEM.findByPk(item_id);
//     if (!isItemExist && isItemExist.isDeleted) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "Item Not Found!" },
//       });
//     }

//     // Check if the new name already exists
//     const isNameAlreadyExist = await ITEM.findOne({
//       where: { item_name, asst_id, item_id: { [Op.ne]: item_id } },
//     });

//     if (isNameAlreadyExist) {
//       return res.status(409).send({
//         code: 409,
//         status: "Item Already Existed In Selected Asset Category!",
//       });
//     }

//     // Check if the UOM exists or not
//     const isUomExist = await UOM.findByPk(uom_id);
//     if (!isUomExist || isUomExist.isDeleted) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "UOM Not Found!" },
//       });
//     }

//     // Check if the Asset Category exists or not
//     const isAssetCatExist = await ASSET_CATEGORY.findByPk(asst_id);
//     if (!isAssetCatExist || isAssetCatExist.isDeleted) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "Asset Category Not Found!" },
//       });
//     }

//     // Update if updated UOM name not already exists
//     const updatedItem = await ITEM.update(
//       {
//         item_name,
//         item_mvp,
//         bar_code,
//         manage_by,
//         item_desc,
//         thresh_stock,
//         uom_id,
//         asst_id,
//       },
//       { where: { item_id }, returning: true, plain: true }
//     );
//     return res.status(201).send({
//       code: 201,
//       status: `${
//         updatedItem ? "Item Updated Successfully!" : "Error While Updating"
//       }`,
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };

// // ================== Update Item Status ================== //
// module.exports.update_itemStatus = async (req, res) => {
//   try {
//     const { item_id } = req.params;

//     // Check if the Item exists or not
//     const isItemExist = await ITEM.findByPk(item_id);

//     if (!isItemExist || isItemExist.isDeleted) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "Item Not Found!" },
//       });
//     }

//     // Update the status
//     const updatedStatus = await ITEM.update(
//       { status: !isItemExist.status },
//       { where: { item_id } }
//     );
//     return res.status(201).send({
//       code: 201,
//       status: `${
//         updatedStatus
//           ? "Item Status Updated Successfully!"
//           : "Error While Updating"
//       }`,
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };

// // ================== Get Item Details By Id ================== //
// module.exports.get_item_byId = async (req, res) => {
//   try {
//     const { item_id } = req.params;

//     // Check if the Item exists or not
//     const isItemExist = await ITEM.findByPk(item_id, {
//       attributes: [
//         "item_id",
//         "item_name",
//         "item_mvp",
//         "bar_code",
//         "manage_by",
//         "thresh_stock",
//         "item_desc",
//         "isDeleted",
//         "status",
//       ],
//       include: [
//         {
//           model: UOM,
//           attributes: ["uom_id", "uom_name", "uom_desc"],
//           through: null,
//         },
//         {
//           model: ASSET_CATEGORY,
//           attributes: ["asst_id", "asst_name", "asst_desc"],
//         },
//         {
//           model: ItemSpec,
//           attributes: ["spec_id", "spec_title", "spec_desc"],
//           where: { isDeleted: false },
//         },
//       ],
//     });

//     if (!isItemExist || isItemExist.isDeleted) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "Item Not Found!" },
//       });
//     }

//     return res.status(200).send({
//       code: 200,
//       status: "Get Item Successfully",
//       data: { itemDetail: isItemExist },
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };

// // ================== Get All Item ================== //
// module.exports.getAll_item = async (req, res) => {
//   try {
//     // Check if the Item exists or not
//     const getAllItem = await ITEM.findAll({
//       attributes: [
//         "item_id",
//         "item_name",
//         "item_mvp",
//         "bar_code",
//         "manage_by",
//         "thresh_stock",
//         "item_desc",
//         "isDeleted",
//         "status",
//       ],
//       include: [
//         {
//           model: UOM,
//           attributes: ["uom_id", "uom_name", "uom_desc"],
//           through: null,
//         },
//         {
//           model: ASSET_CATEGORY,
//           attributes: ["asst_id", "asst_name", "asst_desc"],
//         },
//         {
//           model: ItemSpec,
//           attributes: ["spec_id", "spec_title", "spec_desc"],
//           where: { isDeleted: false },
//         },
//       ],
//       where: { isDeleted: false },
//     });

//     if (!getAllItem) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "No Items Found!" },
//       });
//     }

//     return res.status(201).send({
//       code: 201,
//       status: "All Items List",
//       data: { itemList: getAllItem },
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };

// // ================== Get All Active Item ================== //
// module.exports.getAllActive_item = async (req, res) => {
//   try {
//     // Check if the Items exists or not
//     const getAllActiveItems = await ITEM.findAll({
//       attributes: [
//         "item_id",
//         "item_name",
//         "item_mvp",
//         "bar_code",
//         "manage_by",
//         "thresh_stock",
//         "item_desc",
//         "isDeleted",
//         "status",
//       ],
//       include: [
//         {
//           model: UOM,
//           attributes: ["uom_id", "uom_name", "uom_desc"],
//           through: null,
//         },
//         {
//           model: ASSET_CATEGORY,
//           attributes: ["asst_id", "asst_name", "asst_desc"],
//         },
//         {
//           model: ItemSpec,
//           attributes: ["spec_id", "spec_title", "spec_desc"],
//           where: { isDeleted: false },
//         },
//       ],
//       where: { isDeleted: false, status: true },
//     });

//     if (!getAllActiveItems) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "No Active Items Found!" },
//       });
//     }

//     return res.status(201).send({
//       code: 201,
//       status: "All Active Items List",
//       data: { activeItemsList: getAllActiveItems },
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };

// // ================== Delete Item ================== //
// module.exports.delete_item = async (req, res) => {
//   try {
//     const { item_id } = req.params;

//     // Check if the Item exists or not
//     const isItemExist = await ITEM.findByPk(item_id);

//     if (!isItemExist || isItemExist.isDeleted) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "Item Not Found!" },
//       });
//     }

//     // Delete the Item if exist
//     const deleteItem = await ITEM.update(
//       { isDeleted: true },
//       { where: { item_id } }
//     );
//     return res.status(201).send({
//       code: 201,
//       status: `${
//         deleteItem ? "Item Deleted Successfully!" : "Error While Deleting"
//       }`,
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };
