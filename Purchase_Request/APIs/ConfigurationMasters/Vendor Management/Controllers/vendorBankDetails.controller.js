// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const VENDOR = db.tbl_Vendor_Management;
const Vendor_Bank = db.tbl_Vendor_Bank;
const BANK = db.tbl_bankMaster;

// ================== Create Vendor Bank Details ================== //
module.exports.create_vendorBankDetails = async (req, res) => {
  try {
    const { vendor_id } = req.params;
    const {
      bank_id,
      acc_holder_name,
      branch,
      address,
      country,
      state,
      city,
      acc_no,
      unique_no,
      swift_code,
      iban_code,
    } = req.body;

    // Check Vendor Exist
    const isVendorExist = await VENDOR.findOne({
      where: {
        id: vendor_id,
        isDeleted: { [Op.eq]: false },
      },
    });
    if (!isVendorExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Not Found!",
        },
      });
    }

    // Check Bank Exist
    const isBankExist = await BANK.findOne({
      where: {
        id: bank_id,
        isDeleted: { [Op.eq]: false },
      },
    });
    if (!isBankExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Bank Not Found!",
        },
      });
    }

    // Check if Vendor Bank Details already Exist
    const isAlreadyExist = await Vendor_Bank.findOne({
      where: { vendor_id, acc_no: acc_no, isDeleted: { [Op.eq]: false } },
    });
    if (isAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Bank Already Exist",
        },
      });
    }

    // Create New Vendor Bank details
    const createVendorBank = await Vendor_Bank.create({
      vendor_id,
      bank_id,
      acc_holder_name,
      branch,
      address,
      country,
      state,
      city,
      acc_no,
      unique_no,
      swift_code,
      iban_code,
    });
    return res.status(200).send({
      code: 200,
      status: "Vendor Bank Details Created Successfully!",
      data: { vendorBankDetails: createVendorBank },
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Vendor Bank Details ================== //
module.exports.update_vendorBankDetails = async (req, res) => {
  try {
    const { vendor_id, id } = req.params;
    const {
      bank_id,
      acc_holder_name,
      branch,
      address,
      country,
      state,
      city,
      acc_no,
      unique_no,
      swift_code,
      iban_code,
    } = req.body;

    // Check if Vendor Exist
    const isVendorExist = await VENDOR.findOne({
      where: {
        id: vendor_id,
        isDeleted: { [Op.eq]: false },
      },
    });
    if (!isVendorExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Not Found!",
        },
      });
    }

    // Check if Vendor Bank Exist
    const isVendorBankExist = await Vendor_Bank.findOne({
      where: { vendor_id, id, isDeleted: false },
    });
    if (!isVendorBankExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Bank Not Found!",
        },
      });
    }

    // Check Bank Exist
    const isBankExist = await BANK.findOne({
      where: {
        id: bank_id,
        isDeleted: { [Op.eq]: false },
      },
    });
    if (!isBankExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Bank Not Found!",
        },
      });
    }

    // Check if Vendor Bank Account No. already Exist
    const isAccNoExist = await Vendor_Bank.findOne({
      where: {
        vendor_id,
        acc_no,
        isDeleted: { [Op.eq]: false },
        id: { [Op.ne]: id },
      },
    });
    if (isAccNoExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Bank Account number Already Exist",
        },
      });
    }

    // Update New Vendor Bank details
    const updateVendorBank = await Vendor_Bank.update(
      {
        vendor_id,
        bank_id,
        acc_holder_name,
        branch,
        address,
        country,
        state,
        city,
        acc_no,
        unique_no,
        swift_code,
        iban_code,
      },
      { where: { id } }
    );
    return res.status(200).send({
      code: 200,
      status: "Vendor Bank Details Updated Successfully!",
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Delete Item ================== //
module.exports.delete_vendorBankDetails = async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the Vendor exists or not
    const isBankDetailsExist = await Vendor_Bank.findByPk(id);

    if (!isBankDetailsExist || isBankDetailsExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Bank Detail Not Found!" },
      });
    }

    // Delete the Bank details if exist
    const deleteVendor = await Vendor_Bank.update(
      { isDeleted: true },
      { where: { id: isBankDetailsExist.id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        deleteVendor
          ? "Bank Detail Deleted Successfully!"
          : "Error While Deleting"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};
