// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const VENDOR = db.tbl_Vendor_Management;
const ASSET_CATEGORY = db.tbl_assetCategories;

// ================== Create Vendor ================== //
module.exports.create_vendor = async (req, res) => {
  try {
    const {
      asst_id,
      vendor_name,
      address,
      country,
      state,
      city,
      po_no,
      contact_no,
      altContact_no,
      email,
      web,
      v_code,
      category,
      cc_account_code,
      sns,
      pgs,
      non_registered_supplier,
      supplier_for_cash,
    } = req.body;

    const isAlreadyExist = await VENDOR.findOne({
      where: { vendor_name, asst_id, isDeleted: { [Op.eq]: false } },
    });

    if (isAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Already Exist For The Selected Asset Category!",
        },
      });
    }

    const isVendorCodeExist = await VENDOR.findOne({
      where: { v_code, isDeleted: { [Op.eq]: false } },
    });

    if (isVendorCodeExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Code Already Existed!",
        },
      });
    }

    const isAsstCatExist = await ASSET_CATEGORY.findByPk(asst_id);

    if (!isAsstCatExist || isAsstCatExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: {
          message: "Selected Asset Category Not Exist!",
        },
      });
    }

    const createNewVendor = await VENDOR.create({
      asst_id,
      vendor_name,
      address,
      country,
      state,
      city,
      po_no,
      contact_no,
      altContact_no,
      email,
      web,
      v_code,
      category,
      cc_account_code,
      sns,
      pgs,
      non_registered_supplier,
      supplier_for_cash,
    });
    return res.status(200).send({
      code: 200,
      status: "Vendor Basic Details Created Successfully!",
      data: createNewVendor,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Vendor ================== //
module.exports.update_vendor = async (req, res) => {
  try {
    const { id } = req.params;
    const {
      asst_id,
      vendor_name,
      address,
      country,
      state,
      city,
      po_no,
      contact_no,
      altContact_no,
      email,
      web,
      v_code,
      category,
      cc_account_code,
      sns,
      pgs,
      non_registered_supplier,
      supplier_for_cash,
    } = req.body;

    const isVendorExist = await VENDOR.findByPk(id);
    if (!isVendorExist || isVendorExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Vendor Not Found!" },
      });
    }

    // Check if the Asset Category exists or not
    const isAssetCatExist = await ASSET_CATEGORY.findByPk(asst_id);
    if (!isAssetCatExist || isAssetCatExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Selected Asset Category Not Found!" },
      });
    }

    // Check if the new name already exists
    const isNameAlreadyExist = await VENDOR.findOne({
      where: { vendor_name, asst_id, id: { [Op.ne]: id } },
    });

    if (isNameAlreadyExist) {
      return res.status(409).send({
        code: 409,
        status: "Vendor Already Existed In Selected Asset Category!",
      });
    }

    const isVendorCodeExist = await VENDOR.findOne({
      where: { v_code, isDeleted: false, id: { [Op.ne]: id } },
    });

    if (isVendorCodeExist) {
      return res.status(409).send({
        code: 409,
        data: {
          message: "Vendor Code Already Existed!",
        },
      });
    }

    // Update if updated Vendor name not already exists
    const updatedVendor = await VENDOR.update(
      {
        asst_id,
        vendor_name,
        address,
        country,
        state,
        city,
        po_no,
        contact_no,
        altContact_no,
        email,
        web,
        v_code,
        category,
        cc_account_code,
        sns,
        pgs,
        non_registered_supplier,
        supplier_for_cash,
      },
      { where: { id }, returning: true, plain: true }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedVendor ? "Vendor Updated Successfully!" : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Vendor Status ================== //
module.exports.update_VendorStatus = async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the Vendor exists or not
    const isVendorExist = await VENDOR.findByPk(id);

    if (!isVendorExist || isVendorExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Vendor Not Found!" },
      });
    }

    // Update the status
    const updatedStatus = await VENDOR.update(
      { status: !isVendorExist.status },
      { where: { id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedStatus
          ? "Vendor Status Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get Vendor Details By Id ================== //
module.exports.get_vendor_byId = async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the Vendor exists or not
    const isVendorExist = await VENDOR.findOne(
      { id },
      {
        attributes: [
          "vendor_name",
          "address",
          "country",
          "state",
          "city",
          "po_no",
          "contact_no",
          "altContact_no",
          "email",
          "web",
          "v_code",
          "category",
          "cc_account_code",
          "sns",
          "pgs",
          "non_registered_supplier",
          "supplier_for_cash",
        ],
        where: { isDeleted: false },
        include: [
          {
            model: db.tbl_Vendor_Bank,
            attributes: [
              "branch",
              "acc_holder_name",
              "address",
              "country",
              "state",
              "city",
              "acc_no",
              "unique_no",
              "swift_code",
              "iban_code",
            ],
            where: { isDeleted: false },
            // through: null,
          },
          {
            model: ASSET_CATEGORY,
            attributes: ["asst_id", "asst_name", "asst_desc"],
            where: { isDeleted: false },
            // through: null,
          },
          {
            model: db.tbl_Vendor_Contact,
            attributes: [
              "contact_person_name",
              "email",
              "designation",
              "department",
            ],
            where: { isDeleted: false },
            // through: null,
          },
          {
            model: db.tbl_Vendor_Documents,
            attributes: [
              "doc_type",
              "doc_name",
              "expiry_date",
              "is_prior_notification",
            ],
            where: { isDeleted: false },
            // through: null,
          },
        ],
      }
    );

    if (!isVendorExist || isVendorExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Vendor Not Found!" },
      });
    }

    return res.status(200).send({
      code: 200,
      status: "Get Vendor Successfully",
      data: isVendorExist,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// // ================== Get All Item ================== //
// module.exports.getAll_item = async (req, res) => {
//   try {
//     // Check if the Item exists or not
//     const getAllItem = await ITEM.findAll({
//       attributes: [
//         "item_id",
//         "item_name",
//         "item_mvp",
//         "bar_code",
//         "manage_by",
//         "thresh_stock",
//         "item_desc",
//         "isDeleted",
//         "status",
//       ],
//       include: [
//         {
//           model: UOM,
//           attributes: ["uom_id", "uom_name", "uom_desc"],
//           through: null,
//         },
//         {
//           model: ASSET_CATEGORY,
//           attributes: ["asst_id", "asst_name", "asst_desc"],
//         },
//         {
//           model: ItemSpec,
//           attributes: ["spec_id", "spec_title", "spec_desc"],
//           where: { isDeleted: false },
//         },
//       ],
//       where: { isDeleted: false },
//     });

//     if (!getAllItem) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "No Items Found!" },
//       });
//     }

//     return res.status(201).send({
//       code: 201,
//       status: "All Items List",
//       data: { itemList: getAllItem },
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };

// // ================== Get All Active Item ================== //
// module.exports.getAllActive_item = async (req, res) => {
//   try {
//     // Check if the Items exists or not
//     const getAllActiveItems = await ITEM.findAll({
//       attributes: [
//         "item_id",
//         "item_name",
//         "item_mvp",
//         "bar_code",
//         "manage_by",
//         "thresh_stock",
//         "item_desc",
//         "isDeleted",
//         "status",
//       ],
//       include: [
//         {
//           model: UOM,
//           attributes: ["uom_id", "uom_name", "uom_desc"],
//           through: null,
//         },
//         {
//           model: ASSET_CATEGORY,
//           attributes: ["asst_id", "asst_name", "asst_desc"],
//         },
//         {
//           model: ItemSpec,
//           attributes: ["spec_id", "spec_title", "spec_desc"],
//           where: { isDeleted: false },
//         },
//       ],
//       where: { isDeleted: false, status: true },
//     });

//     if (!getAllActiveItems) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "No Active Items Found!" },
//       });
//     }

//     return res.status(201).send({
//       code: 201,
//       status: "All Active Items List",
//       data: { activeItemsList: getAllActiveItems },
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };

// // ================== Delete Item ================== //
// module.exports.delete_item = async (req, res) => {
//   try {
//     const { item_id } = req.params;

//     // Check if the Item exists or not
//     const isItemExist = await ITEM.findByPk(item_id);

//     if (!isItemExist || isItemExist.isDeleted) {
//       return res.status(404).send({
//         code: 404,
//         data: { message: "Item Not Found!" },
//       });
//     }

//     // Delete the Item if exist
//     const deleteItem = await ITEM.update(
//       { isDeleted: true },
//       { where: { item_id } }
//     );
//     return res.status(201).send({
//       code: 201,
//       status: `${
//         deleteItem ? "Item Deleted Successfully!" : "Error While Deleting"
//       }`,
//     });
//   } catch (error) {
//     return res.status(500).send({ code: 500, message: error.message });
//   }
// };
