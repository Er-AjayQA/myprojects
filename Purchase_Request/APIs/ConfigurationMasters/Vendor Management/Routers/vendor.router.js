// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const vendorController = require("../Controllers/vendor.controller");

// ========== ROUTES ========== //
router.post("/create_vendor", vendorController.create_vendor);
router.put("/update_vendor/:id", vendorController.update_vendor);
router.put("/update_VendorStatus/:id", vendorController.update_VendorStatus);
router.get("/get_vendor_byId/:id", vendorController.get_vendor_byId);
// router.get("/getAll_item", vendorController.getAll_item);
// router.get("/getAllActive_item", vendorController.getAllActive_item);
// router.delete("/delete_item/:item_id", vendorController.delete_item);

module.exports = router;
