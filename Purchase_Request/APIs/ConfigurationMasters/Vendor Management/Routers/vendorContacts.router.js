// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const vendorController = require("../Controllers/vendor.controller");

// ========== ROUTES ========== //
router.post("/create_vendor", vendorController.create_vendor);
// router.put("/update_item/:item_id", vendorController.update_item);
// router.put("/update_itemStatus/:item_id", vendorController.update_itemStatus);
// router.get("/get_item_byId/:item_id", vendorController.get_item_byId);
// router.get("/getAll_item", vendorController.getAll_item);
// router.get("/getAllActive_item", vendorController.getAllActive_item);
// router.delete("/delete_item/:item_id", vendorController.delete_item);

module.exports = router;
