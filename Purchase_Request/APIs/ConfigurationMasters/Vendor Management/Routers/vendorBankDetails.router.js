// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const vendorBankController = require("../Controllers/vendorBankDetails.controller");

// ========== ROUTES ========== //
router.post(
  "/create_vendorBankDetails/:vendor_id",
  vendorBankController.create_vendorBankDetails
);
router.put(
  "/update_vendorBankDetails/:vendor_id/:id",
  vendorBankController.update_vendorBankDetails
);
// router.put(
//   "/update_vendorBankStatus/:vendor_id/:id",
//   vendorBankController.update_vendorBankStatus
// );
// router.get("/get_item_byId/:item_id", vendorBankController.get_item_byId);
// router.get("/getAll_item", vendorBankController.getAll_item);
// router.get("/getAllActive_item", vendorBankController.getAllActive_item);
router.delete(
  "/delete_vendorBank/:id",
  vendorBankController.delete_vendorBankDetails
);

module.exports = router;
