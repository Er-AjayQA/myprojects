module.exports = (sequelize, Sequelize) => {
  const tbl_Vendor_Bank = sequelize.define(
    "VENDOR_BANK",
    {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      branch: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      acc_holder_name: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      country: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      acc_no: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      unique_no: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      swift_code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      iban_code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_Vendor_Bank;
};
