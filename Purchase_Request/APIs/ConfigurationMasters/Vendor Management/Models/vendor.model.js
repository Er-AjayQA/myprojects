module.exports = (sequelize, Sequelize) => {
  const tbl_Vendor_Management = sequelize.define(
    "VENDOR_MANAGEMENT",
    {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      vendor_name: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      country: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      po_no: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      contact_no: {
        type: Sequelize.BIGINT,
        allowNull: false,
        require: true,
      },
      altContact_no: {
        type: Sequelize.BIGINT,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      web: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      v_code: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      category: {
        type: Sequelize.ENUM("International", "Domestic"),
        defaultValue: "Domestic",
        allowNull: false,
        require: true,
      },
      cc_account_code: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      sns: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      pgs: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      non_registered_supplier: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      supplier_for_cash: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_Vendor_Management;
};
