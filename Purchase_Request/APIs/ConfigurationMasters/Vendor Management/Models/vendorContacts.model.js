module.exports = (sequelize, Sequelize) => {
  const tbl_Vendor_Contact = sequelize.define(
    "VENDOR_CONTACT",
    {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      contact_person_name: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      designation: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      department: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_Vendor_Contact;
};
