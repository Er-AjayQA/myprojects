module.exports = (sequelize, Sequelize) => {
  const tbl_Vendor_Documents = sequelize.define(
    "VENDOR_DOCUMENT",
    {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      doc_type: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      doc_name: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      expiry_date: {
        type: Sequelize.STRING,
        allowNull: false,
        require: true,
      },
      is_prior_notification: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_Vendor_Documents;
};
