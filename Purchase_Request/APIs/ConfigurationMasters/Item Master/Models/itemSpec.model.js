module.exports = (sequelize, Sequelize) => {
  const tbl_Item_Specifications = sequelize.define(
    "ITEM_SPECIFICATION",
    {
      spec_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      spec_title: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      spec_desc: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    },
    { freezeTableName: true }
  );
  return tbl_Item_Specifications;
};
