module.exports = (sequelize, Sequelize) => {
  const tbl_Item_Master = sequelize.define(
    "ITEM_MASTER",
    {
      item_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      item_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      item_mvp: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      bar_code: {
        type: Sequelize.ENUM("Yes", "No"),
        defaultValue: "No",
      },
      manage_by: {
        type: Sequelize.ENUM("Batch Only", "S.No. Only"),
      },
      item_desc: {
        type: Sequelize.STRING,
      },
      thresh_stock: {
        type: Sequelize.INTEGER,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_Item_Master;
};
