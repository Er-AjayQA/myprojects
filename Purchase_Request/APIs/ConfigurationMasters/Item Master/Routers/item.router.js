// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const ItemController = require("../Controllers/item.controller");

// ========== ROUTES ========== //
router.post("/create_item", ItemController.create_item);
router.put("/update_item/:item_id", ItemController.update_item);
router.put("/update_itemStatus/:item_id", ItemController.update_itemStatus);
router.get("/get_item_byId/:item_id", ItemController.get_item_byId);
router.get("/getAll_item", ItemController.getAll_item);
router.get("/getAllActive_item", ItemController.getAllActive_item);
router.delete("/delete_item/:item_id", ItemController.delete_item);

module.exports = router;
