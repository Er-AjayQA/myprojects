// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const itemSpecController = require("../../Item Master/Controllers/itemSpec.controller");

// ========== ROUTES ========== //
router.post("/create_itemSpec", itemSpecController.create_itemSpec);
router.put(
  "/update_itemSpec/:item_id/:spec_id",
  itemSpecController.update_itemSpec
);
router.delete(
  "/delete_itemSpec/:item_id/:spec_id",
  itemSpecController.delete_itemSpec
);

module.exports = router;
