// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const ITEM = db.tbl_Item_Master;
const ItemSpec = db.tbl_Item_Specifications;

// ================== Create Item Specification ================== //
module.exports.create_itemSpec = async (req, res) => {
  try {
    const { spec_title, spec_desc, item_id } = req.body;

    const isSpecAlreadyExist = await ItemSpec.findOne({
      where: { spec_title, spec_desc, item_id, isDeleted: { [Op.eq]: false } },
    });

    if (isSpecAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: { message: "Item Specification Already Existed!" },
      });
    }

    const isItemExist = await ITEM.findByPk(item_id);
    if (!isItemExist || isItemExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Item Not Found!" },
      });
    }

    const createItemSpec = await ItemSpec.create({
      spec_title,
      spec_desc,
      item_id,
    });
    return res.status(200).send({
      code: 200,
      status: "Item Specification Created Successfully!",
      data: createItemSpec,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Item Specification ================== //
module.exports.update_itemSpec = async (req, res) => {
  try {
    const { spec_id, item_id } = req.params;
    const { spec_title, spec_desc } = req.body;

    // Check if the Item Specification exists or not
    const isExist = await ItemSpec.findByPk(spec_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Item Specification Not Found!" },
      });
    }

    // Check if the new name already exists
    const isNameAlreadyExist = await ItemSpec.findOne({
      where: { spec_title, spec_desc, item_id, isDeleted: false },
    });

    if (isNameAlreadyExist) {
      return res.status(409).send({
        code: 409,
        status: "Specification Already Existed!",
      });
    }

    // Update if updated Specification name not already exists
    const updatedItemSpecification = await ItemSpec.update(
      { spec_title, spec_desc, item_id },
      { where: { spec_id }, returning: true, plain: true }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedItemSpecification
          ? "Specification Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Delete Item Specification ================== //
module.exports.delete_itemSpec = async (req, res) => {
  try {
    const { item_id, spec_id } = req.params;

    // Check if the Item exists or not
    const isItemExist = await ITEM.findByPk(item_id);
    if (!isItemExist || isItemExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Item Not Found!" },
      });
    }

    // Check if the Item specification exists or not
    const isItemSpecExist = await ItemSpec.findOne({
      where: { spec_id, item_id },
    });
    if (!isItemSpecExist || isItemSpecExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Item Specification Not Found!" },
      });
    }

    // Delete the Item if exist
    const deleteItemSpec = await ItemSpec.update(
      { isDeleted: true },
      { where: { spec_id } }
    );
    return res.status(200).send({
      code: 200,
      status: `${
        deleteItemSpec
          ? "Specification Deleted Successfully!"
          : "Error While Deleting"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};
