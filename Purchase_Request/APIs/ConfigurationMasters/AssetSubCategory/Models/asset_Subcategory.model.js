module.exports = (sequelize, Sequelize) => {
  const tbl_assetSubCategories = sequelize.define(
    "ASSET_SUBCATEGORY",
    {
      subCat_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      subCat_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      subCat_desc: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_assetSubCategories;
};
