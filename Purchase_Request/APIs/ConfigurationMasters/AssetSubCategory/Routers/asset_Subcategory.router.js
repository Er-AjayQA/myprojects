// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const subCategoryController = require("../Controllers/asset_SubCategory.controller");

// ========== ROUTES ========== //
router.post(
  "/create_assetSubCategory",
  subCategoryController.create_assetSubCategory
);
router.put(
  "/update_assetSubCategory/:subCat_id",
  subCategoryController.update_assetSubCategory
);
router.put(
  "/update_subcategoryStatus/:subCat_id",
  subCategoryController.update_subcategoryStatus
);
router.get(
  "/get_subcategory_byId/:subCat_id",
  subCategoryController.get_subcategory_byId
);
router.get("/getAll_subcategory", subCategoryController.getAll_subcategory);
router.get(
  "/getAllActive_subcategory",
  subCategoryController.getAllActive_subcategory
);
router.delete(
  "/delete_subcategory/:subCat_id",
  subCategoryController.delete_subcategory
);

module.exports = router;
