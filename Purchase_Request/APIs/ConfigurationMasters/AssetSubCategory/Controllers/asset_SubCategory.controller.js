// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const SubCategory = db.tbl_assetSubCategories;
const Category = db.tbl_assetCategories;

// ================== Create Asset SubCategory ================== //
module.exports.create_assetSubCategory = async (req, res) => {
  try {
    const { asst_id, subCat_name, subCat_desc } = req.body;

    const isAlreadyExist = await SubCategory.findOne({
      where: { subCat_name, asst_id, isDeleted: false },
    });

    if (isAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: { message: "Asset SubCategory Already Existed!" },
      });
    }

    const isAssetExist = await Category.findByPk(asst_id, {
      where: { isDeleted: false },
    });

    if (!isAssetExist) {
      return res.status(404).send({
        code: 404,
        data: { message: "Selected Asset Category Not Existed!" },
      });
    }

    const createSubCategory = await SubCategory.create({
      asst_id,
      subCat_name,
      subCat_desc,
    });
    return res.status(200).send({
      code: 200,
      status: "Asset SubCategory Created Successfully!",
      data: createSubCategory,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Asset SubCategory ================== //
module.exports.update_assetSubCategory = async (req, res) => {
  try {
    const { subCat_id } = req.params;
    const { asst_id, subCat_name, subCat_desc } = req.body;

    // Check if the subcategory exists or not
    const isExist = await SubCategory.findByPk(subCat_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset SubCategory Not Found!" },
      });
    }

    // Check if the category exists or not
    const isAssetExist = await Category.findByPk(asst_id);

    if (!isAssetExist || isAssetExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset Category Not Found!" },
      });
    }

    // Check if the new name already exists
    const isNameAlreadyExist = await SubCategory.findOne({
      where: { subCat_name, asst_id, subCat_id: { [Op.ne]: subCat_id } },
    });

    if (isNameAlreadyExist) {
      return res.status(409).send({
        code: 409,
        status:
          "Asset SubCategory Name Already Existed in Selected Asset Category!",
      });
    }

    // Update if updated category name not already exists
    const updatedSubCategory = await SubCategory.update(
      { asst_id, subCat_name, subCat_desc },
      { where: { subCat_id }, returning: true, plain: true }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedSubCategory
          ? "Asset SubCategory Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update Asset SubCategory Status ================== //
module.exports.update_subcategoryStatus = async (req, res) => {
  try {
    const { subCat_id } = req.params;

    // Check if the subcategory exists or not
    const isExist = await SubCategory.findByPk(subCat_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset SubCategory Not Found!" },
      });
    }

    // Update the status
    const updatedStatus = await SubCategory.update(
      { status: !isExist.status },
      { where: { subCat_id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedStatus
          ? "Asset SubCategory Status Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get Asset SubCategory By Id ================== //
module.exports.get_subcategory_byId = async (req, res) => {
  try {
    const { subCat_id } = req.params;

    // const query = `SELECT  sc.subCat_name,sc.subCat_desc,sc.isDeleted, sc.status, c.asst_name,c.asst_desc
    //   FROM tbl_assetsubcategories  as sc
    //   INNER JOIN tbl_assetcategories as c on c.asst_id=sc.asst_id
    //   WHERE sc.subCat_id=${subCat_id}`;

    // const isExist = await db.sequelize.query(query, {
    //   type: db.sequelize.QueryTypes.SELECT,
    // });

    // Check if the subcategory exists or not
    const isExist = await SubCategory.findByPk(subCat_id, {
      attributes: [
        "subCat_id",
        "asst_id",
        "subCat_name",
        "subCat_desc",
        "isDeleted",
        "status",
      ],
      include: [
        {
          model: Category,
          attributes: ["asst_name", "asst_desc"],
        },
      ],
    });

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset SubCategory Not Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "Get Asset SubCategory Successfully",
      data: isExist,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All Asset SubCategory ================== //
module.exports.getAll_subcategory = async (req, res) => {
  try {
    // Check if the category exists or not
    const getAllData = await SubCategory.findAll({
      attributes: ["subCat_name", "subCat_desc", "status", "isDeleted"],
      where: { isDeleted: false },
      include: [
        {
          model: Category,
          attributes: ["asst_id", "asst_name"],
        },
      ],
    });

    if (!getAllData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No Asset SubCategory Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All Asset SubCategory List",
      data: getAllData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All Active Asset SubCategory ================== //
module.exports.getAllActive_subcategory = async (req, res) => {
  try {
    // Check if the subcategory exists or not
    const getAllActiveData = await SubCategory.findAll({
      attributes: ["subCat_name", "subCat_desc", "status", "isDeleted"],
      where: { isDeleted: false, status: true },
      include: [
        {
          model: Category,
          attributes: ["asst_id", "asst_name", "asst_desc"],
        },
      ],
    });

    if (!getAllActiveData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No Active Asset SubCategories Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All Active Asset SubCategories List",
      data: getAllActiveData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Delete Asset Category ================== //
module.exports.delete_subcategory = async (req, res) => {
  try {
    const { subCat_id } = req.params;

    // Check if the subcategory exists or not
    const isExist = await SubCategory.findByPk(subCat_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "Asset SubCategory Not Found!" },
      });
    }

    // Delete the subcategory if exist
    const deleteSubCategory = await SubCategory.update(
      { isDeleted: true },
      { where: { subCat_id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        deleteSubCategory
          ? "Asset SubCategory Deleted Successfully!"
          : "Error While Deleting"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};
