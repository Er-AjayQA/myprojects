// ========== IMPORTS and CONFIGS ========== //
const { Op } = require("sequelize");
const db = require("../../../../indexFiles/modelsIndex");
const UOM = db.tbl_assetUOM;

// ================== Create UOM ================== //
module.exports.create_uom = async (req, res) => {
  try {
    const { uom_name, uom_desc } = req.body;

    const isAlreadyExist = await UOM.findOne({
      where: { uom_name, isDeleted: { [Op.eq]: false } },
    });

    if (isAlreadyExist) {
      return res.status(409).send({
        code: 409,
        data: { message: "UOM Already Existed!" },
      });
    }

    const createUOM = await UOM.create({
      uom_name,
      uom_desc,
    });
    return res.status(200).send({
      code: 200,
      status: "UOM Created Successfully!",
      data: createUOM,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update UOM ================== //
module.exports.update_uom = async (req, res) => {
  try {
    const { uom_id } = req.params;
    const { uom_name, uom_desc } = req.body;

    // Check if the UOM exists or not
    const isExist = await UOM.findByPk(uom_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "UOM Not Found!" },
      });
    }

    // Check if the new name already exists
    const isNameAlreadyExist = await UOM.findOne({
      where: { uom_name, uom_id: { [Op.ne]: uom_id } },
    });

    if (isNameAlreadyExist) {
      return res.status(409).send({
        code: 409,
        status: "UOM Already Existed!",
      });
    }

    // Update if updated UOM name not already exists
    const updatedUOM = await UOM.update(
      { uom_name, uom_desc },
      { where: { uom_id }, returning: true, plain: true }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedUOM ? "UOM Updated Successfully!" : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Update UOM Status ================== //
module.exports.update_uomStatus = async (req, res) => {
  try {
    const { uom_id } = req.params;

    // Check if the UOM exists or not
    const isExist = await UOM.findByPk(uom_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "UOM Not Found!" },
      });
    }

    // Update the status
    const updatedStatus = await UOM.update(
      { status: !isExist.status },
      { where: { uom_id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        updatedStatus
          ? "UOM Status Updated Successfully!"
          : "Error While Updating"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get UOM By Id ================== //
module.exports.get_uom_byId = async (req, res) => {
  try {
    const { uom_id } = req.params;

    // Check if the subcategory exists or not
    const isExist = await UOM.findByPk(uom_id, {
      attributes: ["uom_id", "uom_name", "uom_desc", "isDeleted", "status"],
    });

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "UOM Not Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "Get UOM Successfully",
      data: isExist,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All UOM ================== //
module.exports.getAll_uom = async (req, res) => {
  try {
    // Check if the category exists or not
    const getAllData = await UOM.findAll({
      attributes: ["uom_name", "uom_desc", "status", "isDeleted"],
      where: { isDeleted: false },
    });

    if (!getAllData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No UOM Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All UOM List",
      data: getAllData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Get All Active UOM ================== //
module.exports.getAllActive_uom = async (req, res) => {
  try {
    // Check if the UOM exists or not
    const getAllActiveData = await UOM.findAll({
      attributes: ["uom_name", "uom_desc", "status", "isDeleted"],
      where: { isDeleted: false, status: true },
    });

    if (!getAllActiveData) {
      return res.status(404).send({
        code: 404,
        data: { message: "No Active UOM Found!" },
      });
    }

    return res.status(201).send({
      code: 201,
      status: "All Active UOM List",
      data: getAllActiveData,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};

// ================== Delete UOM ================== //
module.exports.delete_uom = async (req, res) => {
  try {
    const { uom_id } = req.params;

    // Check if the UOM exists or not
    const isExist = await UOM.findByPk(uom_id);

    if (!isExist || isExist.isDeleted) {
      return res.status(404).send({
        code: 404,
        data: { message: "UOM Not Found!" },
      });
    }

    // Delete the UOM if exist
    const deleteUOM = await UOM.update(
      { isDeleted: true },
      { where: { uom_id } }
    );
    return res.status(201).send({
      code: 201,
      status: `${
        deleteUOM ? "UOM Deleted Successfully!" : "Error While Deleting"
      }`,
    });
  } catch (error) {
    return res.status(500).send({ code: 500, message: error.message });
  }
};
