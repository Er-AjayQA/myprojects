// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const router = express.Router();
const uomController = require("../Controllers/uom.controller");

// ========== ROUTES ========== //
router.post("/create_uom", uomController.create_uom);
router.put("/update_uom/:uom_id", uomController.update_uom);
router.put("/update_uomStatus/:uom_id", uomController.update_uomStatus);
router.get("/get_uom_byId/:uom_id", uomController.get_uom_byId);
router.get("/getAll_uom", uomController.getAll_uom);
router.get("/getAllActive_uom", uomController.getAllActive_uom);
router.delete("/delete_uom/:uom_id", uomController.delete_uom);

module.exports = router;
