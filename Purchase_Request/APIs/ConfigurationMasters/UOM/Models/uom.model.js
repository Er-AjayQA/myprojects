module.exports = (sequelize, Sequelize) => {
  const tbl_assetUOM = sequelize.define(
    "ASSET_UOM",
    {
      uom_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      uom_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      uom_desc: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    },
    { freezeTableName: true }
  );
  return tbl_assetUOM;
};
