// ========== IMPORTS and CONFIGS ========== //
const config = require("../configFiles/db.config");
const Sequelize = require("sequelize");

// ========== DB CONFIG ========== //
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// ========== MODEL ========== //

// Asset Category Table
db.tbl_assetCategories =
  require("../APIs/ConfigurationMasters/AssetCategory/Models/asset_category.model")(
    sequelize,
    Sequelize
  );
// Asset Sub Category Table
db.tbl_assetSubCategories =
  require("../APIs/ConfigurationMasters/AssetSubCategory/Models/asset_Subcategory.model")(
    sequelize,
    Sequelize
  );

// Relation B/W Asset Category and Asset Sub Category Tables
db.tbl_assetCategories.hasMany(db.tbl_assetSubCategories, {
  foreignKey: "asst_id",
});
db.tbl_assetSubCategories.belongsTo(db.tbl_assetCategories, {
  foreignKey: "asst_id",
});

// Asset UOM Table
db.tbl_assetUOM = require("../APIs/ConfigurationMasters/UOM/Models/uom.model")(
  sequelize,
  Sequelize
);

// Item Specifications Table
db.tbl_Item_Specifications =
  require("../APIs/ConfigurationMasters/Item Master/Models/itemSpec.model")(
    sequelize,
    Sequelize
  );

// Item Master Table
db.tbl_Item_Master =
  require("../APIs/ConfigurationMasters/Item Master/Models/item.model")(
    sequelize,
    Sequelize
  );

// Relation B/W Item Master and Asset Category Tables
db.tbl_Item_Master.belongsTo(db.tbl_assetCategories, { foreignKey: "asst_id" });
db.tbl_assetCategories.hasMany(db.tbl_Item_Master, {
  foreignKey: "asst_id",
});

// Relation B/W Item Master and UOM Tables
db.tbl_Item_Master.belongsTo(db.tbl_assetUOM, { foreignKey: "uom_id" });
db.tbl_assetUOM.hasMany(db.tbl_Item_Master, {
  foreignKey: "uom_id",
});

// Relation B/W Item Master and Item Specifications Tables
db.tbl_Item_Specifications.belongsTo(db.tbl_Item_Master, {
  foreignKey: "item_id",
});
db.tbl_Item_Master.hasMany(db.tbl_Item_Specifications, {
  foreignKey: "item_id",
});

// Bank Master Tables
db.tbl_bankMaster =
  require("../APIs/ConfigurationMasters/BankMaster/Models/bank.model")(
    sequelize,
    Sequelize
  );

// Vendor Management Tables
db.tbl_Vendor_Management =
  require("../APIs/ConfigurationMasters/Vendor Management/Models/vendor.model")(
    sequelize,
    Sequelize
  );
db.tbl_Vendor_Bank =
  require("../APIs/ConfigurationMasters/Vendor Management/Models/vendorBankDetails.model")(
    sequelize,
    Sequelize
  );
db.tbl_Vendor_Documents =
  require("../APIs/ConfigurationMasters/Vendor Management/Models/vendorDocuments.model")(
    sequelize,
    Sequelize
  );
db.tbl_Vendor_Contact =
  require("../APIs/ConfigurationMasters/Vendor Management/Models/vendorContacts.model")(
    sequelize,
    Sequelize
  );

// Relation B/W Vendor Management and Vendor Bank Tables
db.tbl_Vendor_Bank.belongsTo(db.tbl_Vendor_Management, {
  foreignKey: "vendor_id",
});
db.tbl_Vendor_Management.hasMany(db.tbl_Vendor_Bank, {
  foreignKey: "vendor_id",
});

// Relation B/W Vendor Bank and Bank Master Tables
db.tbl_Vendor_Bank.belongsTo(db.tbl_bankMaster, {
  foreignKey: "bank_id",
});
db.tbl_bankMaster.hasMany(db.tbl_Vendor_Bank, {
  foreignKey: "bank_id",
});

// Relation B/W Vendor Management and Vendor Document Tables
db.tbl_Vendor_Documents.belongsTo(db.tbl_Vendor_Management, {
  foreignKey: "vendor_id",
});
db.tbl_Vendor_Management.hasMany(db.tbl_Vendor_Documents, {
  foreignKey: "vendor_id",
});

// Relation B/W Vendor Management and Vendor Contact Tables
db.tbl_Vendor_Contact.belongsTo(db.tbl_Vendor_Management, {
  foreignKey: "vendor_id",
});
db.tbl_Vendor_Management.hasMany(db.tbl_Vendor_Contact, {
  foreignKey: "vendor_id",
});

// Relation B/W Vendor Management and Asset Category Tables
db.tbl_Vendor_Management.belongsTo(db.tbl_assetCategories, {
  foreignKey: "asst_id",
});
db.tbl_assetCategories.hasMany(db.tbl_Vendor_Management, {
  foreignKey: "asst_id",
});

module.exports = db;
