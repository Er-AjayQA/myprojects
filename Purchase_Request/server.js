// ========== IMPORTS and CONFIGS ========== //
const express = require("express");
const db = require("./indexFiles/modelsIndex");
require("dotenv").config();
const port = process.env.PORT || 3000;
const cors = require("cors");

// ========== COMMON CONFIG ========== //
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ========== Cors ========== //
app.use(
  cors({
    origin: "*", // Allow all origins
    methods: ["GET", "POST", "PUT", "DELETE", "OPTIONS"],
    allowedHeaders: ["Content-Type", "Authorization"],
  })
);

// ========== HOME ROUTES ========== //

/**
 * @swagger
 * /:
 *  get:
 *    summary: api/v1/home
 *    description: This API is used to check the connection.
 *    responses:
 *        200:
 *            description: Connection is successful.
 */
app.get("/api/v1/home", (req, res) => {
  try {
    res.status(200).send({
      code: 200,
      data: { message: "Welcome to Purchase Request" },
    });
  } catch (error) {
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
});

// ========== DB SYNC ========== //
// db.sequelize
//   .sync({ alter: true })
//   .then(() => {
//     console.log("DB Sync Successfully............");
//   })
//   .catch((err) => {
//     console.log("DB Sync Failed.............");
//   });

// ========== IMPORT ROUTERS ========== //
const assetCategoryRouter = require("./APIs/ConfigurationMasters/AssetCategory/Routers/asset_category.router");
const assetSubCategoryRouter = require("./APIs/ConfigurationMasters/AssetSubCategory/Routers/asset_Subcategory.router");
const uomRouter = require("./APIs/ConfigurationMasters/UOM/Routers/uom.router");
const itemSpecRouter = require("./APIs/ConfigurationMasters/Item Master/Routers/itemSpec.router");
const itemRouter = require("./APIs/ConfigurationMasters/Item Master/Routers/item.router");
const bankRouter = require("./APIs/ConfigurationMasters/BankMaster/Routers/bank.router");
const vendorRouter = require("./APIs/ConfigurationMasters/Vendor Management/Routers/vendor.router");
const vendorBankRouter = require("./APIs/ConfigurationMasters/Vendor Management/Routers/vendorBankDetails.router");

// ========== ROUTES ========== //
app.use("/api/v1", assetCategoryRouter);
app.use("/api/v1", assetSubCategoryRouter);
app.use("/api/v1", uomRouter);
app.use("/api/v1", itemSpecRouter);
app.use("/api/v1", itemRouter);
app.use("/api/v1", bankRouter);
app.use("/api/v1", vendorRouter);
app.use("/api/v1", vendorBankRouter);

// ========== SWAGGER IMPLEMENTATION ========== //

// ========== LISTENING SERVER ========== //
app.listen(port, (err) => {
  if (!err) {
    console.log(`Listening to the server at port : ${port}`);
  } else {
    console.log(err);
  }
});
